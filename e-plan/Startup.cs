using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Eplan.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Eplan.Services;
using Eplan.Services.Jenis;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;
using Eplan.Services.Rencana.Menengah;

namespace Eplan
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();
      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<RkpdContext>(options => options.UseSqlServer(Configuration.GetConnectionString("RkpdConnectionString")));
      services.AddMvc();
      services.AddScoped<PrimaryKeyService>();
      services.AddScoped<StrukturUnitService>();
      services.AddScoped<StrukturRekeningService>();
      services.AddScoped<GolonganService>();
      services.AddScoped<JenisPerspektifService>();
      services.AddScoped<JenisDanaService>();
      services.AddScoped<JenisKinerjaKegiatanService>();
      services.AddScoped<SifatKegiatanService>();
      services.AddScoped<JenisStandarService>();
      services.AddScoped<DasarHukumService>();
      services.AddScoped<PrioritasProvinsiService>();
      services.AddScoped<PrioritasPembangunanService>();
      services.AddScoped<StandarHargaService>();
      services.AddScoped<SasaranPembangunanService>();
      services.AddScoped<UnitOrganisasiService>();
      services.AddScoped<KecamatanService>();
      services.AddScoped<UnitOrganisasiBuilder>();
      services.AddScoped<TahunService>();
      services.AddScoped<DesaService>();
      services.AddScoped<RekeningService>();
      services.AddScoped<PegawaiService>();
      services.AddScoped<DewanService>();
      services.AddScoped<KepalaSKPDService>();
      services.AddScoped<UrusanUnitService>();
      services.AddScoped<VisiService>();
      services.AddScoped<MisiService>();
      services.AddScoped<TujuanService>();
      services.AddScoped<SasaranService>();
      services.AddScoped<IndikatorSasaranService>();
      services.AddScoped<StrategiService>();
      services.AddScoped<KebijakanService>();
      services.AddScoped<PrioritasNasional>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.Use(async (context, next) =>
      {
        await next();
        if (context.Response.StatusCode == 404 &&
                  !Path.HasExtension(context.Request.Path.Value) &&
                  !context.Request.Path.Value.StartsWith("/api/"))
        {
          context.Request.Path = "/index.html";
          await next();
        }
      });
      app.UseMvcWithDefaultRoute();
      app.UseDefaultFiles();
      app.UseStaticFiles();
    }
  }
}
