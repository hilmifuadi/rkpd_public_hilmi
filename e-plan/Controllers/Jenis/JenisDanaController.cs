using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/jenis-dana")]
  public class JenisDanaController : Controller
  {
    public JenisDanaController(JenisDanaService jenisDanaService)
    {
      service = jenisDanaService;
    }

    private JenisDanaService service;

    [HttpGet]
    public IEnumerable<JenisDana> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpPost]
    public JenisDana Create([FromBody] JenisDana jenisDana)
    {
      return service.Create(jenisDana);
    }

    [HttpGet("{id}")]
    public JenisDana Read(string id)
    {
      return service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody] JenisDana jenisDana)
    {
      return service.Update(jenisDana);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] JenisDana jenisDana)
    {
      return service.Delete(jenisDana);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<JenisDana> items)
    {
      return service.Deletes(items);
    }
  }
}
