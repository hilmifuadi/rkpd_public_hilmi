using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/golongan")]
  public class GolonganController : Controller
  {
    public GolonganController(GolonganService golonganService)
    {
      service = golonganService;
    }

    private GolonganService service;

    [HttpGet]
    public IEnumerable<Golongan> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpGet("{id}")]
    public Golongan Read(string id)
    {
      return service.Read(id);
    }

    [HttpPost]
    public Golongan Create([FromBody] Golongan golongan)
    {
      return service.Create(golongan);
    }

    [HttpPut]
    public bool Update([FromBody] Golongan golongan)
    {
      return service.Update(golongan);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] Golongan golongan)
    {
      return service.Delete(golongan);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<Golongan> items)
    {
      return service.Deletes(items);
    }
  }
}
