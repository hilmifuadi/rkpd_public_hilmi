using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/jenis-perspektif")]
  public class JenisPerspektifController : Controller
  {
    public JenisPerspektifController(JenisPerspektifService jenisPerspektifService)
    {
      service = jenisPerspektifService;
    }

    private JenisPerspektifService service;

    [HttpGet]
    public IEnumerable<JenisPerspektif> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpPost]
    public JenisPerspektif Create([FromBody] JenisPerspektif jenisPerspektif)
    {
      return service.Create(jenisPerspektif);
    }

    [HttpGet("{id}")]
    public JenisPerspektif Read(string id)
    {
      return service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody] JenisPerspektif jenisPerspektif)
    {
      return service.Update(jenisPerspektif);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] JenisPerspektif item)
    {
      return service.Delete(item);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<JenisPerspektif> items)
    {
      return service.Deletes(items);
    }
  }
}
