using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/sifat-kegiatan")]
  public class SifatKegiatanController : Controller
  {
    public SifatKegiatanController(SifatKegiatanService sifatKegiatanService)
    {
      service = sifatKegiatanService;
    }

    private SifatKegiatanService service;

    [HttpGet]
    public IEnumerable<SifatKegiatan> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpPost]
    public SifatKegiatan Create([FromBody] SifatKegiatan sifatKegiatan)
    {
      return service.Create(sifatKegiatan);
    }

    [HttpGet("{id}")]
    public SifatKegiatan Read(string id)
    {
      return service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody] SifatKegiatan sifatKegiatan)
    {
      return service.Update(sifatKegiatan);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] SifatKegiatan entity)
    {
      return service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<SifatKegiatan> entities)
    {
      return service.Deletes(entities);
    }
  }
}
