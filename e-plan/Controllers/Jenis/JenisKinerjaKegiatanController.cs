using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/jenis-kinerja-kegiatan")]
  public class JenisKinerjaKegiatanController : Controller
  {
    public JenisKinerjaKegiatanController(JenisKinerjaKegiatanService jenisKinerjaKegiatanService)
    {
      service = jenisKinerjaKegiatanService;
    }

    private JenisKinerjaKegiatanService service;

    [HttpGet]
    public IEnumerable<JenisKinerjaKegiatan> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpPost]
    public JenisKinerjaKegiatan Create([FromBody] JenisKinerjaKegiatan jenisKinerjaKegiatan)
    {
      return service.Create(jenisKinerjaKegiatan);
    }

    [HttpGet("{id}")]
    public JenisKinerjaKegiatan Read(string id)
    {
      return service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody] JenisKinerjaKegiatan jenisKinerjaKegiatan)
    {
      return service.Update(jenisKinerjaKegiatan);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] JenisKinerjaKegiatan entity)
    {
      return service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<JenisKinerjaKegiatan> entities)
    {
      return service.Deletes(entities);
    } 
  }
}
