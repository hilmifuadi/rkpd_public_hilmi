using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/struktur-unit")]
  public class StrukturUnitController : Controller
  {
    public StrukturUnitController(StrukturUnitService strukturUnitService)
    {
      _service = strukturUnitService;
    }

    private StrukturUnitService _service;

    [HttpGet]
    public IEnumerable<StrukturUnit> GetCollection()
    {
      return _service.GetCollection();
    }

    [HttpPost]
    public StrukturUnit Create([FromBody]StrukturUnit entity)
    {
      return _service.Create(entity);
    }

    [HttpGet("{id}")]
    public StrukturUnit Read(string id)
    {
      return _service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]StrukturUnit strukturUnit)
    {
      return _service.Update(strukturUnit);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] StrukturUnit entity)
    {
      return _service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<StrukturUnit> entities)
    {
      return _service.Deletes(entities);
    }
  }
}
