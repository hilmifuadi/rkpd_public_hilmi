using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/struktur-rekening")]
  public class StrukturRekeningController : Controller
  {
    public StrukturRekeningController(StrukturRekeningService service)
    {
      _service = service;
    }

    private StrukturRekeningService _service;

    [HttpGet]
    public IEnumerable<StrukturRekening> GetCollection()
    {
      return _service.GetCollection();
    }

    [HttpGet("{id}")]
    public StrukturRekening Read(string id)
    {
      return _service.Read(id);
    }

    [HttpPost]
    public StrukturRekening Create([FromBody] StrukturRekening strukturRekening)
    {
      return _service.Create(strukturRekening);
    }

    [HttpPut]
    public bool Update([FromBody] StrukturRekening strukturRekening)
    {
      return _service.Update(strukturRekening);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] StrukturRekening item)
    {
      return _service.Delete(item);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<StrukturRekening> items)
    {
      return _service.Deletes(items);
    }
  }
}
