using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Jenis;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Jenis
{
  [Route("api/jenis/jenis-standar")]
  public class JenisStandarController : Controller
  {
    public JenisStandarController(JenisStandarService jenisStandarService)
    {
      service = jenisStandarService;
    }

    private JenisStandarService service;

    [HttpGet]
    public IEnumerable<JenisStandar> GetCollection()
    {
      return service.GetCollection();
    }

    [HttpPost]
    public JenisStandar Create([FromBody] JenisStandar jenisStandar)
    {
      return service.Create(jenisStandar);
    }

    [HttpGet("{id}")]
    public JenisStandar Read(string id)
    {
      return service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody] JenisStandar jenisStandar)
    {
      return service.Update(jenisStandar);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] JenisStandar entity)
    {
      return service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<JenisStandar> entities)
    {
      return service.Deletes(entities);
    }
  }
}
