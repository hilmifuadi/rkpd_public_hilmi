using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/dasar-hukum")]
  public class DasarHukumController : Controller
  {
    public DasarHukumController(DasarHukumService dasarHukumService)
    {
      _service = dasarHukumService;
    }

    private DasarHukumService _service;

    [HttpGet]
    public IEnumerable<DasarHukum> GetCollection()
    {
      return _service.GetCollection();
    }

    [HttpPost]
    public DasarHukum Create([FromBody]DasarHukum entity)
    {
      return _service.Create(entity);
    }

    [HttpGet("{id}")]
    public DasarHukum Read(string id)
    {
      return _service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]DasarHukum entity)
    {
      return _service.Update(entity);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] DasarHukum entity)
    {
      return _service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<DasarHukum> entities)
    {
      return _service.Deletes(entities);
    }
  }
}
