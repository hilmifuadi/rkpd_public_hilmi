using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/kepala-skpd")]
  public class KepalaSKPDController : Controller
  {
    public KepalaSKPDController(KepalaSKPDService kepalaSKPDService)
    {
      this.kepalaSKPDService = kepalaSKPDService;
    }

    private KepalaSKPDService kepalaSKPDService;

    [HttpGet("{id}")]
    public KepalaSKPD Get(string id)
    {
      var dto = kepalaSKPDService.GetItem(id);
      return dto;
    }

    [HttpPost]
    public KepalaSKPD Add([FromBody] KepalaSKPD item)
    {
      item.ActType = Models.ActType.ADD_ITEM;
      var added = kepalaSKPDService.AddItem(item);
      return added;
    }

    [HttpPost("{id}")]
    public KepalaSKPD Remove(string id)
    {
      var dto = kepalaSKPDService.GetItem(id);
      if(dto.IsError)
      {
        return dto;
      }
      else
      {
        dto.ActType = Models.ActType.REMOVE_ITEM;
        var removed = kepalaSKPDService.RemoveItem(dto);
        return removed;
      }
    }
  }
}
