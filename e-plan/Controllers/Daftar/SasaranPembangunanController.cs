using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/sasaran-pembangunan")]
  public class SasaranPembangunanController : Controller
  {
    public SasaranPembangunanController(SasaranPembangunanService sasaranPembangunanService)
    {
      this.sasaranPembangunanService = sasaranPembangunanService;
    }

    private SasaranPembangunanService sasaranPembangunanService;

    [HttpGet]
    public IEnumerable<SasaranPembangunan> GetCollection()
    {
      return sasaranPembangunanService.GetCollection();
    }

    [HttpPost]
    public SasaranPembangunan Create([FromBody]SasaranPembangunan sasaranPembangunan)
    {
      return sasaranPembangunanService.Create(sasaranPembangunan);
    }

    [HttpGet("{id}")]
    public SasaranPembangunan Read(string id)
    {
      return sasaranPembangunanService.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]SasaranPembangunan sasaranPembangunan)
    {
      return sasaranPembangunanService.Update(sasaranPembangunan);
    }

    [HttpPost("delete")]
    public bool Delete(string id, [FromBody]SasaranPembangunan sasaranPembangunan)
    {
      return sasaranPembangunanService.Delete(sasaranPembangunan);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<SasaranPembangunan> items)
    {
      return sasaranPembangunanService.Deletes(items);
    }
  }
}
