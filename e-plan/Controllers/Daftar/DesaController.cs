using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Data;
using Eplan.Models;
using Eplan.Models.DataTransaction;
using Eplan.Services;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/desa")]
  public class DesaController : Controller
  {
    public DesaController(RkpdContext rkpdContext, DesaService desaService)
    {
      this.rkpdContext = rkpdContext;
      this.desaService = desaService;
    }

    private RkpdContext rkpdContext;
    private DesaService desaService;

    [HttpGet]
    public Desa[] GetManyDesa()
    {
      var desa = desaService.GetItems();
      return desa;
    }

    [HttpPost("add")]
    public Desa AddDesa([FromBody] Desa item)
    {
      item.ActType = ActType.ADD_ITEM;
      var state = desaService.AddItem(item);
      return state;
    }

    [HttpPost("remove/{id}")]
    public Desa RemoveDesa(string id)
    {
      var blo = rkpdContext.DAFTDESA.AsNoTracking().Single(e => e.IDDESA == id);
      var desa = new Desa
      {
        Id = blo.IDDESA,
        Pagu = blo.PAGU,
        KecamatanId = blo.IDKEC,
        TahunId = blo.KDTAHUN,
        UnitOrganisasiId = blo.UNITKEY
      };
      desa.ActType = ActType.REMOVE_ITEM;
      var state = desaService.RemoveItem(desa);
      return state;
    }

    [HttpPost("remove")]
    public Desa[] RemoveManyDesa([FromBody] string[] keys)
    {
      int index = keys.Length;
      Desa[] desa = new Desa[index];
      for (var i = 0; i < index; i++)
      {
        var state = RemoveDesa(keys[i]);
        desa[i] = state;
      }
      return desa;
    }
  }
}
