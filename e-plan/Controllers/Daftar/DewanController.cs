using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  [Produces("application/json")]
  [Route("api/daftar/dewan")]
  public class DewanController : Controller
  {
    public DewanController(DewanService dewanService)
    {
      this.dewanService = dewanService;
    }

    private DewanService dewanService;

    [HttpGet("{id}")]
    public Dewan Get(string id)
    {
      var dto = dewanService.GetItem(id);
      return dto;
    }

    [HttpPost]
    public Dewan Add([FromBody] Dewan item)
    {
      item.ActType = Models.ActType.ADD_ITEM;
      var added = dewanService.AddItem(item);
      return added;
    }

    [HttpPut("{id}")]
    public Dewan Update(string id, [FromBody] Dewan item)
    {
      item.ActType = Models.ActType.UPDATE_ITEM;
      var updated = dewanService.UpdateItem(id, item);
      return updated;
    }

    [HttpPost("{id}")]
    public Dewan Remove(string id)
    {
      var dto = dewanService.GetItem(id);
      if(dto.IsError)
      {
        return dto;
      }
      else
      {
        dto.ActType = Models.ActType.REMOVE_ITEM;
        var removed = dewanService.RemoveItem(dto);
        return removed;
      }
    }

    [HttpPost("removes")]
    public Dewan[] RemoveMany(string[] keys)
    {
      int size = keys.Length;
      Dewan[] items = new Dewan[size];
      for(int i = 0; i < size; i++)
      {
        var dto = dewanService.GetItem(keys[i]);
        if(dto.IsError)
        {
          items[i] = dto;
        }
        else
        {
          dto.ActType = Models.ActType.REMOVE_MANY_ITEM;
          var removed = dewanService.RemoveItem(dto);
          items[i] = removed;
        }
      }
      return items;
    }
  }
}
