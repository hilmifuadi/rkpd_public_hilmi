using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/unit-organisasi")]
  public class UnitOrganisasiController : Controller
  {
    public UnitOrganisasiController(UnitOrganisasiService unitOrganisasiService)
    {
      this.unitOrganisasiService = unitOrganisasiService;
    }

    private UnitOrganisasiService unitOrganisasiService;

    [HttpGet("node/{level}/{unit}")]
    public List<UnitOrganisasi> Get(int level, string unit)
    {
      return unitOrganisasiService.Get(level, unit);
    }

    [HttpGet]
    public List<UnitOrganisasi> GetCollection()
    {
      return unitOrganisasiService.GetCollection();
    }

    [HttpGet("{id}")]
    public UnitOrganisasi Get(string id)
    {
      var dto = unitOrganisasiService.AutoGetItem(id);
      return dto;
    }

    [HttpPost]
    public UnitOrganisasi Add([FromBody] UnitOrganisasi item)
    {
      item.ActType = Models.ActType.ADD_ITEM;
      var added = unitOrganisasiService.AddItem(item);
      return item;
    }

    [HttpPut("{id}")]
    public UnitOrganisasi Update(string id, [FromBody] UnitOrganisasi item)
    {
        item.ActType = Models.ActType.UPDATE_ITEM;
        var updated = unitOrganisasiService.UpdateItem(id, item);
        return updated;
    }

    //Should response send whole object?
    [HttpPost("{id}")]
    public UnitOrganisasi Remove(string id)
    {
      var dto = unitOrganisasiService.AutoGetItem(id);
      if (dto.IsError)
      {
        return dto;
      }
      else
      {
        dto.ActType = Models.ActType.REMOVE_ITEM;
        var removed = unitOrganisasiService.RemoveItem(dto);
        return removed;
      }
    }


    //Should response send whole object?
    [HttpPost("removes")]
    public UnitOrganisasi[] RemoveMany([FromBody] string[] keys)
    {
      UnitOrganisasi[] beenRemoved = new UnitOrganisasi[keys.Length];
      for(int i =0; i < keys.Length; i++)
      {
        var dto = unitOrganisasiService.AutoGetItem(keys[i]);
        if (dto.IsError)
        {
          beenRemoved[i] = dto;
        }
        else
        {
          dto.ActType = Models.ActType.REMOVE_MANY_ITEM;
          var removed = unitOrganisasiService.RemoveItem(dto);
          beenRemoved[i] = removed;
        }
      }
      return beenRemoved;
    }
  }
}
