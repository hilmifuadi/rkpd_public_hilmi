using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/kecamatan")]
  public class KecamatanController : Controller
  {
    public KecamatanController(KecamatanService kecamatanService)
    {
      this.kecamatanService = kecamatanService;
    }

    private KecamatanService kecamatanService;

    [HttpGet]
    public List<Kecamatan> GetCollection()
    {
      return kecamatanService.GetCollection();
    }

    [HttpPost]
    public Kecamatan Create([FromBody] Kecamatan kecamatan)
    {
      return kecamatanService.Create(kecamatan);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] Kecamatan kecamatan)
    {
      return kecamatanService.Delete(kecamatan);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody] List<Kecamatan> items)
    {
      return kecamatanService.Deletes(items);
    }
  }
}
