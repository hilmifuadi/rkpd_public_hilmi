using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  public class RekeningController : Controller
  {
    public RekeningController(RekeningService rekeningService)
    {
      this.rekeningService = rekeningService;
    }

    private RekeningService rekeningService;

    [HttpPost]
    public Rekening Add([FromBody] Rekening item)
    {
      item.ActType = Models.ActType.ADD_ITEM;
      var added = rekeningService.AddItem(item);
      return added;
    }

    [HttpPut("{id}")]
    public Rekening Update(string id, [FromBody] Rekening item)
    {
      item.ActType = Models.ActType.UPDATE_ITEM;
      var updated = rekeningService.UpdateItem(id, item);
      return updated;
    }

    [HttpPost("{id}")]
    public Rekening Remove(string id)
    {
      var dto = rekeningService.GetItem(id);
      if (dto.IsError)
      {
        return dto;
      }
      else
      {
        dto.ActType = Models.ActType.REMOVE_ITEM;
        var removed = rekeningService.RemoveItem(dto);
        return removed;
      }
    }

    [HttpPost("removes")]
    public Rekening[] RemoveMany([FromBody] string[] keys)
    {
      int size = keys.Length;
      Rekening[] items = new Rekening[size];
      for(var i = 0; i < size; i++)
      {
        var dto = rekeningService.GetItem(keys[i]);
        if (dto.IsError)
        {
          items[i] = dto;
        }
        else
        {
          dto.ActType = Models.ActType.REMOVE_MANY_ITEM;
          var removed = rekeningService.RemoveItem(dto);
          items[i] = removed;
        }
      }
      return items;
    }
  }
}
