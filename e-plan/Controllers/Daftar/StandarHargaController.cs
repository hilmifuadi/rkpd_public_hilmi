using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/standar-harga")]
  public class StandarHargaController : Controller
  {
    public StandarHargaController(StandarHargaService standarHargaService)
    {
      this.standarHargaService = standarHargaService;
    }

    private StandarHargaService standarHargaService;

    [HttpGet]
    public IEnumerable<StandarHarga> GetCollection()
    {
      return standarHargaService.GetCollection();
    }

    [HttpPost]
    public StandarHarga Create([FromBody]StandarHarga standarHarga)
    {
      return standarHargaService.Create(standarHarga);
    }

    [HttpGet("{id}")]
    public StandarHarga Read(string id)
    {
      return standarHargaService.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]StandarHarga standarHarga)
    {
      return standarHargaService.Update(standarHarga);
    }

    [HttpPost("delete")]
    public bool Delete(string id, [FromBody]StandarHarga standarHarga)
    {
      return standarHargaService.Delete(standarHarga);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<StandarHarga> entities)
    {
      return standarHargaService.Deletes(entities);
    }
  }
}
