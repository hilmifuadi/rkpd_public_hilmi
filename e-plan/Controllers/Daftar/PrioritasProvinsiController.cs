using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/prioritas-provinsi")]
  public class PrioritasProvinsiController : Controller
  {
    public PrioritasProvinsiController(PrioritasProvinsiService prioritasProvinsiService)
    {
      _service = prioritasProvinsiService;
    }

    private PrioritasProvinsiService _service;

    [HttpGet]
    public IEnumerable<PrioritasProvinsi> GetCollection()
    {
      return _service.GetCollection();
    }

    [HttpPost]
    public PrioritasProvinsi Create([FromBody]PrioritasProvinsi entity)
    {
      return _service.Create(entity);
    }

    [HttpGet("{id}")]
    public PrioritasProvinsi Read(string id)
    {
      return _service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]PrioritasProvinsi entity)
    {
      return _service.Update(entity);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] PrioritasProvinsi entity)
    {
      return _service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<PrioritasProvinsi> entities)
    {
      return _service.Deletes(entities);
    }
  }
}
