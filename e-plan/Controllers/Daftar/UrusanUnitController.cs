using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  [Produces("application/json")]
  [Route("api/daftar-/urusan-unit")]
  public class UrusanUnitController : Controller
  {
    public UrusanUnitController(UrusanUnitService urusanUnitService)
    {
      this.urusanUnitService = urusanUnitService;
    }

    private UrusanUnitService urusanUnitService;

    [HttpPost]
    public UrusanUnit Add([FromBody]string unitId, [FromBody]string[] urusanId)
    {
      UrusanUnit item = new UrusanUnit
      {
        ActType = Models.ActType.ADD_ITEM
      };
      urusanUnitService.AddItem(item, unitId, urusanId);
      return item;
    }

    [HttpDelete]
    public UrusanUnit Remove([FromBody]string unitId, [FromBody]string[] urusanId)
    {
      UrusanUnit item = new UrusanUnit
      {
        ActType = Models.ActType.REMOVE_ITEM
      };
      urusanUnitService.RemoveItem(item, unitId, urusanId);
      return item;
    }
  }
}
