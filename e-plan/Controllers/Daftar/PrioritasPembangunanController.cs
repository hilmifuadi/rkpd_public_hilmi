using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eplan.Services.Daftar;
using Eplan.Models.DataTransaction;

namespace Eplan.Controllers.Daftar
{
  [Route("api/daftar/prioritas-pembangunan")]
  public class PrioritasPembangunanController : Controller
  {
    public PrioritasPembangunanController(PrioritasPembangunanService prioritasPembangunanService)
    {
      _service = prioritasPembangunanService;
    }

    private PrioritasPembangunanService _service;

    [HttpGet]
    public IEnumerable<PrioritasPembangunan> GetCollection()
    {
      return _service.GetCollection();
    }

    [HttpPost]
    public PrioritasPembangunan Create([FromBody]PrioritasPembangunan entity)
    {
      return _service.Create(entity);
    }

    [HttpGet("{id}")]
    public PrioritasPembangunan Read(string id)
    {
      return _service.Read(id);
    }

    [HttpPut]
    public bool Update([FromBody]PrioritasPembangunan entity)
    {
      return _service.Update(entity);
    }

    [HttpPost("delete")]
    public bool Delete([FromBody] PrioritasPembangunan entity)
    {
      return _service.Delete(entity);
    }

    [HttpPost("deletes")]
    public bool Deletes([FromBody]List<PrioritasPembangunan> entities)
    {
      return _service.Deletes(entities);
    }
  }
}
