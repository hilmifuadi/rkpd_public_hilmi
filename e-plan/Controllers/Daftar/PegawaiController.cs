using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Daftar;
using Microsoft.AspNetCore.Mvc;

namespace Eplan.Controllers.Daftar
{
  public class PegawaiController : Controller
  {
    public PegawaiController(PegawaiService pegawaiService)
    {
      this.pegawaiService = pegawaiService;
    }

    private PegawaiService pegawaiService;

    [HttpGet("{id}")]
    public Pegawai Get(string id)
    {
      var dto = pegawaiService.GetItem(id);
      return dto;
    }

    [HttpPost]
    public Pegawai Add([FromBody] Pegawai item)
    {
      item.ActType = Models.ActType.ADD_ITEM;
      var added = pegawaiService.AddItem(item);
      return added;
    }

    [HttpPut("{id}")]
    public Pegawai Update(string id, [FromBody]Pegawai item)
    {
      item.ActType = Models.ActType.UPDATE_ITEM;
      var updated = pegawaiService.UpdateItem(id, item);
      return updated;
    }

    [HttpPost("{id}")]
    public Pegawai Remove(string id)
    {
      var dto = pegawaiService.GetItem(id);
      if(dto.IsError)
      {
        return dto;
      }
      else
      {
        dto.ActType = Models.ActType.REMOVE_ITEM;
        var removed = pegawaiService.RemoveItem(dto);
        return removed;
      }
    }

    [HttpPost("removes")]
    public Pegawai[] RemoveMany([FromBody] string[] keys)
    {
      var size = keys.Length;
      Pegawai[] items = new Pegawai[size];
      for(int i = 0; i < size; i++)
      {
        var dto = pegawaiService.GetItem(keys[i]);
        if (dto.IsError)
        {
          items[i] = dto;
        }
        else
        {
          dto.ActType = Models.ActType.REMOVE_MANY_ITEM;
          var removed = pegawaiService.RemoveItem(dto);
          items[i] = removed;
        }
      }
      return items;
    }
  }
}
