using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eplan.Models.DataTransaction;
using Eplan.Services.Rencana.Menengah;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Eplan.Controllers.Rencana.Menengah
{
  [Route("api/rpjmd/visi")]
  public class VisiController : Controller
  {
    public VisiController(VisiService visiService)
    {
      service = visiService;
    }

    private VisiService service;
    // GET: api/<controller>
    [HttpGet]
    public IEnumerable<Visi> GetCollection()
    {
      return service.GetItems();
    }

    [HttpGet("{id}")]
    public Visi Get(string id)
    {
      return service.GetItem(id);
    }

    [HttpPost]
    public Visi Create([FromBody] Visi visi)
    {
      visi.ActType = Models.ActType.ADD_ITEM;
      return service.AddItem(visi);
    }

    [HttpPut("{id}")]
    public Visi Update([FromBody] Visi visi, string id)
    {
      visi.ActType = Models.ActType.UPDATE_ITEM;
      return service.UpdateItem(id, visi);
    }

    [HttpPost("delete/{id}")]
    public Visi Delete(string id)
    {
      var visi = service.GetItem(id);
      if (visi.IsError)
      {
        return visi;
      }
      else
      {
        visi.ActType = Models.ActType.REMOVE_ITEM;
        return service.RemoveItem(visi);
      }

    }

    [HttpPost("deletes")]
    public List<Visi> Deletes([FromBody] string[] visiids)
    {
      var visis = new List<Visi>();
      foreach (string id in visiids)
      {
        var visi = service.GetItem(id);
        if (visi.IsError)
        {
          visis.Add(visi);
        }
        else
        {
          visi.ActType = Models.ActType.REMOVE_MANY_ITEM;
          visis.Add(service.RemoveItem(visi));
        }

      }
      return visis;
    }


  }
}
