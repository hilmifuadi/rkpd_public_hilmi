using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class StrategiService
  {
    public StrategiService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    public RkpdContext rkpdContext;
    public PrimaryKeyService primaryKeyService;

    private void SetError(Strategi item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    private IQueryable<STRATEGI> bottomToTopQuery;
    protected IQueryable<STRATEGI> BottomToTopQuery
    {
      get
      {
        bottomToTopQuery = rkpdContext.STRATEGI.Include(e => e.SASARAN).AsNoTracking();
        return bottomToTopQuery;
      }
    }

    public Strategi[] GetItems(string sasaranId = null)
    {
      Strategi[] items;
      int length;
      try
      {
        var bdo = rkpdContext.STRATEGI
          .Include(e => e.SASARAN)
          .Where(e => e.IDSAS == sasaranId)
          .AsNoTracking()
          .ToArray();
        length = bdo.Length;
        items = new Strategi[length];
        int i = 0;
        while(i < length)
        {
          var strategi = new Strategi
          {
            Id = bdo[i].STRATEGIKEY,
            Nomor = bdo[i].NOSTRATEGI,
            Uraian = bdo[i].URAISTRATEGI,
            Keterangan = bdo[i].KET,
          };
          var sasaran = new Sasaran
          {
            Id = bdo[i].SASARAN.IDSAS,
            Nomor = bdo[i].SASARAN.NOSAS,
            Uraian = bdo[i].SASARAN.NMSAS,
            Keterangan = bdo[i].SASARAN.KET
          };
          strategi.Sasaran = sasaran;
          items[i] = strategi;
          i++;
        }
      }
      catch
      {
        items = new Strategi[0];
      }
      return items;
    }

    public Strategi GetItem(string id)
    {
      Strategi item = new Strategi
      {
        ActType = Models.ActType.GET_ITEM
      };
      try
      {
        var strategi = BottomToTopQuery.SingleOrDefault(e => e.STRATEGIKEY == id);
        item.Id = strategi.STRATEGIKEY;
        item.Nomor = strategi.NOSTRATEGI;
        item.Uraian = strategi.URAISTRATEGI;
        item.Keterangan = strategi.KET;
        var sasaran = new Sasaran
        {
          Id = strategi.SASARAN.IDSAS,
          Nomor = strategi.SASARAN.NOSAS,
          Uraian = strategi.SASARAN.NMSAS,
          Keterangan = strategi.SASARAN.KET
        };
        item.Sasaran = sasaran;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Strategi AddItem(Strategi item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("Strategi", "Strategikey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);
        item.Id = cKey;
        var bdo = new STRATEGI
        {
          IDSAS = item.Sasaran.Id,
          STRATEGIKEY = item.Id,
          NOSTRATEGI = item.Nomor,
          URAISTRATEGI = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.STRATEGI.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Strategi UpdateItem(string id, Strategi item)
    {
      try
      {
        var bdo = BottomToTopQuery.SingleOrDefault(e => e.STRATEGIKEY == id);
        bdo.NOSTRATEGI = item.Nomor;
        bdo.URAISTRATEGI = item.Uraian;
        bdo.KET = item.Keterangan;
        rkpdContext.STRATEGI.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Strategi RemoveItem(Strategi item)
    {
      try
      {
        var bdo = new STRATEGI
        {
          IDSAS = item.Sasaran.Id,
          STRATEGIKEY = item.Id,
          NOSTRATEGI = item.Nomor,
          URAISTRATEGI = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.STRATEGI.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
