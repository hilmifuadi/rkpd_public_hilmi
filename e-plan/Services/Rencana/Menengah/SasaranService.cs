using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class SasaranService
  {
    public SasaranService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private void SetError(Sasaran item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public Sasaran[] GetItems()
    {
      Sasaran[] items;
      int length;
      try
      {
        var bdo = rkpdContext.SASARAN.Include(e => e.TUJUAN).ToArray();
        length = bdo.Length;
        items = new Sasaran[length];
        int i = 0;
        while(i < length)
        {
          var sasaran = new Sasaran
          {
            Id = bdo[i].IDSAS,
            Nomor = bdo[i].NOSAS,
            Uraian = bdo[i].NMSAS,
            Keterangan = bdo[i].KET
          };
          var tujuan = new Tujuan
          {
            Id = bdo[i].TUJUAN.TUJUKEY,
            Nomor = bdo[i].TUJUAN.NOTUJU,
            Uraian = bdo[i].TUJUAN.URAITUJU
          };
          sasaran.Tujuan = tujuan;
          items[i] = sasaran;
          i++;
        }
      }
      catch
      {
        items = new Sasaran[0];
      }
      return items;
    }

    public Sasaran GetItem(string id)
    {
      Sasaran item = new Sasaran
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.SASARAN.Include(e => e.TUJUAN).SingleOrDefault(e => e.IDSAS == id);
        item.Id = bdo.IDSAS;
        item.Nomor = bdo.NOSAS;
        item.Uraian = bdo.NMSAS;
        item.Keterangan = bdo.KET;
        var tujuan = new Tujuan
        {
          Id = bdo.TUJUAN.TUJUKEY,
          Nomor = bdo.TUJUAN.NOTUJU,
          Uraian = bdo.TUJUAN.URAITUJU
        };
        item.Tujuan = tujuan;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Sasaran Additem(Sasaran item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("Sasaran", "Idsas", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;

        var bdo = new SASARAN
        {
          TUJUKEY = item.Tujuan.Id,
          IDSAS = item.Id,
          NOSAS = item.Nomor,
          NMSAS = item.Uraian,
          KET = item.Keterangan
        };

        rkpdContext.SASARAN.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Sasaran UpdateItem(string id, Sasaran item)
    {
      try
      {
        var bdo = rkpdContext.SASARAN.SingleOrDefault(e => e.IDSAS == id);
        bdo.NOSAS = item.Nomor;
        bdo.NMSAS = item.Uraian;
        bdo.KET = item.Keterangan;
        rkpdContext.SASARAN.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Sasaran RemoveItem(Sasaran item)
    {
      try
      {
        var bdo = new SASARAN
        {
          TUJUKEY = item.Tujuan.Id,
          IDSAS = item.Id,
          NOSAS = item.Nomor,
          NMSAS = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.SASARAN.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
