using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class IndikatorSasaranService
  {
    public IndikatorSasaranService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.primaryKeyService = primaryKeyService;
      this.rkpdContext = rkpdContext;
    }
    
    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private void SetError(IndikatorSasaran item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public IndikatorSasaran[] Getitems(string sasaranId)
    {
      IndikatorSasaran[] items;
      int length;
      try
      {
        var bdo = rkpdContext.SASARANDET.Include(e => e.SASARAN).Where(e => e.IDSAS == sasaranId).AsNoTracking().ToArray();
        length = bdo.Length;
        items = new IndikatorSasaran[length];
        int i = 0;
        while(i < length)
        {
          var dto = new IndikatorSasaran
          {
            Id = bdo[i].IDSASDET,
            Nomor = bdo[i].NOSASDET,
            Uraian = bdo[i].URAIDET,
            Satuan = bdo[i].SATUAN,
          };
          var sasaran = new Sasaran
          {
            Id = bdo[i].SASARAN.IDSAS,
            Nomor = bdo[i].SASARAN.NOSAS,
            Uraian = bdo[i].SASARAN.NMSAS,
            Keterangan = bdo[i].SASARAN.KET
          };
          dto.Sasaran = sasaran;
          items[i] = dto;
          i++;
        }
      }
      catch
      {
        items = new IndikatorSasaran[0];
      }
      return items;
    }

    public IndikatorSasaran GetItem(string id)
    {
      IndikatorSasaran item = new IndikatorSasaran
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.SASARANDET.Include(e => e.SASARAN).AsNoTracking().SingleOrDefault(e => e.IDSASDET == id);
        item.Id = bdo.IDSASDET;
        item.Nomor = bdo.NOSASDET;
        item.Uraian = bdo.URAIDET;
        item.Satuan = bdo.SATUAN;
        var sasaran = new Sasaran
        {
          Id = bdo.SASARAN.IDSAS,
          Nomor = bdo.SASARAN.NOSAS,
          Uraian = bdo.SASARAN.NMSAS,
          Keterangan = bdo.SASARAN.KET
        };
        item.Sasaran = sasaran;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public IndikatorSasaran AddItem(IndikatorSasaran item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("Sasarandet", "Idsas", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;

        var bdo = new SASARANDET
        {
          IDSAS = item.Sasaran.Id,
          IDSASDET = item.Id,
          NOSASDET = item.Nomor,
          URAIDET = item.Uraian,
          SATUAN = item.Satuan
        };

        rkpdContext.SASARANDET.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public IndikatorSasaran UpdateItem(string id, IndikatorSasaran item)
    {
      try
      {
        var bdo = rkpdContext.SASARANDET.SingleOrDefault(e => e.IDSASDET == id);
        bdo.NOSASDET = item.Nomor;
        bdo.URAIDET = item.Uraian;
        bdo.SATUAN = item.Satuan;
        rkpdContext.SASARANDET.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public IndikatorSasaran RemoveItem(IndikatorSasaran item)
    {
      try
      {
        var bdo = new SASARANDET
        {
          IDSAS = item.Sasaran.Id,
          IDSASDET = item.Id,
          NOSASDET = item.Nomor,
          URAIDET = item.Uraian,
          SATUAN = item.Satuan
        };
        rkpdContext.SASARANDET.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
