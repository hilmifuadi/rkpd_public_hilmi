using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class MisiService
  {
    public MisiService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }
    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private void SetError(Misi item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public Misi[] GetItems()
    {
      Misi[] items;
      int length;
      try
      {
        var bdo = rkpdContext.MISI.Include(e => e.VISI).ToArray();
        length = bdo.Length;
        items = new Misi[length];
        int i = 0;
        while(i < length)
        {
          var dto = new Misi
          {
            Id = bdo[i].MISIKEY,
            Nomor = bdo[i].NOMISI,
            Uraian = bdo[i].URAIMISI
          };
          var visi = new Visi
          {
            Id = bdo[i].VISI.IDVISI,
            Nomor = bdo[i].VISI.NOVISI,
            Uraian = bdo[i].VISI.NMVISI
          };
          dto.Visi = visi;
          items[i] = dto;
          i++;
        }
      }
      catch
      {
        items = new Misi[0];
      }
      return items;
    }

    public Misi GetItem(string id)
    {
      Misi item = new Misi
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var misi = rkpdContext.MISI
          .Include(e => e.VISI)
          .SingleOrDefault(e => e.MISIKEY == id);
        item.Id = misi.MISIKEY;
        item.Nomor = misi.NOMISI;
        item.Uraian = misi.URAIMISI;
        var visi = new Visi
        {
          Id = misi.VISI.IDVISI,
          Nomor = misi.VISI.NOVISI,
          Uraian = misi.VISI.NMVISI,
        };
        item.Visi = visi;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Misi AddItem(Misi item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("misi", "misikey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;

        var bdo = new MISI
        {
          IDVISI = item.Visi.Id,
          MISIKEY = item.Id,
          NOMISI = item.Nomor,
          URAIMISI = item.Uraian
        };

        rkpdContext.MISI.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Misi UpdateItem(string id, Misi item)
    {
      try
      {
        var bdo = rkpdContext.MISI.SingleOrDefault(e => e.MISIKEY == id);
        bdo.NOMISI = item.Nomor;
        bdo.URAIMISI = item.Uraian;
        rkpdContext.MISI.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Misi RemoveItem(Misi item)
    {
      try
      {
        var bdo = new MISI
        {
          IDVISI = item.Visi.Id,
          MISIKEY = item.Id,
          NOMISI = item.Nomor,
          URAIMISI = item.Uraian
        };
        rkpdContext.MISI.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
