using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class TujuanService
  {
    public TujuanService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    public RkpdContext rkpdContext;
    public PrimaryKeyService primaryKeyService;

    private void SetError(Tujuan item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public Tujuan[] GetItems()
    {
      Tujuan[] items;
      int length;
      try
      {
        var bdo = rkpdContext
          .TUJUAN
          .Include(e => e.MISI)
          .ToArray();
        length = bdo.Length;
        items = new Tujuan[length];
        int i = 0;
        while(i < length)
        {
          var tujuan = new Tujuan
          {
            Id = bdo[i].TUJUKEY,
            Nomor = bdo[i].NOTUJU,
            Uraian = bdo[i].URAITUJU
          };
          var misi = new Misi
          {
            Id = bdo[i].MISI.MISIKEY,
            Nomor = bdo[i].MISI.NOMISI,
            Uraian = bdo[i].MISI.URAIMISI
          };
          tujuan.Misi = misi;
          items[i] = tujuan;
          i++;
        }
      }
      catch
      {
        items = new Tujuan[0];
      }
      return items;
    }

    public Tujuan GetItem(string id)
    {
      Tujuan item = new Tujuan
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.TUJUAN.Include(e => e.MISI).SingleOrDefault(e => e.TUJUKEY == id);
        item.Id = bdo.TUJUKEY;
        item.Nomor = bdo.NOTUJU;
        item.Uraian = bdo.URAITUJU;
        var misi = new Misi
        {
          Id = bdo.MISI.MISIKEY,
          Nomor = bdo.MISI.NOMISI,
          Uraian = bdo.MISI.URAIMISI
        };
        item.Misi = misi;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Tujuan Additem(Tujuan item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("Tujuan", "Tujukey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;

        var bdo = new TUJUAN
        {
          MISIKEY = item.Misi.Id,
          TUJUKEY = item.Id,
          NOTUJU = item.Nomor,
          URAITUJU = item.Uraian
        };

        rkpdContext.TUJUAN.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Tujuan UpdateItem(string id, Tujuan item)
    {
      try
      {
        var bdo = rkpdContext.TUJUAN.SingleOrDefault(e => e.TUJUKEY == id);
        bdo.NOTUJU = item.Nomor;
        bdo.URAITUJU = item.Uraian;
        rkpdContext.TUJUAN.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Tujuan RemoveItem(Tujuan item)
    {
      try
      {
        var bdo = new TUJUAN
        {
          MISIKEY = item.Misi.Id,
          TUJUKEY = item.Id,
          NOTUJU = item.Nomor,
          URAITUJU = item.Uraian,
        };
        rkpdContext.TUJUAN.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
