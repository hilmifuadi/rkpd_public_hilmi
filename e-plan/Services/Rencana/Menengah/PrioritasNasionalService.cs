using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class PrioritasNasionalService
  {
    public PrioritasNasionalService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private IQueryable<PRIONAS> untrackedQuery;
    protected IQueryable<PRIONAS> UntrackedQuery
    {
      get
      {
        untrackedQuery = rkpdContext.PRIONAS.AsNoTracking();
        return untrackedQuery;
      }
    }

    private IQueryable<PRIONAS> trackedQuery;
    protected IQueryable<PRIONAS> TrackedQuery
    {
      get
      {
        trackedQuery = rkpdContext.PRIONAS;
        return trackedQuery;
      }
    }

    private void SetError(PrioritasNasional item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public PrioritasNasional[] Getitems()
    {
      PrioritasNasional[] items;
      int length;
      try
      {
        var bdo = UntrackedQuery.ToArray();
        length = bdo.Length;
        items = new PrioritasNasional[length];
        int i = 0;
        while(i < length)
        {
          var item = new PrioritasNasional
          {
            Id = bdo[i].PRIONASKEY,
            Nomor = bdo[i].NUPRIO,
            Uraian = bdo[i].NMPRIO,
            Keterangan = bdo[i].KET
          };
          items[i] = item;
          i++;
        }
      }
      catch
      {
        items = new PrioritasNasional[0];
      }
      return items;
    }

    public PrioritasNasional GetItem(string id)
    {
      PrioritasNasional item = new PrioritasNasional
      {
        ActType = Models.ActType.GET_ITEM
      };
      try
      {
        var bdo = UntrackedQuery.SingleOrDefault(e => e.PRIONASKEY == id);
        item.Id = bdo.PRIONASKEY;
        item.Nomor = bdo.NUPRIO;
        item.Uraian = bdo.NMPRIO;
        item.Keterangan = bdo.KET;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public PrioritasNasional AddItem(PrioritasNasional item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("Prionas", "Prionaskey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);
        item.Id = cKey;
        var bdo = new PRIONAS
        {
          PRIONASKEY = item.Id,
          NUPRIO = item.Nomor,
          NMPRIO = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.PRIONAS.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public PrioritasNasional UpdateItem(string id, PrioritasNasional item)
    {
      try
      {
        var bdo = TrackedQuery.SingleOrDefault(e => e.PRIONASKEY == id);
        bdo.NUPRIO = item.Nomor;
        bdo.NMPRIO = item.Uraian;
        bdo.KET = item.Keterangan;
        rkpdContext.PRIONAS.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public PrioritasNasional RemoveItem(PrioritasNasional item)
    {
      try
      {
        var bdo = new PRIONAS
        {
          PRIONASKEY = item.Id,
          NUPRIO = item.Nomor,
          NMPRIO = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.PRIONAS.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
