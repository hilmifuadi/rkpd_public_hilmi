using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class KebijakanService
  {
    public KebijakanService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private IQueryable<KEBIJAKAN> bottomToTopQuery;
    protected IQueryable<KEBIJAKAN> BottomToTopQuery
    {
      get
      {
        bottomToTopQuery = rkpdContext.KEBIJAKAN.Include(e => e.STRATEGI).AsNoTracking();
        return bottomToTopQuery;
      }
    }

    private IQueryable<KEBIJAKAN> trackedQuery;
    protected IQueryable<KEBIJAKAN> TrackedQuery
    {
      get
      {
        trackedQuery = rkpdContext.KEBIJAKAN.Include(e => e.STRATEGI);
        return trackedQuery;
      }
    }

    private void SetError(Kebijakan item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public Kebijakan[] GetItems(string strategiId = null)
    {
      Kebijakan[] items;
      int length;
      try
      {
        var bdo = BottomToTopQuery.Where(e => e.STRATEGIKEY == strategiId).ToArray();
        length = bdo.Length;
        items = new Kebijakan[length];
        int i = 0;
        while(i < length)
        {
          var kebijakan = new Kebijakan
          {
            Id = bdo[i].BIJAKKEY,
            Nomor = bdo[i].NOBIJAKAN,
            Uraian = bdo[i].NMEBIJAKAN,
            Keterangan = bdo[i].KET,
          };
          var strategi = new Strategi
          {
            Id = bdo[i].STRATEGI.STRATEGIKEY,
            Nomor = bdo[i].STRATEGI.NOSTRATEGI,
            Uraian = bdo[i].STRATEGI.URAISTRATEGI,
            Keterangan = bdo[i].STRATEGI.KET,
          };
          kebijakan.Strategi = strategi;
          items[i] = kebijakan;
          i++;
        }
      }
      catch
      {
        items = new Kebijakan[0];
      }
      return items;
    }

    public Kebijakan GetItem(string id)
    {
      Kebijakan item = new Kebijakan
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = BottomToTopQuery.SingleOrDefault(e => e.BIJAKKEY == id);
        item.Id = bdo.BIJAKKEY;
        item.Nomor = bdo.NOBIJAKAN;
        item.Uraian = bdo.NMEBIJAKAN;
        item.Keterangan = bdo.KET;
        var strategi = new Strategi
        {
          Id = bdo.STRATEGI.STRATEGIKEY,
          Nomor = bdo.STRATEGI.NOSTRATEGI,
          Uraian = bdo.STRATEGI.URAISTRATEGI,
          Keterangan = bdo.STRATEGI.KET,
        };
        item.Strategi = strategi;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Kebijakan AddItem(Kebijakan item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("kebijakan", "bijakkey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);
        item.Id = cKey;
        var bdo = new KEBIJAKAN
        {
          STRATEGIKEY = item.Strategi.Id,
          BIJAKKEY = item.Id,
          NOBIJAKAN = item.Nomor,
          NMEBIJAKAN = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.KEBIJAKAN.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Kebijakan UpdateItem(string id, Kebijakan item)
    {
      try
      {
        var bdo = TrackedQuery.SingleOrDefault(e => e.BIJAKKEY == id);
        bdo.NOBIJAKAN = item.Nomor;
        bdo.NMEBIJAKAN = item.Uraian;
        bdo.KET = item.Keterangan;
        rkpdContext.KEBIJAKAN.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Kebijakan RemoveItem(Kebijakan item)
    {
      try
      {
        var bdo = new KEBIJAKAN
        {
          STRATEGIKEY = item.Strategi.Id,
          BIJAKKEY = item.Id,
          NOBIJAKAN = item.Nomor,
          NMEBIJAKAN = item.Uraian,
          KET = item.Keterangan
        };
        rkpdContext.KEBIJAKAN.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
