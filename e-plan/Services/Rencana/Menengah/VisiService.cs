using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class VisiService
  {
    public VisiService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private void SetError(Visi item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    public Visi[] GetItems()
    {
      int length;
      Visi[] items;
      try
      {
        var bdo = rkpdContext.VISI.AsNoTracking().ToArray();
        length = bdo.Length;
        items = new Visi[length];
        int i = 0;
        while (i < length)
        {
          var dto = new Visi
          {
            Id = bdo[i].IDVISI,
            Nomor = bdo[i].NOVISI,
            Uraian = bdo[i].NMVISI
          };
          items[i] = dto;
          i++;
        }
      }
      catch
      {
        items = new Visi[0];
      }
      return items;
    }

    public Visi GetItem(string id)
    {
      Visi item = new Visi
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.VISI.AsNoTracking().SingleOrDefault(e => e.IDVISI == id);
        item.Id = bdo.IDVISI;
        item.Nomor = bdo.NOVISI;
        item.Uraian = bdo.NMVISI;
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Visi AddItem(Visi item)
    {
      try
      {
        NEXTKEY nKey;
        var cKey = primaryKeyService.GetPrimaryKey("visi", "Idvisi", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;

        var bdo = new VISI
        {
          IDVISI = item.Id,
          NOVISI = item.Nomor,
          NMVISI = item.Uraian
        };

        rkpdContext.VISI.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Visi UpdateItem(string id, Visi item)
    {
      try
      {
        var bdo = rkpdContext.VISI.SingleOrDefault(e => e.IDVISI == id);
        bdo.NOVISI = item.Nomor;
        bdo.NMVISI = item.Uraian;
        rkpdContext.VISI.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }

    public Visi RemoveItem(Visi item)
    {
      try
      {
        var bdo = new VISI
        {
          IDVISI = item.Id,
          NOVISI = item.Nomor,
          NMVISI = item.Uraian
        };
        rkpdContext.VISI.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(item, e.Message);
      }
      return item;
    }
  }
}
