using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Rencana.Menengah
{
  public class ProgramService
  {
    public ProgramService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    private IQueryable<MPGRMRPJM> untrackedQuery;
    protected IQueryable<MPGRMRPJM> UntrackedQuery
    {
      get
      {
        untrackedQuery = rkpdContext.MPGRMRPJM.AsNoTracking();
        return untrackedQuery;
      }
    }

    private IQueryable<MPGRMRPJM> trackedQuery;
    protected IQueryable<MPGRMRPJM> TrackedQuery
    {
      get
      {
        trackedQuery = rkpdContext.MPGRMRPJM;
        return trackedQuery;
      }
    }

    private void SetError(ProgramRPJM item, string message)
    {
      item.IsError = true;
      var error = new ErrorMessage
      {
        Title = $"Error from action: {item.ActType}",
        Message = $"Message: {message}. Please contact your administrator."
      };
      item.Error = error;
    }

    private void BDOToDTO(out ProgramRPJM program, out Target target, MPGRMRPJM bdo)
    {
      program = new ProgramRPJM
      {
        Id = bdo.PGRMRPJMKEY,
        UnitKey = bdo.UNITKEY,
        Kode = bdo.NUPRGRM,
        Uraian = bdo.NMPRGRM,
        Indikator = bdo.INDIKATOR,
      };
      target = new Target
      {
        T1 = bdo.TARGET1,
        T2 = bdo.TARGET2,
        T3 = bdo.TARGET3,
        T4 = bdo.TARGET4,
        T5 = bdo.TARGET5
      };
      program.Target = target;
    }

    private void DTOToBDO(out MPGRMRPJM bdo, ProgramRPJM dto)
    {
      bdo = new MPGRMRPJM();
      bdo.PGRMRPJMKEY = dto.Id;
      bdo.UNITKEY = dto.UnitKey;
      bdo.NUPRGRM = dto.Kode;
      bdo.NMPRGRM = dto.Uraian;
      bdo.INDIKATOR = dto.Indikator;
      bdo.TARGET1 = dto.Target.T1;
      bdo.TARGET2 = dto.Target.T2;
      bdo.TARGET3 = dto.Target.T3;
      bdo.TARGET4 = dto.Target.T4;
      bdo.TARGET5 = dto.Target.T5;
    }

    public ProgramRPJM[] GetItems(string unitKey, int size, int page = 1)
    {
      ProgramRPJM[] items = new ProgramRPJM[size];
      ProgramRPJM program;
      Target target;
      try
      {
        var bdo = untrackedQuery
          .Include(e => e.DAFTUNIT)
          .Where(e => e.UNITKEY == unitKey)
          .Skip(page * size)
          .Take(size)
          .ToArray();
        int i = 0;
        while(i < size)
        {
          BDOToDTO(out program, out target, bdo[i]);
          items[i] = program;
          i++;
        }
      }
      catch
      {
        items = new ProgramRPJM[0];
      }
      finally
      {
        program = null;
        target = null;
      }
      return items;
    }

    public ProgramRPJM AddItem(ProgramRPJM dto)
    {
      MPGRMRPJM bdo;
      try
      {
        NEXTKEY nKey;
        string cKey = primaryKeyService.GetPrimaryKey("mpgrmrpjm", "pgrmrpjmkey", out nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);
        dto.Id = cKey;
        DTOToBDO(out bdo, dto);
        rkpdContext.MPGRMRPJM.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(dto, e.Message);
      }
      finally
      {
        bdo = null;
      }
      return dto;
    }

    public ProgramRPJM UpdateItem(string id, ProgramRPJM dto)
    {
      try
      {
        var bdo = trackedQuery.SingleOrDefault(e => e.PGRMRPJMKEY == id);
        bdo.NUPRGRM = dto.Kode;
        bdo.NMPRGRM = dto.Uraian;
        bdo.INDIKATOR = dto.Indikator;
        bdo.TARGET1 = dto.Target.T1;
        bdo.TARGET2 = dto.Target.T2;
        bdo.TARGET3 = dto.Target.T3;
        bdo.TARGET4 = dto.Target.T4;
        bdo.TARGET5 = dto.Target.T5;
        rkpdContext.MPGRMRPJM.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(dto, e.Message);
      }
      return dto;
    }

    public ProgramRPJM RemoveItem(ProgramRPJM dto)
    {
      MPGRMRPJM bdo;
      try
      {
        DTOToBDO(out bdo, dto);
        rkpdContext.MPGRMRPJM.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        SetError(dto, e.Message);
      }
      finally
      {
        bdo = null;
      }
      return dto;
    } 
  }
}
