using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services
{
  public interface IServiceBase<DTO>
  {
    List<DTO> GetCollection();
    DTO Create(DTO entity);
    DTO Read(string id);
    bool Update(DTO entity);
    bool Deletes(List<DTO> entities);
  }

  public class PrimaryKeyService
  {
    public PrimaryKeyService(RkpdContext rkpdContext)
    {
      _rkpdContext = rkpdContext;
    }

    private RkpdContext _rkpdContext;

    public string TrimKey(string key)
    {
      return key.Trim();
    }

    public string GetPrimaryKey(string tableId, string fieldId, out NEXTKEY nextKey)
    {
      string key = String.Empty;
      nextKey = _rkpdContext.NEXTKEY.Where(e => e.TABLEID.Contains(tableId) && e.FIELDID.Contains(fieldId)).SingleOrDefault();
      if (nextKey != null)
      {
        key = TrimKey(nextKey.NEXTKEYs);
      }
      return key;
    }

    public string GeneratePrimaryKey(string currentKey)
    {
      int key = int.Parse(currentKey.Substring(0, currentKey.Length - 1)) + 1;
      return string.Format("{0}_", key);
    }
  }

  public class TahunService : IServiceBase<Tahun>
  {
    public TahunService(RkpdContext rkpdContext)
    {
      this.rkpdContext = rkpdContext;
    }

    private RkpdContext rkpdContext;

    public Tahun Create(Tahun entity)
    {
      throw new NotImplementedException();
    }

    public bool Deletes(List<Tahun> entities)
    {
      throw new NotImplementedException();
    }

    public List<Tahun> GetCollection()
    {
      throw new NotImplementedException();
    }

    public Tahun Read(string id)
    {
      var blo = rkpdContext.TAHUN.Single(e => e.KDTAHUN == id);
      var dto = new Tahun
      {
        Id = blo.KDTAHUN,
        Nama = blo.NMTAHUN
      };
      return dto;
    }

    public bool Update(Tahun entity)
    {
      throw new NotImplementedException();
    }
  }
}
