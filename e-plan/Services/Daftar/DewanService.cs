using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class DewanService
  {
    public DewanService(RkpdContext rkpdContext, UnitOrganisasiService unitOrganisasiService, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.unitOrganisasiService = unitOrganisasiService;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private UnitOrganisasiService unitOrganisasiService;
    private PrimaryKeyService primaryKeyService;

    public Dewan GetItem(string id)
    {
      Dewan item = new Dewan
      {
        ActType = Models.ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.DAFTDPRD.SingleOrDefault(e => e.IDDPRD == id);
        item.Id = bdo.IDDPRD;
        item.Pagu = bdo.PAGU;
        item.Fraksi = bdo.AKROUNIT;
        item.UnitOrganisasi = unitOrganisasiService.AutoGetItem(bdo.UNITKEY);
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Dewan AddItem(Dewan item)
    {
      try
      {
        var cKey = primaryKeyService.GetPrimaryKey("daftdprd", "iddprd", out NEXTKEY nKey);
        nKey.NEXTKEYs = cKey;

        item.Id = cKey;
        var bdo = new DAFTDPRD
        {
          IDDPRD = item.Id,
          AKROUNIT = item.Fraksi,
          PAGU = item.Pagu,
          UNITKEY = item.UnitOrganisasi.Id
        };

        rkpdContext.DAFTDPRD.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Dewan UpdateItem(string id, Dewan item)
    {
      try
      {
        var bdo = rkpdContext.DAFTDPRD.SingleOrDefault(e => e.IDDPRD == id);
        bdo.AKROUNIT = item.Fraksi;
        bdo.PAGU = item.Pagu;
        rkpdContext.DAFTDPRD.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Dewan RemoveItem(Dewan item)
    {
      try
      {
        var bdo = new DAFTDPRD
        {
          IDDPRD = item.Id,
          PAGU = item.Pagu,
          AKROUNIT = item.Fraksi,
          UNITKEY = item.UnitOrganisasi.Id
        };
        rkpdContext.DAFTDPRD.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
