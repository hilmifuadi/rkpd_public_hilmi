using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Eplan.Services.Jenis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class PegawaiService
  {
    public PegawaiService(
      RkpdContext rkpdContext,
      UnitOrganisasiService unitOrganisasiService,
      GolonganService golonganService)
    {
      this.rkpdContext = rkpdContext;
      this.unitOrganisasiService = unitOrganisasiService;
      this.golonganService = golonganService;
    }

    private RkpdContext rkpdContext;
    private UnitOrganisasiService unitOrganisasiService;
    private GolonganService golonganService;

    public Pegawai GetItem(string id)
    {
      Pegawai item = new Pegawai
      {
        ActType = Models.ActType.GET_ITEM        
      };
      try
      {
        var bdo = rkpdContext.PEGAWAI.SingleOrDefault(e => e.NIP == id);
        item.Nip = bdo.NIP;
        item.Nama = bdo.NAMA;
        item.Alamat = bdo.ALAMAT;
        item.Pendidikan = bdo.PDDK;
        item.Jabatan = bdo.JABATAN;
        item.Golongan = golonganService.Read(bdo.KDGOL);
        item.UnitOrganisasi = unitOrganisasiService.AutoGetItem(bdo.UNITKEY);
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Pegawai AddItem(Pegawai item)
    {
      try
      {
        var bdo = new PEGAWAI
        {
          NIP = item.Nip,
          NAMA = item.Nama,
          ALAMAT = item.Alamat,
          JABATAN = item.Jabatan,
          PDDK = item.Pendidikan,
          KDGOL = item.Golongan.Kode,
          UNITKEY = item.UnitOrganisasi.Id
        };
        rkpdContext.PEGAWAI.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Pegawai UpdateItem(string id, Pegawai item)
    {
      try
      {
        var bdo = rkpdContext.PEGAWAI.SingleOrDefault(e => e.NIP == id);
        bdo.NIP = item.Nip;
        bdo.NAMA = item.Nama;
        bdo.JABATAN = item.Jabatan;
        bdo.ALAMAT = item.Alamat;
        bdo.PDDK = item.Pendidikan;
        bdo.KDGOL = item.Golongan.Kode;
        bdo.UNITKEY = item.UnitOrganisasi.Id;
        rkpdContext.PEGAWAI.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Pegawai RemoveItem(Pegawai item)
    {
      try
      {
        var bdo = new PEGAWAI
        {
          NIP = item.Nip,
          NAMA = item.Nama,
          ALAMAT = item.Alamat,
          JABATAN = item.Jabatan,
          PDDK = item.Pendidikan,
          KDGOL = item.Golongan.Kode,
          UNITKEY = item.UnitOrganisasi.Id
        };
        rkpdContext.PEGAWAI.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch (Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
