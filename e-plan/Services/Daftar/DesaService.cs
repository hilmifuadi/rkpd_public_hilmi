using Eplan.Controllers;
using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class DesaService
  {
    public DesaService(
      RkpdContext rkpdContext,
      PrimaryKeyService primaryKeyService,
      UnitOrganisasiService unitOrganisasiService,
      KecamatanService kecamatanService,
      TahunService tahunService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
      this.unitOrganisasiService = unitOrganisasiService;
      this.kecamatanService = kecamatanService;
      this.tahunService = tahunService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;
    private UnitOrganisasiService unitOrganisasiService;
    private KecamatanService kecamatanService;
    private TahunService tahunService;

    private DAFTDESA TranslateDTOtoBLO(Desa dto)
    {
      var blo = new DAFTDESA
      {
        IDDESA = dto.Id,
        PAGU = dto.Pagu,
        IDKEC = dto.KecamatanId,
        UNITKEY = dto.UnitOrganisasiId,
        KDTAHUN = dto.TahunId
      };
      return blo;
    }

    public Desa[] GetItems()
    {
      Desa[] items;
        var blo = rkpdContext.DAFTDESA.AsNoTracking().ToArray();
        int index = blo.Length;
        items = new Desa[index];
        for (var i = 0; i < index; i++)
        {
          var desa = new Desa
          {
            Id = blo[i].IDDESA,
            Pagu = blo[i].PAGU,
            KecamatanId = blo[i].IDKEC,
            TahunId = blo[i].KDTAHUN,
            UnitOrganisasiId = blo[i].UNITKEY
          };
          desa.UnitOrganisasi = unitOrganisasiService.AutoGetItem(desa.UnitOrganisasiId);
          desa.Kecamatan = kecamatanService.Read(desa.KecamatanId);
          desa.Tahun = tahunService.Read(desa.TahunId);  
          items[i] = desa;
        }
      return items;
    }

    public Desa AddItem(Desa item)
    {
      try
      {
        var blo = TranslateDTOtoBLO(item);
        rkpdContext.DAFTDESA.Add(blo);
        rkpdContext.SaveChanges();
      }
      catch (Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Desa RemoveItem(Desa item)
    {
      try
      {
        var blo = TranslateDTOtoBLO(item);
        rkpdContext.DAFTDESA.Remove(blo);
        rkpdContext.SaveChanges();
      }
      catch (Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
