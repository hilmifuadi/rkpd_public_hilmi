using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class KecamatanService : IServiceBase<Kecamatan>
  {
    public KecamatanService(RkpdContext rkpdContext, PrimaryKeyService keyService, UnitOrganisasiService unitOrganisasiService)
    {
      this.rkpdContext = rkpdContext;
      this.keyService = keyService;
      this.unitOrganisasiService = unitOrganisasiService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService keyService;
    private UnitOrganisasiService unitOrganisasiService;

    public List<Kecamatan> GetCollection()
    {
      var bdo = rkpdContext.DAFTKEC
        .AsNoTracking()
        .ToList();
      var items = new List<Kecamatan>();
      foreach(var item in bdo)
      {
        var dto = new Kecamatan
        {
          Id = item.IDKEC,
          UnitOrganisasiKey = item.UNITKEY
        };
        dto.UnitOrganisasi = unitOrganisasiService.AutoGetItem(dto.UnitOrganisasiKey);
        items.Add(dto);
      }
      return items;
    }

    public DAFTKEC TranslateDTOToBLO(Kecamatan kecamatan)
    {
      var bdo = new DAFTKEC
      {
        IDKEC = kecamatan.Id,
        UNITKEY = kecamatan.UnitOrganisasiKey
      };

      return bdo;
    }

    public Kecamatan Create(Kecamatan entity)
    {
      var cKey = keyService.GetPrimaryKey("daftkec", "Idkec", out NEXTKEY nKey);
      nKey.NEXTKEYs = keyService.GeneratePrimaryKey(cKey);
      entity.Id = cKey;
      rkpdContext.DAFTKEC.Add(TranslateDTOToBLO(entity));
      rkpdContext.SaveChanges();
      entity.UnitOrganisasi = unitOrganisasiService.AutoGetItem(entity.UnitOrganisasiKey);
      return entity;
    }

    public Kecamatan Read(string id)
    {
      Kecamatan item;
      try
      {
        var bdo = rkpdContext.DAFTKEC.AsNoTracking().SingleOrDefault(e => e.IDKEC.Trim() == id.Trim());
        item = new Kecamatan
        {
          Id = bdo.IDKEC,
          UnitOrganisasiKey = bdo.UNITKEY
        };
        item.UnitOrganisasi = unitOrganisasiService.AutoGetItem(item.UnitOrganisasiKey);
      }
      catch(Exception e)
      {
        item = new Kecamatan
        {
          IsError = true
        };
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public bool Update(Kecamatan entity)
    {
      return rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(Kecamatan kecamatan)
    {
      var old = rkpdContext.DAFTKEC.Single(e => e.IDKEC == kecamatan.Id);
      if (old != null)
      {
        rkpdContext.DAFTKEC.Remove(old);
      }
      return rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<Kecamatan> entities)
    {
      var items = new List<DAFTKEC>();
      foreach (var entity in entities)
      {
        items.Add(rkpdContext.DAFTKEC.Single(e => e.IDKEC == entity.Id));
      }
      rkpdContext.DAFTKEC.RemoveRange(items);
      return rkpdContext.SaveChanges() > 0 ? true : false;
    }
  }
}
