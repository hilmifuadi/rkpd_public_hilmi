using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class SasaranPembangunanService : IServiceBase<SasaranPembangunan>
  {
    public SasaranPembangunanService(RkpdContext context, PrimaryKeyService keyService, PrioritasPembangunanService prioritasPembangunanService)
    {
      this.context = context;
      this.keyService = keyService;
      this.prioritasPembangunanService = prioritasPembangunanService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;
    private PrioritasPembangunanService prioritasPembangunanService;

    private SasaranPembangunan TranslateBLOToDTO(SASARANTHN blo)
    {
      var dto = new SasaranPembangunan
      {
        Id = blo.IDSAS,
        Nomor = blo.NOSAS,
        Sasaran = blo.NMSAS,
        Keterangan = blo.KET,
        PrioritasPembangunanKey = blo.PRIOPPASKEY,
        PrioritasPembangunan = new PrioritasPembangunan
        {
          Id = blo.PRIOPPAS.PRIOPPASKEY,
          Nomor = blo.PRIOPPAS.NOPRIOPPAS,
          Nama = blo.PRIOPPAS.NMPRIOPPAS,
          Sasaran = blo.PRIOPPAS.SASARAN,
          Keterangan = blo.PRIOPPAS.KET
        },
      };
      return dto;
    }

    private SASARANTHN TranslateDTOToBLO(SasaranPembangunan sasaranPembangunan)
    {
      return new SASARANTHN
      {
        IDSAS = sasaranPembangunan.Id,
        NOSAS = sasaranPembangunan.Nomor,
        NMSAS = sasaranPembangunan.Sasaran,
        KET = sasaranPembangunan.Keterangan,
        PRIOPPASKEY = sasaranPembangunan.PrioritasPembangunan.Id.Trim()
      };
    }

    public List<SasaranPembangunan> GetCollection()
    {
      return context.SASARANTHN
        .Select(e => new SasaranPembangunan
        {
          Id = e.IDSAS,
          Nomor = e.NOSAS,
          Sasaran = e.NMSAS,
          Keterangan = e.KET,
          PrioritasPembangunanKey = e.PRIOPPASKEY,
          PrioritasPembangunan = new PrioritasPembangunan
          {
            Id = e.PRIOPPAS.PRIOPPASKEY,
            Nomor = e.PRIOPPAS.NOPRIOPPAS,
            Nama = e.PRIOPPAS.NMPRIOPPAS,
            Sasaran = e.PRIOPPAS.SASARAN,
            Keterangan = e.PRIOPPAS.KET
          },
        })
        .OrderBy(e => e.Id)
        .AsNoTracking()
        .ToList();
    }

    public SasaranPembangunan Create(SasaranPembangunan entity)
    {
      var cKey = keyService.GetPrimaryKey("Sasaranthn", "Idsas", out NEXTKEY nKey);
      nKey.NEXTKEYs = keyService.GeneratePrimaryKey(cKey);
      entity.Id = cKey;
      var bdo = TranslateDTOToBLO(entity);
      context.SASARANTHN.Add(bdo);
      context.SaveChanges();
      return entity;
    }

    public SasaranPembangunan Read(string id)
    {
      return context.SASARANTHN
        .Where(e => e.IDSAS == id)
        .Select(e => TranslateBLOToDTO(e))
        .OrderBy(e => e.Nomor)
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(SasaranPembangunan entity)
    {
      var old = context.SASARANTHN.Where(e => e.IDSAS == entity.Id).SingleOrDefault();
      old.NOSAS = entity.Nomor;
      old.NMSAS = entity.Sasaran;
      old.KET = entity.Keterangan;
      context.SASARANTHN.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<SasaranPembangunan> entities)
    {
      var items = new List<SASARANTHN>();
      foreach (var i in entities)
      {
        items.Add(context.SASARANTHN.Where(e => e.IDSAS == i.Id).SingleOrDefault());
      }
      context.SASARANTHN.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(SasaranPembangunan sasaranPembangunan)
    {
      var item = context.SASARANTHN.Where(e => e.IDSAS == sasaranPembangunan.Id).SingleOrDefault();
      context.SASARANTHN.Remove(item);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
