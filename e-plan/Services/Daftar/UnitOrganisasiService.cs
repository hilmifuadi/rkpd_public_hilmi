using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Eplan.Services.Jenis;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class UnitOrganisasiBuilder
  {
    public UnitOrganisasiBuilder(RkpdContext rkpdContext, StrukturUnitService strukturUnitService)
    {
      this.rkpdContext = rkpdContext;
      this.strukturUnitService = strukturUnitService;
    }

    RkpdContext rkpdContext;
    StrukturUnitService strukturUnitService;

    private const int level = 1;
    private int Level { get => level; }

    private int MaxLevel
    {
      get
      {
        return strukturUnitService.GetCollection().Count;
      }
    }

    private List<UnitOrganisasi> Items
    {
      get
      {
        var items = rkpdContext.DAFTUNIT.Select(e => new UnitOrganisasi
        {
          Id = e.UNITKEY,
          Kode = e.KDUNIT,
          Nama = e.NMUNIT,
          Akronim = e.AKROUNIT,
          Alamat = e.ALAMAT,
          Telepon = e.TELEPON,
          StrukturRekeningKey = e.KDLEVEL,
          StatusUnitKey = e.KDJKEC
        })
        .AsNoTracking()
        .ToList();
        return items;
      }
    }

    public List<UnitOrganisasi> Build()
    {
      var items = rkpdContext.DAFTUNIT
        .Where(e => int.Parse(e.KDLEVEL.Trim()) == Level)
        .OrderBy(e => e.KDUNIT)
        .Select(e => new UnitOrganisasi
        {
          Id = e.UNITKEY,
          Kode = e.KDUNIT,
          Nama = e.NMUNIT,
          Akronim = e.AKROUNIT,
          Alamat = e.ALAMAT,
          Telepon = e.TELEPON,
          StrukturRekeningKey = e.KDLEVEL,
          StatusUnitKey = e.KDJKEC
        })
        .AsNoTracking()
        .ToList();

      foreach (var item in items)
      {
        Build(item);
      }

      return items;
    }

    public void Build(UnitOrganisasi unitOrganisasi, int level = 1)
    {
      if (level != MaxLevel)
      {
        unitOrganisasi.Unit.AddRange(
          Items.Where(e =>
          {
            return
              e.StrukturRekeningKey.Trim() == (level + 1).ToString() &&
              e.Kode.Trim().Substring(0, unitOrganisasi.Kode.Trim().Length) == unitOrganisasi.Kode.Trim();
          })
          .OrderBy(e => e.Kode)
          .ToList());
        foreach (var i in unitOrganisasi.Unit)
        {
          Build(i, level + 1);
        }
      }
    }
  }

  public class UnitOrganisasiService
  {
    public UnitOrganisasiService(RkpdContext rkpdContext, PrimaryKeyService keyService, UnitOrganisasiBuilder builder)
    {
      this.rkpdContext = rkpdContext;
      this.keyService = keyService;
      this.builder = builder;
    }

    RkpdContext rkpdContext;
    PrimaryKeyService keyService;
    UnitOrganisasiBuilder builder;

    public List<UnitOrganisasi> Get(int level, string unit)
    {
      string query = "EXEC FetchOrganisasi {0}, {1}";
      var items = rkpdContext.DAFTUNIT
        .FromSql(query, level, unit)
        .AsNoTracking()
        .ToList();

      var data = new List<UnitOrganisasi>();

      foreach (var e in items)
      {
        data.Add(new UnitOrganisasi
        {
          Id = e.UNITKEY,
          Kode = e.KDUNIT,
          Nama = e.NMUNIT,
          Akronim = e.AKROUNIT,
          Alamat = e.ALAMAT,
          Telepon = e.TELEPON,
          Tipe = e.TYPE,
          StatusUnitKey = e.KDJKEC,
          StrukturRekeningKey = e.KDLEVEL
        });
      }

      return data;
    }

    public List<UnitOrganisasi> GetCollection()
    {
      return builder.Build();
    }

    //Do StatusUnit and Struktur Rekening must to be filled?
    public UnitOrganisasi AutoGetItem(string id)
    {
      UnitOrganisasi item = new UnitOrganisasi
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.DAFTUNIT.AsNoTracking().SingleOrDefault(e => e.UNITKEY.Trim() == id.Trim());
        item.Id = bdo.UNITKEY;
        item.Kode = bdo.KDUNIT;
        item.Nama = bdo.NMUNIT;
        item.Akronim = bdo.AKROUNIT;
        item.Alamat = bdo.ALAMAT;
        item.Telepon = bdo.TELEPON;
        item.Tipe = bdo.TYPE;
        item.StatusUnitKey = bdo.KDJKEC;
        item.StrukturRekeningKey = bdo.KDLEVEL;
      }
      catch (Exception e)
      {
        item = new UnitOrganisasi
        {
          IsError = true
        };
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public UnitOrganisasi AddItem(UnitOrganisasi item)
    {
      try
      {
        var ckey = keyService.GetPrimaryKey("daftunit", "unitkey", out NEXTKEY nkey);
        nkey.NEXTKEYs = keyService.GeneratePrimaryKey(ckey);
        item.Id = ckey;
        var bdo = new DAFTUNIT
        {
          UNITKEY = item.Id,
          KDUNIT = item.Kode,
          NMUNIT = item.Nama,
          AKROUNIT = item.Akronim,
          ALAMAT = item.Alamat,
          TELEPON = item.Telepon,
          KDJKEC = item.StatusUnitKey,
          KDLEVEL = item.StrukturRekeningKey,
        };
        rkpdContext.DAFTUNIT.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch (Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public UnitOrganisasi UpdateItem(string id, UnitOrganisasi recent)
    {
      try
      {
        var bdo = rkpdContext.DAFTUNIT.SingleOrDefault(e => e.UNITKEY == id);

        bdo.KDUNIT = recent.Kode;
        bdo.NMUNIT = recent.Nama;
        bdo.AKROUNIT = recent.Akronim;
        bdo.ALAMAT = recent.Alamat;
        bdo.TELEPON = recent.Telepon;
        bdo.KDJKEC = recent.StatusUnitKey;
        bdo.KDLEVEL = recent.StrukturRekeningKey;
        
        rkpdContext.DAFTUNIT.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        recent.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {recent.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        recent.Error = error;
      }
      return recent;
    }

    public UnitOrganisasi RemoveItem(UnitOrganisasi item)
    {
      try
      {
        var bdo = new DAFTUNIT
        {
          UNITKEY = item.Id,
          KDUNIT = item.Kode,
          NMUNIT = item.Nama,
          AKROUNIT = item.Akronim,
          ALAMAT = item.Alamat,
          TELEPON = item.Telepon,
          KDJKEC = item.StatusUnitKey,
          KDLEVEL = item.StrukturRekeningKey
        };
        rkpdContext.DAFTUNIT.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
