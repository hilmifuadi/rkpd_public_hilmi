using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class UrusanUnitService
  {
    public UrusanUnitService(RkpdContext rkpdContext)
    {
      this.rkpdContext = rkpdContext;
    }

    private RkpdContext rkpdContext;

    public UrusanUnit Getitem(UrusanUnit item, string unitId, string urusanId)
    {
      try
      {
        var bdo = rkpdContext.URUSANUNIT
          .Include(e => e.UNIT)
          .Include(e => e.URUSAN)
          .SingleOrDefault(e => e.UNITKEY == unitId && e.URUSKEY == urusanId);

        var unit = new UnitOrganisasi {
          Id = bdo.UNIT.UNITKEY,
          Kode = bdo.UNIT.KDUNIT,
          Nama = bdo.UNIT.NMUNIT,
          Akronim = bdo.UNIT.AKROUNIT,
          Alamat = bdo.UNIT.ALAMAT,
          Telepon = bdo.UNIT.TELEPON,
        };

        item.Unit = unit;

        int length = bdo.URUSAN.Count;
        item.Urusan = new UnitOrganisasi[length];
        for (int i = 0; i < length; i++)
        {
          UnitOrganisasi urus = new UnitOrganisasi
          {
            Id = bdo.URUSAN[i].UNITKEY,
            Kode = bdo.URUSAN[i].KDUNIT,
            Nama = bdo.URUSAN[i].NMUNIT,
            Akronim = bdo.URUSAN[i].AKROUNIT,
            Alamat = bdo.URUSAN[i].ALAMAT,
            Telepon = bdo.URUSAN[i].TELEPON,
          };
          item.Urusan[i] = urus;
        }        
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public UrusanUnit AddItem(UrusanUnit item, string unitId, string[] urusanId)
    {
      try
      {
        int length = urusanId.Length;
        URUSANUNIT[] items = new URUSANUNIT[length];
        for(int i = 0; i < length; i++)
        {
          var bdo = new URUSANUNIT
          {
            UNITKEY = unitId,
            URUSKEY = urusanId[i]
          };
          items[i] = bdo;
        }
        rkpdContext.URUSANUNIT.AddRange(items);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public UrusanUnit RemoveItem(UrusanUnit item, string unitId, string[] urusanId)
    {
      try
      {
        int length = urusanId.Length;
        URUSANUNIT[] items = new URUSANUNIT[length];
        for(int i = 0; i < length; i++)
        {
          var bdo = new URUSANUNIT
          {
            UNITKEY = unitId,
            URUSKEY = urusanId[i]
          };
          items[i] = bdo;
        }
        rkpdContext.URUSANUNIT.RemoveRange(items);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
