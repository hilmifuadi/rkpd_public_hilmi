using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class PrioritasPembangunanService : IServiceBase<PrioritasPembangunan>
  {
    public PrioritasPembangunanService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      context = rkpdContext;
      keyService = primaryKeyService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;

    public List<PrioritasPembangunan> GetCollection()
    {
      return context.PRIOPPAS
        .Select(e => new PrioritasPembangunan
        {
          Id = e.PRIOPPASKEY,
          Nomor = e.NOPRIOPPAS,
          Nama = e.NMPRIOPPAS,
          Sasaran = e.SASARAN,
          Keterangan = e.KET
        })
        .OrderBy(e => e.Nomor)
        .AsNoTracking()
        .ToList();
    }

    public PrioritasPembangunan Create(PrioritasPembangunan entity)
    {
      string cKey = keyService.GetPrimaryKey("prioppas", "prioppaskey", out NEXTKEY nKey);
      nKey.NEXTKEYs = keyService.GeneratePrimaryKey(cKey);

      entity.Id = cKey;
      context.PRIOPPAS.Add(new PRIOPPAS
      {
        PRIOPPASKEY = entity.Id,
        NOPRI = entity.Nomor,
        NOPRIOPPAS = entity.Nomor,
        NMPRIOPPAS = entity.Nama,
        SASARAN = entity.Sasaran,
        KET = entity.Keterangan
      });
      context.SaveChanges();
      return entity;
    }

    public PrioritasPembangunan Read(string id)
    {
      return context.PRIOPPAS
        .Where(e => e.PRIOPPASKEY == id)
        .Select(e => new PrioritasPembangunan
        {
          Id = e.PRIOPPASKEY,
          Nomor = e.NOPRIOPPAS,
          Nama = e.NMPRIOPPAS,
          Sasaran = e.SASARAN,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(PrioritasPembangunan entity)
    {
      var old = context.PRIOPPAS.Where(e => e.PRIOPPASKEY == entity.Id).SingleOrDefault();
      old.NOPRI = entity.Nomor;
      old.NOPRIOPPAS = entity.Nomor;
      old.NMPRIOPPAS = entity.Nama;
      old.SASARAN = entity.Sasaran;
      old.KET = entity.Keterangan;
      context.PRIOPPAS.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(PrioritasPembangunan prioritasPembangunan)
    {
      var item = context.PRIOPPAS.Where(e => e.PRIOPPASKEY == prioritasPembangunan.Id).Single();
      if(item != null)
      {
        context.PRIOPPAS.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<PrioritasPembangunan> entities)
    {
      var items = new List<PRIOPPAS>();
      foreach (var item in entities)
      {
        items.Add(context.PRIOPPAS.Where(e => e.PRIOPPASKEY == item.Id).SingleOrDefault());
      }
      context.PRIOPPAS.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
