using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class PrioritasProvinsiService : IServiceBase<PrioritasProvinsi>
  {
    public PrioritasProvinsiService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      context = rkpdContext;
      keyService = primaryKeyService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;

    public List<PrioritasProvinsi> GetCollection()
    {
      return context.PRIOPROV
        .Select(e => new PrioritasProvinsi
        {
          Id = e.PRIOPROVKEY,
          Nomor = e.NUPRIO,
          Nama = e.NMPRIO,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .ToList();
    }

    public PrioritasProvinsi Create(PrioritasProvinsi entity)
    {
      string cKey = keyService.GetPrimaryKey("prioprov", "prioprovkey", out NEXTKEY nKey);
      nKey.NEXTKEYs = keyService.GeneratePrimaryKey(cKey);

      entity.Id = cKey;
      context.PRIOPROV.Add(new PRIOPROV
      {
        PRIOPROVKEY = entity.Id,
        NUPRIO = entity.Nomor,
        NMPRIO = entity.Nama,
        KET = entity.Keterangan
      });
      context.SaveChanges();
      return entity;
    }

    public PrioritasProvinsi Read(string id)
    {
      return context.PRIOPROV
        .Where(e => e.PRIOPROVKEY == id)
        .Select(e => new PrioritasProvinsi
        {
          Id = e.PRIOPROVKEY,
          Nomor = e.NUPRIO,
          Nama = e.NMPRIO,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(PrioritasProvinsi entity)
    {
      var old = context.PRIOPROV.Where(e => e.PRIOPROVKEY == entity.Id).SingleOrDefault();
      old.NUPRIO = entity.Nomor;
      old.NMPRIO = entity.Nama;
      old.KET = entity.Keterangan;
      context.PRIOPROV.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(PrioritasProvinsi entity)
    {
      var item = context.PRIOPROV.Where(e => e.PRIOPROVKEY == entity.Id).Single();
      if(item != null)
      {
        context.PRIOPROV.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<PrioritasProvinsi> entities)
    {
      var items = new List<PRIOPROV>();
      foreach (var item in entities)
      {
        items.Add(context.PRIOPROV.Where(e => e.PRIOPROVKEY == item.Id).SingleOrDefault());
      }
      context.PRIOPROV.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
