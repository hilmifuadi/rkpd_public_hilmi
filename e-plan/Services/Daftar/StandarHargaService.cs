using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Eplan.Services.Jenis;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class StandarHargaService : IServiceBase<StandarHarga>
  {
    public StandarHargaService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService, JenisStandarService jenisStandarService)
    {
      context = rkpdContext;
      keyService = primaryKeyService;
      this.jenisStandarService = jenisStandarService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;
    private JenisStandarService jenisStandarService;

    public List<StandarHarga> GetCollection()
    {
      return context.STDHARGA
        .Select(e => new StandarHarga
        {
          Id = e.IDSTDHARGA,
          Nomor = e.NOSTD,
          Nama = e.NMSTD,
          Spesifikasi = e.SPEKSTD,
          Merk = e.MERKSTD,
          Harga = e.HRGSTD,
          Satuan = e.SATUAN,
          Keterangan = e.KET,
          IdJenisStandar = e.KDJNSSTD,
          JenisStandar = new JenisStandar
          {
            Kode = e.JSTANDAR.KDJNSSTD,
            Nama = e.JSTANDAR.NMJNSSTD,
            Keterangan = e.JSTANDAR.KET
          }
        })
        .OrderBy(e => e.Nomor)
        .AsNoTracking()
        .ToList();
    }

    public StandarHarga Create(StandarHarga entity)
    {
      var cKey = keyService.GetPrimaryKey("stdharga", "idstdharga", out NEXTKEY nKey);
      nKey.NEXTKEYs = keyService.GeneratePrimaryKey(cKey);

      entity.Id = cKey;
      entity.IdJenisStandar = entity.JenisStandar.Kode;
      context.STDHARGA.Add(new STDHARGA
      {
        IDSTDHARGA = entity.Id,
        NOSTD = entity.Nomor,
        NMSTD = entity.Nama,
        SATUAN = entity.Satuan,
        MERKSTD = entity.Merk,
        HRGSTD = entity.Harga,
        SPEKSTD = entity.Spesifikasi,
        KET = entity.Keterangan,
        KDJNSSTD = entity.IdJenisStandar,
      });

      context.SaveChanges();

      return entity;
    }

    public StandarHarga Read(string id)
    {
      return context.STDHARGA
        .Where(e => e.IDSTDHARGA == id)
        .Select(e => new StandarHarga
        {
          Id = e.IDSTDHARGA,
          Kode = e.KDJNSSTD,
          Nomor = e.NOSTD,
          Nama = e.NMSTD,
          Spesifikasi = e.SPEKSTD,
          Merk = e.MERKSTD,
          Harga = e.HRGSTD,
          Satuan = e.SATUAN,
          Keterangan = e.KET,
          IdJenisStandar = e.KDJNSSTD,
          JenisStandar = new JenisStandar
          {
            Kode = e.JSTANDAR.KDJNSSTD,
            Nama = e.JSTANDAR.NMJNSSTD,
            Keterangan = e.JSTANDAR.KET
          }
        })
        .OrderBy(e => e.Nomor)
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(StandarHarga entity)
    {
      var item = context.STDHARGA.Where(e => e.IDSTDHARGA == entity.Id).Single();

      item.NOSTD = entity.Nomor;
      item.NMSTD = entity.Nama;
      item.SPEKSTD = entity.Spesifikasi;
      item.SATUAN = entity.Satuan;
      item.MERKSTD = entity.Merk;
      item.HRGSTD = entity.Harga;
      item.KET = entity.Keterangan;

      context.STDHARGA.Update(item);

      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<StandarHarga> entities)
    {
      var items = new List<STDHARGA>();
      foreach(var entiity in entities)
      {
        items.Add(context.STDHARGA.Where(e => e.IDSTDHARGA == entiity.Id).Single());
      }
      context.STDHARGA.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(StandarHarga standarHarga)
    {
      var item = context.STDHARGA.Where(e => e.IDSTDHARGA == standarHarga.Id).SingleOrDefault();
      context.STDHARGA.Remove(item);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
