using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class KepalaSKPDService
  {
    public KepalaSKPDService(RkpdContext rkpdContext)
    {
      this.rkpdContext = rkpdContext;
    }

    private RkpdContext rkpdContext;

    public KepalaSKPD GetItem(string id)
    {
      KepalaSKPD item = new KepalaSKPD
      {
        ActType = Models.ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.ATASBEND
          .Include(e => e.PEGAWAI)
            .ThenInclude(e => e.GOLONGAN)
          .Include(e => e.DAFTUNIT)
          .AsNoTracking()
          .SingleOrDefault(e => e.UNITKEY == id);

        var pegawai = new Pegawai
        {
          Nip = bdo.PEGAWAI.NIP,
          Nama = bdo.PEGAWAI.NAMA,
          Alamat = bdo.PEGAWAI.ALAMAT,
          Pendidikan = bdo.PEGAWAI.PDDK,
          Jabatan = bdo.PEGAWAI.JABATAN
        };

        var golongan = new Golongan
        {
          Kode = bdo.PEGAWAI.GOLONGAN.KDGOL,
          Nama = bdo.PEGAWAI.GOLONGAN.NMGOL,
          Pangkat = bdo.PEGAWAI.GOLONGAN.PANGKAT
        };

        pegawai.Golongan = golongan;

        var unit = new UnitOrganisasi
        {
          Id = bdo.DAFTUNIT.UNITKEY,
          Kode = bdo.DAFTUNIT.KDUNIT,
          Nama = bdo.DAFTUNIT.NMUNIT,
          Akronim = bdo.DAFTUNIT.AKROUNIT,
          Alamat = bdo.DAFTUNIT.ALAMAT,
          Telepon = bdo.DAFTUNIT.TELEPON
        };

        item.Pegawai = pegawai;
        item.Unit = unit;
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public KepalaSKPD AddItem(KepalaSKPD item)
    {
      try
      {
        var bdo = new ATASBEND
        {
          UNITKEY = item.Unit.Id,
          NIP = item.Pegawai.Nip
        };
        rkpdContext.ATASBEND.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public KepalaSKPD RemoveItem(KepalaSKPD item)
    {
      try
      {
        var bdo = new ATASBEND
        {
          UNITKEY = item.Unit.Id,
          NIP = item.Pegawai.Nip
        };
        rkpdContext.ATASBEND.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
