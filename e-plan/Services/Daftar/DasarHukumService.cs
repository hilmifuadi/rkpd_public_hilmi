using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class DasarHukumService : IServiceBase<DasarHukum>
  {
    public DasarHukumService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      context = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext context;
    private PrimaryKeyService primaryKeyService;

    public List<DasarHukum> GetCollection()
    {
      return context.DASKUM
        .Select(e => new DasarHukum
        {
            Id = e.DASKUMKEY,
            Nomor = e.NODASKUM,
            Isi = e.ISIDASKUM
        })
        .AsNoTracking()
        .ToList();
    }

    public DasarHukum Create(DasarHukum entity)
    {
      var cKey = primaryKeyService.GetPrimaryKey("daskum", "daskumkey", out NEXTKEY nKey);
      nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

      entity.Id = cKey;
      context.DASKUM.Add(new DASKUM
      {
        DASKUMKEY = entity.Id,
        NODASKUM = entity.Nomor,
        ISIDASKUM = entity.Isi
      });
      context.SaveChanges();
      return entity;
    }

    public DasarHukum Read(string id)
    {
      return context.DASKUM
        .Where(e => e.DASKUMKEY == id)
        .Select(e => new DasarHukum
        {
          Id = e.DASKUMKEY,
          Nomor = e.NODASKUM,
          Isi = e.ISIDASKUM
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(DasarHukum entity)
    {
      var old = context.DASKUM.Where(e => e.DASKUMKEY == entity.Id).SingleOrDefault();
      old.NODASKUM = entity.Nomor;
      old.ISIDASKUM = entity.Isi;
      context.DASKUM.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(DasarHukum dasarHukum)
    {
      var item = context.DASKUM.Where(e => e.DASKUMKEY == dasarHukum.Id).Single();
      if(item != null)
      {
        context.DASKUM.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<DasarHukum> entities)
    {
      var items = new List<DASKUM>();
      foreach(var item in entities)
      {
        items.Add(context.DASKUM.Where(e => e.DASKUMKEY == item.Id).SingleOrDefault());
      }
      context.DASKUM.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
