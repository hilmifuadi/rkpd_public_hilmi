using Eplan.Data;
using Eplan.Models;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Daftar
{
  public class RekeningService
  {
    public RekeningService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      this.rkpdContext = rkpdContext;
      this.primaryKeyService = primaryKeyService;
    }

    private RkpdContext rkpdContext;
    private PrimaryKeyService primaryKeyService;

    public Rekening GetItem(string id)
    {
      Rekening item = new Rekening
      {
        ActType = ActType.GET_ITEM
      };
      try
      {
        var bdo = rkpdContext.MATANGR.SingleOrDefault(e => e.MTGKEY == id);
        item.Id = bdo.MTGKEY;
        item.Kode = bdo.KDPER;
        item.Nama = bdo.NMPER;
        item.Level = int.Parse(bdo.MTGLEVEL);
        item.Tipe = bdo.TYPE;
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    //which table should I have to read?
    public Rekening AddItem(Rekening item)
    {
      try
      {
        var cKey = primaryKeyService.GetPrimaryKey("", "", out NEXTKEY nKey);
        nKey.NEXTKEYs = primaryKeyService.GeneratePrimaryKey(cKey);

        item.Id = cKey;
        var bdo = new MATANGR
        {
          MTGKEY = item.Id,
          KDPER = item.Kode,
          NMPER = item.Nama,
          MTGLEVEL = item.Level.ToString(),
          TYPE = item.Tipe
        };

        rkpdContext.MATANGR.Add(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      } 
      return item;
    }

    public Rekening UpdateItem(string id, Rekening item)
    {
      try
      {
        var bdo = rkpdContext.MATANGR.SingleOrDefault(e => e.MTGKEY == id);
        bdo.NMPER = item.Nama;
        bdo.MTGLEVEL = item.Level.ToString();
        bdo.TYPE = item.Tipe;
        rkpdContext.MATANGR.Update(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }

    public Rekening RemoveItem(Rekening item)
    {
      try
      {
        var bdo = new MATANGR
        {
          MTGKEY = item.Id,
          NMPER = item.Nama,
          KDPER = item.Kode,
          TYPE = item.Tipe,
          MTGLEVEL = item.Level.ToString()
        };

        rkpdContext.MATANGR.Remove(bdo);
        rkpdContext.SaveChanges();
      }
      catch(Exception e)
      {
        item.IsError = true;
        var error = new ErrorMessage
        {
          Title = $"Error from action: {item.ActType}",
          Message = $"Message: {e.Message}. Please contact your administrator."
        };
        item.Error = error;
      }
      return item;
    }
  }
}
