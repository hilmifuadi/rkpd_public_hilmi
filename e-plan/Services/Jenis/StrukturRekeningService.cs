using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class StrukturRekeningService : IServiceBase<StrukturRekening>
  {
    public StrukturRekeningService(RkpdContext rkpdContext)
    {
      _rkpdContext = rkpdContext;
    }

    private RkpdContext _rkpdContext;

    public List<StrukturRekening> GetCollection()
    {
      return _rkpdContext.STRUREK
        .Select(e => new StrukturRekening
        {
          Kode = e.MTGLEVEL,
          Uraian = e.NMLEVEL
        })
      .AsNoTracking()
      .ToList();
    }

    public StrukturRekening Create(StrukturRekening entity)
    {
      _rkpdContext.STRUREK.Add(new STRUREK
      {
        MTGLEVEL = entity.Kode,
        NMLEVEL = entity.Uraian
      });

      _rkpdContext.SaveChanges();

      return entity;
    }

    public StrukturRekening Read(string id)
    {
      return _rkpdContext.STRUREK
        .Where(e => e.MTGLEVEL == id)
        .Select(e => new StrukturRekening
        {
          Kode = e.MTGLEVEL,
          Uraian = e.NMLEVEL
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(StrukturRekening entity)
    {
      var old = _rkpdContext.STRUREK.Where(e => e.MTGLEVEL == entity.Kode).SingleOrDefault();
      old.NMLEVEL = entity.Uraian;
      _rkpdContext.STRUREK.Update(old);
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(StrukturRekening strukturRekening)
    {
      var item = _rkpdContext.STRUREK.Where(e => e.MTGLEVEL == strukturRekening.Kode).Single();
      if(item != null)
      {
        _rkpdContext.STRUREK.Remove(item);
      }
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<StrukturRekening> entities)
    {
      var items = new List<STRUREK>();
      foreach(var item in entities)
      {
        items.Add(_rkpdContext.STRUREK.Where(e => e.MTGLEVEL == item.Kode).SingleOrDefault());
      }
      _rkpdContext.STRUREK.RemoveRange(items);
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }
  }
}
