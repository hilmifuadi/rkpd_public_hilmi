using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class GolonganService : IServiceBase<Golongan>
  {
    public GolonganService(RkpdContext rkpdContext)
    {
      context = rkpdContext;
    }

    private RkpdContext context;

    public List<Golongan> GetCollection()
    {
      return context.GOLONGAN
        .Select(e => new Golongan
        {
          Kode = e.KDGOL,
          Nama = e.NMGOL,
          Pangkat = e.PANGKAT
        })
        .AsNoTracking()
        .ToList();
    }

    public Golongan Create(Golongan entity)
    {
      context.GOLONGAN.Add(new GOLONGAN
      {
        KDGOL = entity.Kode,
        NMGOL = entity.Nama,
        PANGKAT = entity.Pangkat
      });
      context.SaveChanges();
      return entity;
    }

    public Golongan Read(string id)
    {
      return context.GOLONGAN
        .Where(e => e.KDGOL == id)
        .Select(e => new Golongan
        {
          Kode = e.KDGOL,
          Nama = e.NMGOL,
          Pangkat = e.PANGKAT
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(Golongan entity)
    {
      var old = context.GOLONGAN.Where(e => e.KDGOL == entity.Kode).SingleOrDefault();
      old.NMGOL = entity.Nama;
      old.PANGKAT = entity.Pangkat;
      context.GOLONGAN.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(Golongan golongan)
    {
      var item = context.GOLONGAN.Where(e => e.KDGOL == golongan.Kode).Single();
      if (item != null)
      {
        context.GOLONGAN.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<Golongan> entities)
    {
      var items = new List<GOLONGAN>();
      foreach(var item in entities)
      {
        items.Add(context.GOLONGAN.Where(e => e.KDGOL == item.Kode).SingleOrDefault());
      }
      context.GOLONGAN.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
