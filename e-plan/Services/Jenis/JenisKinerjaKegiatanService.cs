using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class JenisKinerjaKegiatanService : IServiceBase<JenisKinerjaKegiatan>
  {
    public JenisKinerjaKegiatanService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      context = rkpdContext;
      keyService = primaryKeyService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;

    public List<JenisKinerjaKegiatan> GetCollection()
    {
      return context.JKINKEG
        .Select(e => new JenisKinerjaKegiatan
        {
          Kode = e.KDJKK,
          Uraian = e.URJKK
        })
        .AsNoTracking()
        .ToList();
    }

    public JenisKinerjaKegiatan Create(JenisKinerjaKegiatan entity)
    {
      context.JKINKEG.Add(new JKINKEG
      {
        KDJKK = entity.Kode,
        URJKK = entity.Uraian
      });
      context.SaveChanges();
      return entity;
    }

    public JenisKinerjaKegiatan Read(string id)
    {
      return context.JKINKEG
        .Where(e => e.KDJKK == id)
        .Select(e => new JenisKinerjaKegiatan
        {
          Kode = e.KDJKK,
          Uraian = e.URJKK
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(JenisKinerjaKegiatan entity)
    {
      var old = context.JKINKEG.Where(e => e.KDJKK == entity.Kode).SingleOrDefault();
      old.URJKK = entity.Uraian;
      context.JKINKEG.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(JenisKinerjaKegiatan jenisKinerjaKegiatan)
    {
      var item = context.JKINKEG.Where(e => e.KDJKK == jenisKinerjaKegiatan.Kode).Single();
      if(item != null)
      {
        context.JKINKEG.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<JenisKinerjaKegiatan> entities)
    {
      var items = new List<JKINKEG>();
      foreach(var entity in entities)
      {
        items.Add(context.JKINKEG.Where(e => e.KDJKK == entity.Kode).SingleOrDefault());
      }
      context.JKINKEG.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
