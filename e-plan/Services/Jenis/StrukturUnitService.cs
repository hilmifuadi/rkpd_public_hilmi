using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class StrukturUnitService : IServiceBase<StrukturUnit>
  {
    public StrukturUnitService(RkpdContext rkpdContext)
    {
      _rkpdContext = rkpdContext;
    }

    private RkpdContext _rkpdContext;

    public List<StrukturUnit> GetCollection()
    {
      return _rkpdContext.STRUUNIT
        .AsNoTracking()
        .Select(e => new StrukturUnit
        {
          Kode = e.KDLEVEL,
          Uraian = e.NMLEVEL
        })
        .ToList();
    }

    public StrukturUnit Create(StrukturUnit entity)
    {
      _rkpdContext.STRUUNIT.Add(new STRUUNIT
      {
        KDLEVEL = entity.Kode,
        NMLEVEL = entity.Uraian
      });

      _rkpdContext.SaveChanges();

      return entity;
    }

    public StrukturUnit Read(string id)
    {
      return _rkpdContext.STRUUNIT
        .Where(e => e.KDLEVEL == id)
        .Select(e => new StrukturUnit
        {
          Kode = e.KDLEVEL,
          Uraian = e.NMLEVEL
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(StrukturUnit entity)
    {
      var old = _rkpdContext.STRUUNIT.Where(e => e.KDLEVEL == entity.Kode).SingleOrDefault();
      old.NMLEVEL = entity.Uraian;
      _rkpdContext.STRUUNIT.Update(old);
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(StrukturUnit strukturUnit)
    {
      var item = _rkpdContext.STRUUNIT.Where(e => e.KDLEVEL == strukturUnit.Kode).Single();
      if(item != null)
      {
        _rkpdContext.STRUUNIT.Remove(item);
      }
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<StrukturUnit> entities)
    {
      var items = new List<STRUUNIT>();
      foreach (var entity in entities)
      {
        items.Add(_rkpdContext.STRUUNIT.Where(e => e.KDLEVEL == entity.Kode).SingleOrDefault());
      }
      _rkpdContext.STRUUNIT.RemoveRange(items);
      return _rkpdContext.SaveChanges() > 0 ? true : false;
    }
  }
}
