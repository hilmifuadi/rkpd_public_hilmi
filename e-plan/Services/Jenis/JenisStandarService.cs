using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class JenisStandarService : IServiceBase<JenisStandar>
  {
    public JenisStandarService(RkpdContext rkpdContext, PrimaryKeyService primaryKeyService)
    {
      context = rkpdContext;
      keyService = primaryKeyService;
    }

    private RkpdContext context;
    private PrimaryKeyService keyService;

    public List<JenisStandar> GetCollection()
    {
      return context.JSTANDAR.Select(e => new JenisStandar
      {
        Kode = e.KDJNSSTD,
        Nama = e.NMJNSSTD,
        Keterangan = e.KET
      })
      .AsNoTracking()
      .ToList();
    }

    public JenisStandar Create(JenisStandar entity)
    {
      context.JSTANDAR.Add(new JSTANDAR
      {
        KDJNSSTD = entity.Kode,
        NMJNSSTD = entity.Nama,
        KET = entity.Keterangan
      });

      context.SaveChanges();

      return entity;
    }

    public JenisStandar Read(string id)
    {
      return context.JSTANDAR
        .Where(e => e.KDJNSSTD == id)
        .Select(e => new JenisStandar
        {
          Kode = e.KDJNSSTD,
          Nama = e.NMJNSSTD,
          Keterangan = e.KET
        })
      .AsNoTracking()
      .SingleOrDefault();
    }

    public bool Update(JenisStandar entity)
    {
      var old = context.JSTANDAR.Where(e => e.KDJNSSTD == entity.Kode).SingleOrDefault();
      old.NMJNSSTD = entity.Nama;
      old.KET = entity.Keterangan;
      context.JSTANDAR.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(JenisStandar jenisStandar)
    {
      var item = context.JSTANDAR.Where(e => e.KDJNSSTD == jenisStandar.Kode).Single();
      if(item != null)
      {
        context.JSTANDAR.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<JenisStandar> entities)
    {
      var items = new List<JSTANDAR>();
      foreach (var entity in entities)
      {
        items.Add(context.JSTANDAR.Where(e => e.KDJNSSTD == entity.Kode).SingleOrDefault());
      }
      context.JSTANDAR.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
