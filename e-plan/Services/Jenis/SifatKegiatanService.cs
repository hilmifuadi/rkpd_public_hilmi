using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class SifatKegiatanService : IServiceBase<SifatKegiatan>
  {
    public SifatKegiatanService(RkpdContext rkpdContext)
    {
      context = rkpdContext;
    }

    private RkpdContext context;

    public List<SifatKegiatan> GetCollection()
    {
      return context.SIFATKEG
        .Select(e => new SifatKegiatan
        {
          Kode = e.KDSIFAT,
          Uraian = e.NMSIFAT
        })
        .AsNoTracking()
        .ToList();
    }

    public SifatKegiatan Create(SifatKegiatan entity)
    {
      context.SIFATKEG.Add(new SIFATKEG
      {
        KDSIFAT = entity.Kode,
        NMSIFAT = entity.Uraian
      });

      context.SaveChanges();

      return entity;
    }

    public SifatKegiatan Read(string id)
    {
      return context.SIFATKEG
        .Where(e => e.KDSIFAT == id)
        .Select(e => new SifatKegiatan
        {
          Kode = e.KDSIFAT,
          Uraian = e.NMSIFAT
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(SifatKegiatan entity)
    {
      var old = context.SIFATKEG.Where(e => e.KDSIFAT == entity.Kode).SingleOrDefault();
      old.NMSIFAT = entity.Uraian;
      context.SIFATKEG.Update(old);
      return context.SaveChanges() > 0 ? true: false;
    }

    public bool Delete(SifatKegiatan sifatKegiatan)
    {
      var item = context.SIFATKEG.Where(e => e.KDSIFAT == sifatKegiatan.Kode).Single();
      if(item != null)
      {
        context.SIFATKEG.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<SifatKegiatan> entities)
    {
      var items = new List<SIFATKEG>();
      foreach(var entity in entities)
      {
        items.Add(context.SIFATKEG.Where(e => e.KDSIFAT == entity.Kode).SingleOrDefault());
      }
      context.SIFATKEG.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
