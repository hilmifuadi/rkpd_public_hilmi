using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class JenisDanaService : IServiceBase<JenisDana>
  {
    public JenisDanaService(RkpdContext rkpdContext)
    {
      context = rkpdContext;
    }

    private RkpdContext context;

    public List<JenisDana> GetCollection()
    {
      return context.JDANA
        .Select(e => new JenisDana
        {
          Kode = e.KDDANA,
          Nama = e.NMDANA,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .ToList();
    }

    public JenisDana Create(JenisDana entity)
    {
      context.JDANA.Add(new JDANA
      {
        KDDANA = entity.Kode,
        NMDANA = entity.Nama,
        KET = entity.Keterangan
      });
      context.SaveChanges();
      return entity;
    }

    public JenisDana Read(string id)
    {
      return context.JDANA
        .Where(e => e.KDDANA == id)
        .Select(e => new JenisDana
        {
          Kode = e.KDDANA,
          Nama = e.NMDANA,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(JenisDana entity)
    {
      var old = context.JDANA.Where(e => e.KDDANA == entity.Kode).SingleOrDefault();
      old.NMDANA = entity.Nama;
      old.KET = entity.Keterangan;
      context.JDANA.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(JenisDana jenisDana)
    {
      var item = context.JDANA.Where(e => e.KDDANA == jenisDana.Kode).Single();
      if(item != null)
      {
        context.JDANA.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<JenisDana> entities)
    {
      var items = new List<JDANA>();
      foreach(var item in entities)
      {
        items.Add(context.JDANA.Where(e => e.KDDANA == item.Kode).SingleOrDefault());
      }
      context.JDANA.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
