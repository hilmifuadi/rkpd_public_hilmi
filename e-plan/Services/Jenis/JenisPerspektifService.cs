using Eplan.Data;
using Eplan.Models.BusinessLogic;
using Eplan.Models.DataTransaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Services.Jenis
{
  public class JenisPerspektifService : IServiceBase<JenisPerspektif>
  {
    public JenisPerspektifService(RkpdContext rkpdContext)
    {
      context = rkpdContext;
    }

    private RkpdContext context;

    public List<JenisPerspektif> GetCollection()
    {
      return context.JPERSPEKTIF
        .Select(e => new JenisPerspektif
        {
          Kode = e.KDPERSPEKTIF,
          Nama = e.NMPERSPEKTIF,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .ToList();
    }

    public JenisPerspektif Create(JenisPerspektif entity)
    {
      context.JPERSPEKTIF.Add(new JPERSPEKTIF
      {
        KDPERSPEKTIF = entity.Kode,
        NMPERSPEKTIF = entity.Nama,
        KET = entity.Keterangan
      });

      context.SaveChanges();
      return entity;
    }

    public JenisPerspektif Read(string id)
    {
      return context.JPERSPEKTIF
        .Where(e => e.KDPERSPEKTIF == id)
        .Select(e => new JenisPerspektif
        {
          Kode = e.KDPERSPEKTIF,
          Nama = e.NMPERSPEKTIF,
          Keterangan = e.KET
        })
        .AsNoTracking()
        .SingleOrDefault();
    }

    public bool Update(JenisPerspektif entity)
    {
      var old = context.JPERSPEKTIF.Where(e => e.KDPERSPEKTIF == entity.Kode).SingleOrDefault();
      old.NMPERSPEKTIF = entity.Nama;
      old.KET = entity.Keterangan;
      context.JPERSPEKTIF.Update(old);
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Delete(JenisPerspektif jenisPerspektif)
    {
      var item = context.JPERSPEKTIF.Where(e => e.KDPERSPEKTIF == jenisPerspektif.Kode).Single();
      if(item != null)
      {
        context.JPERSPEKTIF.Remove(item);
      }
      return context.SaveChanges() > 0 ? true : false;
    }

    public bool Deletes(List<JenisPerspektif> entities)
    {
      var items = new List<JPERSPEKTIF>();
      foreach(var item in entities)
      {
        items.Add(context.JPERSPEKTIF.Where(e => e.KDPERSPEKTIF == item.Kode).SingleOrDefault());
      }
      context.JPERSPEKTIF.RemoveRange(items);
      return context.SaveChanges() > 0 ? true : false;
    }
  }
}
