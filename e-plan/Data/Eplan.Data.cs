using Eplan.Models.BusinessLogic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Data
{
  public class RkpdContext : DbContext
  {
    public RkpdContext(DbContextOptions<RkpdContext> dbContextOptions) :base(dbContextOptions) { }

    public DbSet<DAFTUNIT> DAFTUNIT { get; set; }
    public DbSet<PEGAWAI> PEGAWAI { get; set; }
    public DbSet<TAHAP> TAHAP { get; set; }
    public DbSet<TAHUN> TAHUN { get; set; }
    public DbSet<STRUUNIT> STRUUNIT { get; set; }
    public DbSet<STRUREK> STRUREK { get; set; }
    public DbSet<SIFATKEG> SIFATKEG { get; set; }
    public DbSet<JSTANDAR> JSTANDAR { get; set; }
    public DbSet<JPERSPEKTIF> JPERSPEKTIF { get; set; }
    public DbSet<JKINKEG> JKINKEG { get; set; }
    public DbSet<JDANA> JDANA { get; set; }
    public DbSet<GOLONGAN> GOLONGAN { get; set; }
    public DbSet<JKEC> JKEC { get; set; }
    public DbSet<NEXTKEY> NEXTKEY { get; set; }
    public DbSet<WEBGROUP> WEBGROUP { get; set; }
    public DbSet<WEBOTOR> WEBOTOR { get; set; }
    public DbSet<WEBROLE> WEBROLE { get; set; }
    public DbSet<DASKUM> DASKUM { get; set; }
    public DbSet<PRIOPROV> PRIOPROV { get; set; }
    public DbSet<PRIOPPAS> PRIOPPAS { get; set; }
    public DbSet<SASARANTHN> SASARANTHN { get; set; }
    public DbSet<STDHARGA> STDHARGA { get; set; }
    public DbSet<DAFTKEC> DAFTKEC { get; set; }
    public DbSet<DAFTDESA> DAFTDESA { get; set; }
    public DbSet<MATANGR> MATANGR { get; set; }
    public DbSet<DAFTDPRD> DAFTDPRD { get; set; }
    public DbSet<ATASBEND> ATASBEND { get; set; }
    public DbSet<URUSANUNIT> URUSANUNIT { get; set; }
    public DbSet<VISI> VISI { get; set; }
    public DbSet<MISI> MISI { get; set; }
    public DbSet<TUJUAN> TUJUAN { get; set; }
    public DbSet<SASARAN> SASARAN { get; set; }
    public DbSet<SASARANDET> SASARANDET { get; set; }
    public DbSet<STRATEGI> STRATEGI { get; set; }
    public DbSet<KEBIJAKAN> KEBIJAKAN { get; set; }
    public DbSet<PRIONAS> PRIONAS { get; set; }
    public DbSet<MPGRMRPJM> MPGRMRPJM { get; set; }
    public DbSet<MKEGRPJM> MKEGRPJM { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<WEBOTOR>().HasKey(c => new { c.GROUPID, c.ROLEID });
      modelBuilder.Entity<URUSANUNIT>().HasKey(c => new { c.UNITKEY, c.URUSKEY });
      modelBuilder.Entity<ATASBEND>().HasKey(c => new { c.UNITKEY, c.NIP });
      modelBuilder.Entity<URUSANUNIT>().HasKey(c => new { c.UNITKEY, c.URUSKEY });
      modelBuilder.Entity<RPJMBIJAK>().HasKey(c => new { c.BIJAKKEY, c.PGRMRPJMKEY });
    }
  }
}
