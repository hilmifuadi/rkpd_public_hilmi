using Eplan.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Models
{
  public enum ActType
  {
    GET_MANY_ITEM,
    GET_ITEM,
    REMOVE_ITEM,
    REMOVE_MANY_ITEM,
    ADD_ITEM,
    UPDATE_ITEM
  };

  public interface IState
  {
    ActType ActType { get; set; }
    bool IsError { get; set; }
    ErrorMessage Error { get; set; }
  }

  public class ErrorMessage
  {
    public string Title { get; set; }
    public string Message { get; set; }
  }
}
