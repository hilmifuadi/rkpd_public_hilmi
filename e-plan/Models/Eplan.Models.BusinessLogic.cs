using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Models.BusinessLogic
{
  #region Rkpd

  public class JKEC
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDJKEC { get; set; }
    public string URJKEC { get; set; }
  }

  public class STRUUNIT
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDLEVEL { get; set; }
    public string NMLEVEL { get; set; }
  }

  public class DAFTUNIT
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string UNITKEY { get; set; }

    [ForeignKey("STRUUNIT")]
    public string KDLEVEL { get; set; }
    public STRUUNIT STRUUNIT { get; set; }

    public string KDUNIT { get; set; }
    public string NMUNIT { get; set; }
    public string AKROUNIT { get; set; }
    public string ALAMAT { get; set; }
    public string TELEPON { get; set; }
    public string TYPE { get; set; }

    [ForeignKey("JKEC")]
    public string KDJKEC { get; set; }
    public JKEC JKEC { get; set; }
  }

  public class NEXTKEY
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string TABLEID { get; set; }
    [Column("NEXTKEY")]
    public string NEXTKEYs { get; set; }
    public string NEXTKEYDESC { get; set; }
    public string FIELDID { get; set; }
  }

  public class PEGAWAI
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string NIP { get; set; }

    [ForeignKey("GOLONGAN")]
    public string KDGOL { get; set; }
    public GOLONGAN GOLONGAN { get; set; }

    [ForeignKey("DAFTUNIT")]
    public string UNITKEY { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }

    public string NAMA { get; set; }
    public string ALAMAT { get; set; }
    public string JABATAN { get; set; }
    public string PDDK { get; set; }
  }

  public class GOLONGAN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDGOL { get; set; }
    public string NMGOL { get; set; }
    public string PANGKAT { get; set; }
  }

  public class TAHUN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDTAHUN { get; set; }
    public string NMTAHUN { get; set; }
  }

  public class TAHAP
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDTAHAP { get; set; }
    public string NMTAHAP { get; set; }
    public string KET { get; set; }
  }

  public class JDANA
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDDANA { get; set; }
    public string NMDANA { get; set; }
    public string KET { get; set; }
    public string TYPE { get; set; }
  }

  public class JKINKEG
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDJKK { get; set; }
    public string URJKK { get; set; }
  }

  public class JPERSPEKTIF
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDPERSPEKTIF { get; set; }
    public string NMPERSPEKTIF { get; set; }
    public string KET { get; set; }
  }

  public class JSTANDAR
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDJNSSTD { get; set; }
    public string NMJNSSTD { get; set; }
    public string KET { get; set; }
  }

  public class SIFATKEG
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KDSIFAT { get; set; }
    public string NMSIFAT { get; set; }
  }

  public class STRUREK
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string MTGLEVEL { get; set; }
    public string NMLEVEL { get; set; }
  }

  public class WEBGROUP
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string GROUPID { get; set; }
    public string NMGROUP { get; set; }
    public string KET { get; set; }
  }

  public class WEBOTOR
  {
    [ForeignKey("WEBGROUP")]
    public string GROUPID { get; set; }
    [ForeignKey("WEBROLE")]
    public string ROLEID { get; set; }

    public WEBGROUP WEBGROUP { get; set; }
    public WEBROLE WEBROLE { get; set; }
  }

  public class WEBROLE
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string ROLEID { get; set; }
    public string ROLE { get; set; }
    public string TYPE { get; set; }
    public string MENUID { get; set; }
    public string BANTUAN { get; set; }
  }

  public class DASKUM
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string DASKUMKEY { get; set; }
    public string NODASKUM { get; set; }
    public string ISIDASKUM { get; set; }
  }

  public class PRIOPROV
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string PRIOPROVKEY { get; set; }
    public string NUPRIO { get; set; }
    public string NMPRIO { get; set; }
    public string KET { get; set; }
  }

  public class PRIOPPAS
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string PRIOPPASKEY { get; set; }
    public string NOPRI { get; set; }
    public string NOPRIOPPAS { get; set; }
    public string NMPRIOPPAS { get; set; }
    public string SASARAN { get; set; }
    public string KET { get; set; }
  }

  public class SASARANTHN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDSAS { get; set; }
    public string NOSAS { get; set; }
    public string NMSAS { get; set; }
    public string KET { get; set; }

    [ForeignKey("PRIOPPAS")]
    public string PRIOPPASKEY { get; set; }
    public PRIOPPAS PRIOPPAS { get; set; }
  }

  public class STDHARGA
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDSTDHARGA { get; set; }

    public string NOSTD { get; set; }
    public string NMSTD { get; set; }
    public string SPEKSTD { get; set; }
    public string MERKSTD { get; set; }
    public string SATUAN { get; set; }
    public decimal HRGSTD { get; set; }
    public string KET { get; set; }
    public string STVALID { get; set; }

    [ForeignKey("JSTANDAR")]
    public string KDJNSSTD { get; set; }
    public JSTANDAR JSTANDAR { get; set; }
  }

  public class DAFTKEC
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDKEC { get; set; }

    [ForeignKey("DAFTUNIT")]
    public string UNITKEY { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }
  }

  public class DAFTDESA
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDDESA { get; set; }
    public decimal PAGU { get; set; }

    [ForeignKey("DAFTUNIT")]
    public string UNITKEY { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }

    [ForeignKey("DAFTKEC")]
    public string IDKEC { get; set; }
    public DAFTKEC DAFTKEC { get; set; }

    [ForeignKey("TAHUN")]
    public string KDTAHUN { get; set; }
    public TAHUN TAHUN { get; set; }
  }

  public class MATANGR
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string MTGKEY { get; set; }
    public string MTGLEVEL { get; set; }
    public string KDPER { get; set; }
    public string NMPER { get; set; }
    public string TYPE { get; set; }
  }

  public class DAFTDPRD
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDDPRD { get; set; }
    public string AKROUNIT { get; set; }
    public double PAGU { get; set; }
    [ForeignKey("DAFTUNIT")]
    public string UNITKEY { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }
  }

  public class ATASBEND
  {
    public string UNITKEY { get; set; }
    public string NIP { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }
    public PEGAWAI PEGAWAI { get; set; }
  }

  public class URUSANUNIT
  {
    public string UNITKEY { get; set; }
    public string URUSKEY { get; set; }
    public DAFTUNIT UNIT { get; set; }
    public List<DAFTUNIT> URUSAN { get; set; }
  }

  public class VISI
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDVISI { get; set; }
    public string NOVISI { get; set; }
    public string NMVISI { get; set; }
  }

  public class MISI
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string MISIKEY { get; set; }
    public string NOMISI { get; set; }
    public string URAIMISI { get; set; }
    public string SASARAN { get; set; }

    [ForeignKey("VISI")]
    public string IDVISI { get; set; }
    public VISI VISI { get; set; }
  }

  public class TUJUAN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string TUJUKEY { get; set; }
    public string NOTUJU { get; set; }
    public string URAITUJU { get; set; }

    [ForeignKey("MISI")]
    public string MISIKEY { get; set; }
    public MISI MISI { get; set; }
  }

  public class SASARAN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDSAS { get; set; }
    public string NOSAS { get; set; }
    public string NMSAS { get; set; }
    public string KET { get; set; }

    [ForeignKey("TUJUAN")]
    public string TUJUKEY { get; set; }
    public TUJUAN TUJUAN { get; set; }
  }

  public class SASARANDET
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string IDSASDET { get; set; }
    public string NOSASDET { get; set; }
    public string URAIDET { get; set; }
    public string SATUAN { get; set; }

    [ForeignKey("SASARAN")]
    public string IDSAS { get; set; }
    public SASARAN SASARAN { get; set; }
  }

  public class STRATEGI
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string STRATEGIKEY { get; set; }
    public string NOSTRATEGI { get; set; }
    public string URAISTRATEGI { get; set; }
    public string KET { get; set; }

    [ForeignKey("SASARAN")]
    public string IDSAS { get; set; }
    public SASARAN SASARAN { get; set; }
  }

  public class KEBIJAKAN
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string BIJAKKEY { get; set; }
    public string NOBIJAKAN { get; set; }
    public string NMEBIJAKAN { get; set; }
    public string KET { get; set; }

    [ForeignKey("STRATEGI")]
    public string STRATEGIKEY { get; set; }
    public STRATEGI STRATEGI { get; set; }
  }

  public class PRIONAS
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string PRIONASKEY { get; set; }
    public string NUPRIO { get; set; }
    public string NMPRIO { get; set; }
    public string KET { get; set; }
  }

  public class MPGRMRPJM
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string PGRMRPJMKEY { get; set; }
    public string NUPRGRM { get; set; }
    public string NMPRGRM { get; set; }
    public string INDIKATOR { get; set; }
    public string TARGET1 { get; set; }
    public string TARGET2 { get; set; }
    public string TARGET3 { get; set; }
    public string TARGET4 { get; set; }
    public string TARGET5 { get; set; }

    [ForeignKey("DAFTUNIT")]
    public string UNITKEY { get; set; }
    public DAFTUNIT DAFTUNIT { get; set; }
  }

  public class MKEGRPJM
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string KEGRPJMKEY { get; set; }
    public string NUKEG { get; set; }
    public string NMKEG { get; set; }

    [ForeignKey("JPERSPEKTIF")]
    public string KDPERSPEKTIF { get; set; }
    public JPERSPEKTIF JPERSPEKTIF { get; set; }

    [ForeignKey("MPGRMRPJM")]
    public string PGRMRPJMKEY { get; set; }
    public MPGRMRPJM MPGRMRPJM { get; set; }
  }

  public class RPJMBIJAK
  {
    public string BIJAKKEY { get; set; }
    public string PGRMRPJMKEY { get; set; }

    public KEBIJAKAN KEBIJAKAN { get; set; }
    public MPGRMRPJM  MPGRMRPJM { get; set; }
  }
  #endregion
}
