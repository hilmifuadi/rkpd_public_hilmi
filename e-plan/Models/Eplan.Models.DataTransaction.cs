using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eplan.Models.DataTransaction
{
  public class StrukturUnit
  {
    public string Kode { get; set; }
    public string Uraian { get; set; }
  }

  public class StrukturRekening
  {
    public string Kode { get; set; }
    public string Uraian { get; set; }
  }

  public class Golongan
  {
    public string Kode { get; set; }
    public string Nama { get; set; }
    public string Pangkat { get; set; }
  }

  public class JenisPerspektif
  {
    public string Kode { get; set; }
    public string Nama { get; set; }
    public string Keterangan { get; set; }
  }

  public class JenisDana
  {
    public string Kode { get; set; }
    public string Nama { get; set; }
    public string Keterangan { get; set; }
    public string Tipe { get; set; }
  }

  public class JenisKinerjaKegiatan
  {
    public string Kode { get; set; }
    public string Uraian { get; set; }
  }

  public class SifatKegiatan
  {
    public string Kode { get; set; }
    public string Uraian { get; set; }
  }

  public class JenisStandar
  {
    public string Kode { get; set; }
    public string Nama { get; set; }
    public string Keterangan { get; set; }
  }

  public class DasarHukum
  {
    public string Id { get; set; }
    public string Nomor { get; set; }
    public string Isi { get; set; }
  }

  public class PrioritasProvinsi
  {
    public string Id { get; set; }
    public string Nomor { get; set; }
    public string Nama { get; set; }
    public string Keterangan { get; set; }
  }

  public class PrioritasPembangunan
  {
    public string Id { get; set; }
    public string Nomor { get; set; }
    public string Nama { get; set; }
    public string Sasaran { get; set; }
    public string Keterangan { get; set; }
  }

  public class StandarHarga
  {
    public string Id { get; set; }
    public string Kode { get; set; }
    public string Nomor { get; set; }
    public string Nama { get; set; }
    public string Spesifikasi { get; set; }
    public string Merk { get; set; }
    public string Satuan { get; set; }
    public decimal Harga { get; set; }
    public string Keterangan { get; set; }

    public string IdJenisStandar { get; set; }
    public JenisStandar JenisStandar { get; set; }
  }

  public class SasaranPembangunan
  {
    public string Id { get; set; }
    public string Nomor { get; set; }
    public string Sasaran { get; set; }
    public string Keterangan { get; set; }

    public string PrioritasPembangunanKey { get; set; }
    public PrioritasPembangunan PrioritasPembangunan { get; set; }
  }

  public class Kecamatan: IState
  {
    public string Id { get; set; }
    public string UnitOrganisasiKey { get; set; }
    public UnitOrganisasi UnitOrganisasi { get; set; }

    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class UnitOrganisasi: IState
  {
    public string Id { get; set; }
    public string Kode { get; set; }
    public string Nama { get; set; }
    public string Akronim { get; set; }
    public string Alamat { get; set; }
    public string Telepon { get; set; }
    public string Tipe { get; set; }

    public string StrukturRekeningKey { get; set; }
    public StrukturRekening StrukturRekening { get; set; }
    public string StatusUnitKey { get; set; }
    public StatusUnit StatusUnit { get; set; }

    public List<UnitOrganisasi> Unit = new List<UnitOrganisasi>();

    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class StatusUnit
  {
    public string Kode { get; set; }
    public string Uraian { get; set; }
  }

  public class Tahun
  {
    public string Id { get; set; }
    public string Nama { get; set; }
  }

  public class Desa: IState
  {
    public string Id { get; set; }
    public decimal Pagu { get; set; }

    public string KecamatanId { get; set; }
    public Kecamatan Kecamatan { get; set; }
    public string UnitOrganisasiId { get; set; }
    public UnitOrganisasi UnitOrganisasi { get; set; }
    public string TahunId { get; set; }
    public Tahun Tahun { get; set; }

    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Rekening : IState
  {
    public string Id { get; set; }
    public string Kode { get; set; }
    public string Nama { get; set; }
    public int Level { get; set; }
    public string Tipe { get; set; }

    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Pegawai : IState
  {
    public string Nip { get; set; }
    public string Nama { get; set; }
    public string Alamat { get; set; }
    public string Jabatan { get; set; }
    public string Pendidikan { get; set; }
    public Golongan Golongan { get; set; }
    public UnitOrganisasi UnitOrganisasi { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Dewan: IState
  {
    public string Id { get; set; }
    public string Fraksi { get; set; }
    public double Pagu { get; set; }
    public UnitOrganisasi UnitOrganisasi { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class KepalaSKPD : IState
  {
    public Pegawai Pegawai { get; set; }
    public UnitOrganisasi Unit { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class UrusanUnit: IState
  {
    public UnitOrganisasi Unit { get; set; }
    public UnitOrganisasi[] Urusan;
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class RPJM
  {
    public string Id { get; set; }
    public string Nomor { get; set; }
    public string Uraian { get; set; }
  }

  public class Visi: RPJM, IState
  {
    public Misi[] Misi;
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Misi: RPJM, IState
  {
    public Visi Visi { get; set; }
    public Tujuan[] Tujuan;
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Tujuan: RPJM, IState
  {
    public Misi Misi { get; set; }
    public Sasaran[] Sasaran;
    public int MyProperty { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Sasaran: RPJM, IState
  {
    public Tujuan Tujuan { get; set; }
    public string Keterangan { get; set; }
    public Strategi[] Strategi;
    public IndikatorSasaran[] IndikatorSasaran;
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class IndikatorSasaran: RPJM, IState
  {
    public string Satuan { get; set; }
    public Sasaran Sasaran { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Strategi: RPJM, IState
  {
    public Sasaran Sasaran { get; set; }
    public Kebijakan[] Kebijakan;
    public string Keterangan { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Kebijakan: RPJM, IState
  {
    public Strategi Strategi { get; set; }
    public string Keterangan { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class PrioritasNasional: RPJM, IState
  {
    public string Keterangan { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class Target
  {
    public string T1 { get; set; }
    public string T2 { get; set; }
    public string T3 { get; set; }
    public string T4 { get; set; }
    public string T5 { get; set; }
  }
  public class ProgramRPJM : IState
  {
    public string Id { get; set; }
    public string Kode { get; set; }
    public string Uraian { get; set; }
    public string Indikator { get; set; }
    public Target Target { get; set; }
    public string UnitKey { get; set; }
    public ActType ActType { get; set; }
    public bool IsError { get; set; }
    public ErrorMessage Error { get; set; }
  }

  public class KegiatanRPJM
  {
    public string Id { get; set; }
    public string Kode { get; set; }
    public string Uraian { get; set; }
    public JenisPerspektif JenisPerspektif { get; set; }
    public ProgramRPJM Program { get; set; }
  }
}
