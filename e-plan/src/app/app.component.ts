import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MediaMatcher]
})
export class AppComponent {
  title: string = 'e-planning'
  @ViewChild(MatSidenav) sidenav: MatSidenav;
  tabletQuery: MediaQueryList;
  private tabletQueryListener: () => void;
  
  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.tabletQuery = media.matchMedia('(max-width: 1280px)');
    this.tabletQueryListener = () => changeDetectorRef.detectChanges();
    this.tabletQuery.addListener(this.tabletQueryListener);
  }

  ngOnDestroy(): void {
    this.tabletQuery.removeListener(this.tabletQueryListener);
  }

  toggleSidenavHandler(): void {
    this.sidenav.toggle();
  }
}
