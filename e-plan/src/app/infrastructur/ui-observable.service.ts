import { Injectable } from "@angular/core";
import { UIObservableServer } from "./publisher";
import { UIFilter } from "./data";
import { IUIObserver } from "./interfaces";
import { Tree } from "../shared/tree/tree";

@Injectable()
export class UIObservableService {

    private uiObservableServer: UIObservableServer = new UIObservableServer;
    
    addObserver(observer: IUIObserver) {
        this.uiObservableServer.addObserver(observer);
    }

    removeObserver(observer: IUIObserver) {
        this.uiObservableServer.removeObserver(observer);
    }

    notifyOnCreateStateChanged() {
        this.uiObservableServer.notifyOnCreateStateChanged();
    }

    notifyOnDetailStateChanged(state: boolean) {
        this.uiObservableServer.notifyOnDetailStateChanged(state);
    }

    notifyOnDeleteStateChanged() {
        this.uiObservableServer.notifyOnDeleteStateChanged();
    }

    public notifyOnBinStateChanged() {
        this.uiObservableServer.notifyOnBinStateChanged();
    }

    public notifyOnFilterStateChanged() {
        this.uiObservableServer.notifyOnFilterStateChanged();
    }

    notifyOnFilterChanged(filter: UIFilter[]) {
        this.uiObservableServer.notifyOnFilterChanged(filter);
    }

    public notifyOnFilterCreated(tree: Tree, category: string) {
        this.uiObservableServer.notifyOnFilterCreated(tree, category);
    }
}