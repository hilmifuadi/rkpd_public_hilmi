import { NgModule } from "@angular/core";
import { UIObservableService } from "./ui-observable.service";

@NgModule({
    providers: [ UIObservableService ]
})
export class UIObservableModule { }