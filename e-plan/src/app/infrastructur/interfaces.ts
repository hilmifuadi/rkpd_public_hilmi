import { UIData } from "./data";

export interface IUIObservable {
    addObserver(observer: IUIObserver);
    removeObserver(observer: IUIObserver);
    notifyObservers();
}

export interface IUIObserver {
    setNotification(uiData: UIData): void;
}
