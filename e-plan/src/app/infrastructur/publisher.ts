import { UIObservable } from "./base";
import { UIData, UIFilter } from "./data";
import { IUIObserver } from "./interfaces";
import { Tree } from "../shared/tree/tree";

export class UIObservableServer extends UIObservable {

    constructor() {
        super();
    }

    public addObserver(observer: IUIObserver) {
        this._observers.push(observer);
        this.notifyObservers();
    }

    public notifyOnFilterStateChanged() {
        this.uiData.state.filterState = !this.uiData.state.filterState;
        if(this.uiData.state.filterState) {
            this.uiData.state.createState = this.uiData.state.detailState = !this.uiData.state.filterState;
        }
        this.notifyObservers();
    }

    public notifyOnCreateStateChanged() {
        this.uiData.state.createState = !this.uiData.state.createState;
        if(this.uiData.state.createState) {
            this.uiData.state.filterState = !this.uiData.state.createState;
        }
        this.notifyObservers();
    }

    public notifyOnDetailStateChanged(state: boolean) {
        this.uiData.state.detailState = state;
        if(state) {
            this.uiData.state.createState = this.uiData.state.filterState = !state;
        }
        this.notifyObservers();
    }

    public notifyOnDeleteStateChanged() {
        this.uiData.state.deleteState = !this.uiData.state.deleteState;
        if(this.uiData.state.deleteState) {
            this.uiData.state.createState = this.uiData.state.detailState = this.uiData.state.filterState = !this.uiData.state.deleteState;
        }
        this.notifyObservers();
    }

    public notifyOnFilterChanged(filter: UIFilter[]) {
        this.uiData.filter = filter;
        this.notifyObservers();
    }

    public notifyOnFilterCreated(tree: Tree, category: string) {
        let filters: UIFilter[] = [];
        filters.push(new UIFilter(tree.id, tree.text, category));
        this.uiData.filter = filters;
        this.notifyObservers();
    }

    public notifyOnBinStateChanged() {
        this.uiData.state.binState = !this.uiData.state.binState;
        this.notifyObservers();
    }
}