import { IUIObservable, IUIObserver } from "./interfaces";
import { UIData } from "./data";

export abstract class UIObservable implements IUIObservable {
    
    protected _observers: IUIObserver[];
    protected uiData: UIData = new UIData;

    constructor() {
        this._observers = [];
    }

    public abstract addObserver(observer: IUIObserver);

    public notifyObservers() {
        let i = this._observers.length;
        while(i--) {
            this._observers[i].setNotification(this.uiData);
        } 
    }

    public removeObserver(observer: IUIObserver) : void {
        let i = this._observers.length;
        while(i--) {
            if(this._observers[i] === observer) {
                this._observers.splice(i, 1);
            }
        }
    }
}
