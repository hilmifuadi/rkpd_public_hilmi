class UIState {
    constructor(filterState = true, deleteState = false, detailState = false, 
        createState = false, binState = false) {
        this.filterState = filterState; 
        this.deleteState = deleteState; 
        this.detailState = detailState;
        this.createState = createState;
        this.binState = binState;
    }
    public filterState: boolean;
    public deleteState: boolean;
    public detailState: boolean;
    public createState: boolean;
    public binState: boolean;
}

export class UIFilter {
    constructor(id: string = "", label: string = "", category: string = "") { 
        this.id = id;
        this.label = label;
        this.category = category;
    }
    id: string;
    label: string;
    category: string;
}

export class UIData {
    constructor() {
        this.state = new UIState;
        this.filter = [];
    }
    public state: UIState;
    public filter: UIFilter[];
}