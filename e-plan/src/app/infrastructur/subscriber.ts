import { IUIObserver } from "./interfaces";
import { UIData } from "./data";

export class UIObservableClient implements IUIObserver  {
    public uiData: UIData = new UIData;
    public setNotification(uiData: UIData): void {
        this.uiData = uiData;
    }
}