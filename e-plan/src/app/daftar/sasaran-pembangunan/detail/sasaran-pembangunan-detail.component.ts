import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";

import { SasaranPembangunan, SasaranPembangunanService } from "../sasaran-pembangunan.service";
import { JenisStandarService } from "../../../jenis/jenis-standar/jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { Observable } from "rxjs/Observable";
import { PrioritasPembangunanService } from "../../prioritas-pembangunan/prioritas-pembangunan.service";

@Component({
    selector: 'sasaran-pembangunan-detail',
    templateUrl: './sasaran-pembangunan-detail.component.html',
    styleUrls: ['./sasaran-pembangunan-detail.component.css']
})
export class SasaranPembangunanDetailComponent implements OnInit, OnDestroy {
    constructor(private service: SasaranPembangunanService,
        private prioService: PrioritasPembangunanService,
        private uiService: UIObservableService) { }

    uiClient: UIObservableClient = new UIObservableClient;
    @Input() item: SasaranPembangunan;
    @Output() onItemUpdated = new EventEmitter<SasaranPembangunan>();
    @Output() onItemDeleted = new EventEmitter<SasaranPembangunan>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}