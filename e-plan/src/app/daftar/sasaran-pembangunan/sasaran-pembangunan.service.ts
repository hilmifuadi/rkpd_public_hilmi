import { PrioritasPembangunan } from "../prioritas-pembangunan/prioritas-pembangunan.service";
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient  } from "@angular/common/http";

export class SasaranPembangunan {
    constructor(
        public id: string = "",
        public nomor: string = "",
        public sasaran: string = "",
        public keterangan: string = "",
        public isSelected: boolean = false,
        public prioritasPembangunanKey: string = "",
        public prioritasPembangunan: PrioritasPembangunan = new PrioritasPembangunan()) { }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const api = 'api/daftar/sasaran-pembangunan';

@Injectable()
export class SasaranPembangunanService {
    constructor(private http: HttpClient) { }

    getCollection() {
        return this.http.get<SasaranPembangunan[]>(api);
    }

    create(sasaranPembangunan: SasaranPembangunan) {
        return this.http.post<SasaranPembangunan>(
            api, 
            sasaranPembangunan, 
            httpOptions);
    }

    read(id: string) {
        return this.http.get<SasaranPembangunan>(`${api}/${id}`);
    }

    update(sasaranPembangunan: SasaranPembangunan) {
        return this.http.put<boolean>(
            api, 
            sasaranPembangunan, 
            httpOptions);
    }

    delete(sasaranPembangunan: SasaranPembangunan) {
        return this.http.post<boolean>(
            `${api}/delete`, 
            sasaranPembangunan, 
            httpOptions);
    }

    deletes(items: SasaranPembangunan[]) {
        return this.http.post<boolean>(
            `${api}/deletes`, 
            items, 
            httpOptions);
    }
}