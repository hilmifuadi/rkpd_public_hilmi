import { NgModule } from '@angular/core'
import { MatTableModule } from "@angular/material/table";
import { MatListModule } from "@angular/material/list";
import { MatInputModule } from "@angular/material/input";
import { MatChipsModule } from "@angular/material/chips";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSortModule } from "@angular/material/sort";
import { MatPaginatorModule } from "@angular/material/paginator";

@NgModule({
  imports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatChipsModule, 
    MatSortModule,
    MatPaginatorModule,
  ],
  exports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatChipsModule, 
    MatSortModule,
    MatPaginatorModule,
  ]
})
export class SasaranPembangunanUiModule { }
