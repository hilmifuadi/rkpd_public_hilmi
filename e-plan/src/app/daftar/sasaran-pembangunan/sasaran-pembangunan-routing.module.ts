import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { SasaranPembangunanComponent } from "./sasaran-pembangunan.component";

const routes: Routes = [
    {
        path: '',
        component: SasaranPembangunanComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SasaranPembangunanRoutingModule { }