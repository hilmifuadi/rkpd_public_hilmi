import { Component, OnInit, ViewChild } from "@angular/core";
import { UIObservableService } from "../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../infrastructur/subscriber";

import { SasaranPembangunan, SasaranPembangunanService } from "./sasaran-pembangunan.service";
import { PrioritasPembangunan, PrioritasPembangunanService } from "../prioritas-pembangunan/prioritas-pembangunan.service";

import { MatTableDataSource, MatSort } from "@angular/material";
import { UIFilter } from "../../infrastructur/data";

@Component({
    templateUrl: './sasaran-pembangunan.component.html',
    providers: [
        UIObservableService
    ]
})
export class SasaranPembangunanComponent implements OnInit {
    constructor(
        private service: SasaranPembangunanService,
        private prioritasPembangunanService: PrioritasPembangunanService,
        private uiService: UIObservableService) {}

    title: string = "Sasaran Pembangunan";
    
    items: SasaranPembangunan[] = [];
    item: SasaranPembangunan = new SasaranPembangunan;

    listOfPrioritasPembangunan: PrioritasPembangunan[];

    columns = [ 'nomor', 'sasaran' ];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    uiClient: UIObservableClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.prioritasPembangunanService.getCollection().subscribe(value => {
            this.listOfPrioritasPembangunan = value;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(search: string) {
        this.dataSource.filter = search;
    }

    onFilterResultChanged() {
        this.fillDataSource();
    }

    onItemCreated(item: SasaranPembangunan) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: SasaranPembangunan) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: SasaranPembangunan) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    fillDataSource() {
        if(this.uiClient.uiData.filter.length > 0) {
            this.service.getCollection().subscribe(value => {
                this.items = value;
                let i = this.uiClient.uiData.filter.length;
                while(i--) {
                    if(this.uiClient.uiData.filter[i].category === "Prioritas Pembangunan") {
                        this.items = this.items.filter(f => f.prioritasPembangunanKey.trim() === this.uiClient.uiData.filter[i].id.trim());
                    }
                }
                this.dataSource.data = this.items;
            });
        }
    }

    rowSelectedHandler(item: SasaranPembangunan): void {
        this.item = item;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(item: SasaranPembangunan) {
        item.isSelected = !item.isSelected;    
    }

    deleteItemsHandler() {
        let found = this.items.filter(e => e.isSelected);
        if(found) {
            this.service.deletes(found).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
    
                }
            );
        }
    }

    toogleSnackbarHandler() {
        this.uiService.notifyOnBinStateChanged();
    }
}