import { NgModule } from "@angular/core";

import { SharedModule } from "../../shared/shared.module";
import { SasaranPembangunanUiModule } from "./sasaran-pembangunan-ui.module";
import { PrioritasPembangunanModule } from "../prioritas-pembangunan/prioritas-pembangunan.module";
import { UIObservableModule } from "../../infrastructur/ui-observable.module";
import { SasaranPembangunanRoutingModule } from "./sasaran-pembangunan-routing.module";

import { SasaranPembangunanComponent } from "./sasaran-pembangunan.component";
import { SasaranPembangunanService } from "./sasaran-pembangunan.service";
import { SasaranPembangunanFilterComponent } from "./filter/sasaran-pembangunan-filter.component";
import { CreateSasaranPembangunanComponent } from "./create/create-sasaran-pembangunan.component";
import { SasaranPembangunanDetailComponent } from "./detail/sasaran-pembangunan-detail.component";


@NgModule({
    imports: [
        SharedModule, 
        SasaranPembangunanUiModule, 
        SasaranPembangunanRoutingModule, 
        PrioritasPembangunanModule,
        UIObservableModule
    ],
    declarations: [
        SasaranPembangunanComponent, 
        SasaranPembangunanFilterComponent, 
        CreateSasaranPembangunanComponent, 
        SasaranPembangunanDetailComponent
    ],
    providers: [
        SasaranPembangunanService
    ]
})
export class SasaranPembangunanModule {}