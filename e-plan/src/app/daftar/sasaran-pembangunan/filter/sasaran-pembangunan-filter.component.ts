import { Component, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { SasaranPembangunanService } from "../sasaran-pembangunan.service";
import { PrioritasPembangunanService, PrioritasPembangunan } from "../../prioritas-pembangunan/prioritas-pembangunan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIFilter } from "../../../infrastructur/data";


@Component({
    selector: 'sasaran-pembangunan-filter',
    templateUrl: './sasaran-pembangunan-filter.component.html',
    styleUrls: ['./sasaran-pembangunan-filter.component.css']
})
export class SasaranPembangunanFilterComponent implements OnInit, OnDestroy {
    constructor(private service: SasaranPembangunanService, 
        private prioService: PrioritasPembangunanService,
        private uiService: UIObservableService) {}
    listOfPrio: PrioritasPembangunan[];
    uiClient: UIObservableClient = new UIObservableClient;
    @Output() onFilterResultChanged = new EventEmitter;
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.prioService.getCollection().subscribe(value=> this.listOfPrio = value);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    prioritasPembangunanSelectedHandler(item: PrioritasPembangunan) {
        this.buildUiFilter(item).then(value => {
            this.uiService.notifyOnFilterChanged(value);
            this.onFilterResultChanged.emit();
        });
    }

    removeItemHandler(item: UIFilter) {
        this.updateUiFilter(item).then(value => {
            this.uiService.notifyOnFilterChanged(value);
        });
    }

    updateUiFilter(filter: UIFilter) {
        return new Promise<UIFilter[]>(resolver => {
            let i = this.uiClient.uiData.filter.length;
            while(i--) {
                if(this.uiClient.uiData.filter[i].category === filter.category) {
                    this.uiClient.uiData.filter.splice(i, 1);
                }
            }
            resolver(this.uiClient.uiData.filter);
        });
    }

    buildUiFilter(item: PrioritasPembangunan) {
        return new Promise<UIFilter[]>(resolver => {
            let filter: UIFilter[] = [];
            filter.push(new UIFilter(item.id, item.nama, "Prioritas Pembangunan"));
            resolver(filter);
        });
    }
}