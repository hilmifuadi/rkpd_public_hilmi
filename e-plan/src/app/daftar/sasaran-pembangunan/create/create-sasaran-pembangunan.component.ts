import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { JenisStandarService } from "../../../jenis/jenis-standar/jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIFilter } from "../../../infrastructur/data";
import { SasaranPembangunanService, SasaranPembangunan } from "../sasaran-pembangunan.service";
import { PrioritasPembangunanService } from "../../prioritas-pembangunan/prioritas-pembangunan.service";

@Component({
    selector: 'create-sasaran-pembangunan',
    templateUrl: './create-sasaran-pembangunan.component.html',
    styleUrls: [
        './create-sasaran-pembangunan.component.css'
    ]
})
export class CreateSasaranPembangunanComponent implements OnInit, OnDestroy { 
    constructor(private service: SasaranPembangunanService,
        private prioService: PrioritasPembangunanService,
        private uiService: UIObservableService) {}

    @Output() onItemCreated = new EventEmitter<SasaranPembangunan>();

    item: SasaranPembangunan = new SasaranPembangunan;
    uiClient: UIObservableClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.setNavigationProperty();
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }
    
    setNavigationProperty() {
        let i = this.uiClient.uiData.filter.length;
        while(i--) {
            if(this.uiClient.uiData.filter[i].category === "Prioritas Pembangunan") {
                this.prioService.read(this.uiClient.uiData.filter[i].id).subscribe(value => {
                    this.item.prioritasPembangunan =  value;
                });
            }
        }
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new SasaranPembangunan;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}