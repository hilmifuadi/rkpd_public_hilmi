import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { DasarHukum, DasarHukumService } from "./dasar-hukum.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './dasar-hukum.component.html'
})
export class DasarHukumComponent {
    constructor(private service: DasarHukumService, private uiService: UIObservableService) { }

    public items: DasarHukum[];
    public item = new DasarHukum;
    public title: string = 'Dasar Hukum';
    public uiClient = new UIObservableClient;

    public columns = ['nomor', 'isi'];
    public dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe(
            items => {
                this.items = items;
                this.dataSource.data = items;
                this.dataSource.sort = this.sort;
            },
            (error: any) => {

            },
            () => {

            }
        );
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if(found) {
            this.service.deletes(found).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
                    console.log(error);
                }
            );
        }
    }

    onItemCreated(item: DasarHukum) {
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemUpdated(item: DasarHukum) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemDeleted(item: DasarHukum) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items.sort();
    }

    rowSelectedHandler(data: DasarHukum): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: DasarHukum) {
        data.isSelected = !data.isSelected; 
    }
}