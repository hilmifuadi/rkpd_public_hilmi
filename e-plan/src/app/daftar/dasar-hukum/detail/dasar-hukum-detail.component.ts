import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DasarHukum, DasarHukumService } from "../dasar-hukum.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'dasar-hukum-detail',
    templateUrl: './dasar-hukum-detail.component.html'
})
export class DasarHukumDetailComponent {
    constructor(private service: DasarHukumService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: DasarHukum;
    @Output() onItemUpdated = new EventEmitter<DasarHukum>();
    @Output() onItemDeleted = new EventEmitter<DasarHukum>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}