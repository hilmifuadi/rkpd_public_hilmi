import { NgModule } from '@angular/core'

import { DasarHukumService } from "./dasar-hukum.service";

import { SharedModule } from "../../shared/shared.module";
import { DasarHukumUiModule } from "./dasar-hukum-ui.module";
import { DasarHukumRoutingModule } from './dasar-hukum-routing.module';

import { DasarHukumComponent } from './dasar-hukum.component';
import { CreateDasarHukumComponent } from "./create/create-dasar-hukum.component";
import { DasarHukumDetailComponent } from './detail/dasar-hukum-detail.component';
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

@NgModule({
    imports: [
        SharedModule,
        DasarHukumUiModule,
        DasarHukumRoutingModule,
        UIObservableModule
    ],
    declarations: [ 
        DasarHukumComponent,
        CreateDasarHukumComponent, DasarHukumDetailComponent
    ],
    providers: [
        DasarHukumService
    ]
})
export class DasarHukumModule { }