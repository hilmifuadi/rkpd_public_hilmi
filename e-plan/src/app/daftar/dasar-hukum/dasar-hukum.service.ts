import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class DasarHukum {
  constructor(
    public id: string = "", 
    public nomor: string = "", 
    public isi: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class DasarHukumService {

  constructor(private http: HttpClient) { }

  private api = 'api/daftar/dasar-hukum'

  getCollection() {
    return this.http.get<DasarHukum[]>(this.api);
  }

  create(entity: DasarHukum) {  
    return this.http.post<DasarHukum>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<DasarHukum> {
    return this.http.get<DasarHukum>(`${this.api}/${id}`);
  }

  update(entity: DasarHukum): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: DasarHukum): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: DasarHukum[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
