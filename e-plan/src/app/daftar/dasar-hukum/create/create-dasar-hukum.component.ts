import { Component, Output, EventEmitter } from "@angular/core";
import { DasarHukum, DasarHukumService } from "../dasar-hukum.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-dasar-hukum',
    templateUrl: './create-dasar-hukum.component.html'
})
export class CreateDasarHukumComponent {
    constructor(private service: DasarHukumService,  public uiService: UIObservableService) { }
    
    @Output() onItemCreated = new EventEmitter<DasarHukum>();

    public item = new DasarHukum;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new DasarHukum;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}