import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PrioritasProvinsiComponent } from './prioritas-provinsi.component'

const routes: Routes = [
    { 
        path: '', 
        component: PrioritasProvinsiComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class PrioritasProvinsiRoutingModule { }