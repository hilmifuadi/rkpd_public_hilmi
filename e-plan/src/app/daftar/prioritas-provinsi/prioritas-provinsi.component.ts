import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { PrioritasProvinsi, PrioritasProvinsiService } from "./prioritas-provinsi.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './prioritas-provinsi.component.html'
})
export class PrioritasProvinsiComponent {
    constructor(private service: PrioritasProvinsiService, private uiService: UIObservableService) { }

    public items: PrioritasProvinsi[];
    public item = new PrioritasProvinsi;

    public title: string = 'Prioritas Provinsi';

    public uiClient = new UIObservableClient;

    columns = ['nomor', 'nama'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data= value;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: PrioritasProvinsi) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: PrioritasProvinsi) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: PrioritasProvinsi) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: PrioritasProvinsi): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: PrioritasProvinsi) {
        data.isSelected = !data.isSelected;
    }
}