import { Component, Input, Output, EventEmitter } from "@angular/core";
import { PrioritasProvinsi, PrioritasProvinsiService } from "../prioritas-provinsi.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'prioritas-provinsi-detail',
    templateUrl: './prioritas-provinsi-detail.component.html'
})
export class PrioritasProvinsiDetailComponent {
    constructor(private service: PrioritasProvinsiService, private uiService: UIObservableService) { }
    
    public uiClient = new UIObservableClient;
    @Input() item: PrioritasProvinsi;
    @Output() onItemUpdated = new EventEmitter<PrioritasProvinsi>();
    @Output() onItemDeleted = new EventEmitter<PrioritasProvinsi>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}