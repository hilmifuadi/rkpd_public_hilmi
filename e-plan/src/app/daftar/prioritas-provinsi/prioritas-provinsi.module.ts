import { NgModule } from '@angular/core'

import { PrioritasProvinsiService } from "./prioritas-provinsi.service";

import { SharedModule } from "../../shared/shared.module";
import { PrioritasProvinsiUiModule } from "./prioritas-provinsi-ui.module";
import { PrioritasProvinsiRoutingModule } from './prioritas-provinsi-routing.module';

import { PrioritasProvinsiComponent } from './prioritas-provinsi.component';
import { CreatePrioritasProvinsiComponent } from "./create/create-prioritas-provinsi.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { PrioritasProvinsiDetailComponent } from './detail/prioritas-provinsi-detail.component';


@NgModule({
    imports: [
        SharedModule,
        PrioritasProvinsiUiModule,
        UIObservableModule,
        PrioritasProvinsiRoutingModule
    ],
    declarations: [ 
        PrioritasProvinsiComponent, PrioritasProvinsiDetailComponent,
        CreatePrioritasProvinsiComponent
    ],
    providers: [
        PrioritasProvinsiService
    ]
})
export class PrioritasProvinsiModule { }