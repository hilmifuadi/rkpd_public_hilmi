import { Component, Output, EventEmitter } from "@angular/core";
import { PrioritasProvinsi, PrioritasProvinsiService } from "../prioritas-provinsi.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-prioritas-provinsi',
    templateUrl: './create-prioritas-provinsi.component.html'
})
export class CreatePrioritasProvinsiComponent {
    constructor(private service: PrioritasProvinsiService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<PrioritasProvinsi>();

    public item = new PrioritasProvinsi;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new PrioritasProvinsi;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}