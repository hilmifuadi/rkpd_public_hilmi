import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class PrioritasProvinsi {
    constructor(
        public id: string = "",
        public nomor: string = "",
        public nama: string = "",
        public keterangan: string = "",
        public isSelected: boolean = false
    ) { }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PrioritasProvinsiService {

    constructor(private http: HttpClient) { }

    private api = 'api/daftar/prioritas-provinsi'

    getCollection() {
        return this.http.get<PrioritasProvinsi[]>(this.api);
    }

    create(entity: PrioritasProvinsi) {
        return this.http.post<PrioritasProvinsi>(this.api, entity, httpOptions);
    }

    read(id: string): Observable<PrioritasProvinsi> {
        return this.http.get<PrioritasProvinsi>(`${this.api}/${id}`);
    }

    update(entity: PrioritasProvinsi): Observable<boolean> {
        return this.http.put<boolean>(this.api, entity, httpOptions);
    }

    delete(item: PrioritasProvinsi) {
        return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
    }

    deletes(entities: PrioritasProvinsi[]): Observable<boolean> {
        return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
    }
}
