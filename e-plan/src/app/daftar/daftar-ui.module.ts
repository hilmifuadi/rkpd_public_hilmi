import { NgModule } from '@angular/core'
import { MatIconModule, MatListModule } from '@angular/material'

@NgModule({
  imports: [MatIconModule, MatListModule],
  exports: [MatIconModule, MatListModule]
})
export class DaftarUiModule { }
