import { NgModule } from '@angular/core'

import { DaftarUiModule } from './daftar-ui.module'
import { DaftarRoutingModule } from './daftar-routing.module'

import { DaftarComponent } from './daftar.component'

@NgModule({
    imports: [
        DaftarUiModule,
        DaftarRoutingModule
    ],
    declarations: [
        DaftarComponent
    ]
})
export class DaftarModule { }
