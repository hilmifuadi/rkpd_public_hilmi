import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { JenisStandar } from "../../jenis/jenis-standar/jenis-standar.service";

export class StandarHarga {
    constructor(
        public id: string = "",
        public nomor: string = "",
        public nama: string = "",
        public spesifikasi: string = "",
        public merk: string = "",
        public satuan: string = "",
        public harga: number = 0,
        public keterangan: string = "",
        public idJenisStandar: string = "",
        public jenisStandar: JenisStandar = new JenisStandar(),
        public isSelected: boolean = false) { }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const api = 'api/daftar/standar-harga';

@Injectable()
export class StandarHargaService {
    constructor(private http: HttpClient) { }

    getCollection() {
        return this.http.get<StandarHarga[]>(api);
    }

    create(entity: StandarHarga) {
        return this.http.post<StandarHarga>(
            api, 
            entity, 
            httpOptions);
    }

    read(id: string): Observable<StandarHarga> {
        return this.http.get<StandarHarga>(`${api}/${id}`);
    }

    update(entity: StandarHarga): Observable<boolean> {
        return this.http.put<boolean>(
            api, 
            entity, 
            httpOptions);
    }

    delete(entity: StandarHarga) {
        return this.http.post<boolean>(
            `${api}/delete`, 
            entity, 
            httpOptions);
    }

    deletes(entities: StandarHarga[]): Observable<boolean> {
        return this.http.post<boolean>(
            `${api}/deletes`, 
            entities, 
            httpOptions);
    }
}
