import { Component, ViewChild, OnInit } from "@angular/core";
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { StandarHarga, StandarHargaService } from "./standar-harga.service";
import { JenisStandarService, JenisStandar } from "../../jenis/jenis-standar/jenis-standar.service";
import { UIObservableService } from "../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../infrastructur/subscriber";
import { UIFilter } from "../../infrastructur/data";


@Component({
    templateUrl: './standar-harga.component.html',
    providers: [ 
        UIObservableService 
    ]
})
export class StandarHargaComponent implements OnInit {
    constructor(private service: StandarHargaService,
        private uiService: UIObservableService) { }

    items: StandarHarga[]= [];
    standarHarga: StandarHarga = new StandarHarga;
    listOfjenisStandar: JenisStandar[];

    columns = ['nomor', 'nama', 'spesifikasi', 'satuan', 'harga'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    title: string = 'Standar Harga';
    uiClient: UIObservableClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.dataSource.sort = this.sort;
    }

    fillDataSource() {
        if(this.uiClient.uiData.filter.length > 0) {
            this.service.getCollection().subscribe(value => {
                this.items = value;
                let i = this.uiClient.uiData.filter.length;
                while(i--) {
                    if(this.uiClient.uiData.filter[i].category === "Jenis Standar") {
                        this.items = this.items.filter(f => f.idJenisStandar.trim() === this.uiClient.uiData.filter[i].id.trim());
                    }
                }
                this.dataSource.data = this.items;
            });
        }
    }

    onSearchChanged(search: string) {
        this.dataSource.filter = search;
    }

    onFilterResultChanged() {
        this.fillDataSource();
    }

    onItemCreated(standarHarga: StandarHarga) {
        this.items.push(standarHarga);
        this.dataSource.data = this.items;
    }

    onItemDeleted(standarHarga: StandarHarga) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === standarHarga.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    onItemUpdated(standarHarga: StandarHarga) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === standarHarga.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(standarHarga);
        this.dataSource.data = this.items;
    }
    
    rowSelectedHandler(data: StandarHarga): void {
        this.standarHarga = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: StandarHarga) {
        data.isSelected = !data.isSelected;    
    }

    toogleSnackbarHandler() {
        this.uiService.notifyOnBinStateChanged();
    }

    deleteItemsHandler() {
        let found = this.items.filter(e => e.isSelected);
        if(found) {
            this.service.deletes(found).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
    
                }
            );
        }
    }
}