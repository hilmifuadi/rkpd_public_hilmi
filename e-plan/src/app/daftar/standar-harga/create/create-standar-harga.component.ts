import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { StandarHargaService, StandarHarga } from "../standar-harga.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { JenisStandarService } from "../../../jenis/jenis-standar/jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIFilter } from "../../../infrastructur/data";

@Component({
    selector: 'create-standar-harga',
    templateUrl: './create-standar-harga.component.html',
    styleUrls: [
        './create-standar-harga.component.css'
    ]
})
export class CreateStandarHargaComponent implements OnInit, OnDestroy { 
    constructor(private standarHargaService: StandarHargaService,
        private jenisStandarHargaService: JenisStandarService,
        private uiService: UIObservableService) {}

    @Output() onItemCreated = new EventEmitter<StandarHarga>();

    standarHarga: StandarHarga = new StandarHarga;
    uiClient: UIObservableClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.setNavigationProperty();
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }
    
    setNavigationProperty() {
        let i = this.uiClient.uiData.filter.length;
        while(i--) {
            if(this.uiClient.uiData.filter[i].category === "Jenis Standar") {
                this.jenisStandarHargaService.read(this.uiClient.uiData.filter[i].id).subscribe(value => {
                    this.standarHarga.jenisStandar =  value;
                });
            }
        }
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.standarHargaService.create(this.standarHarga).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.standarHarga = new StandarHarga;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}