import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { StandarHargaComponent } from './standar-harga.component'

const routes: Routes = [
    { 
        path: '', 
        component: StandarHargaComponent
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class StandarHargaRoutingModule { }