import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";

import { StandarHarga, StandarHargaService } from "../standar-harga.service";
import { JenisStandarService } from "../../../jenis/jenis-standar/jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'standar-harga-detail',
    templateUrl: './standar-harga-detail.component.html',
    styleUrls: [ './standar-harga-detail.component.css' ]
})
export class StandarHargaDetailComponent implements OnInit, OnDestroy {
    constructor(private standarHargaService: StandarHargaService,
        private jenisStandarHargaService: JenisStandarService,
        private uiService: UIObservableService) { }

    uiClient: UIObservableClient = new UIObservableClient;
    @Input() standarHarga: StandarHarga;
    @Output() onItemUpdated = new EventEmitter<StandarHarga>();
    @Output() onItemDeleted = new EventEmitter<StandarHarga>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }
    
    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.standarHargaService.update(this.standarHarga).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.standarHarga);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.standarHargaService.delete(this.standarHarga).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.standarHarga);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}