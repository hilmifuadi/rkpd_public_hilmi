import { Component, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIFilter } from "../../../infrastructur/data";
import { JenisStandar, JenisStandarService } from "../../../jenis/jenis-standar/jenis-standar.service";

@Component({
    selector: 'standar-harga-filter',
    templateUrl: './standar-harga-filter.component.html',
    styleUrls: ['./standar-harga-filter.component.css']
})
export class StandarHargaFilterComponent implements OnInit, OnDestroy {
    constructor(private uiService: UIObservableService, private jenisStandarService: JenisStandarService) { }
    uiClient: UIObservableClient = new UIObservableClient;
    listOfjenisStandar: JenisStandar[];
    @Output() onFilterResultChanged = new EventEmitter;
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.jenisStandarService.getCollection().subscribe(value => {
            this.listOfjenisStandar = value
        });
    }
    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }
    
    jenisStandarSelectedHandler(jenisStandar: JenisStandar) {
        this.buildUIFilter(jenisStandar).then(filter => {
            this.uiService.notifyOnFilterChanged(filter);
            this.onFilterResultChanged.emit();
        });
    }

    removeItemHandler(item: UIFilter) {
        this.updateUiFilter(item).then(value => {
            this.uiService.notifyOnFilterChanged(value);
        });
    }

    buildUIFilter(jenisStandar: JenisStandar) {
        return new Promise<UIFilter[]>(resolver => {
            let filter: UIFilter[] = [];
            filter.push(new UIFilter(jenisStandar.kode, jenisStandar.nama, "Jenis Standar"));
            resolver(filter);
        });
    }

    updateUiFilter(filter: UIFilter) {
        return new Promise<UIFilter[]>(resolver => {
            let i = this.uiClient.uiData.filter.length;
            while(i--) {
                if(this.uiClient.uiData.filter[i].category === filter.category) {
                    this.uiClient.uiData.filter.splice(i, 1);
                }
            }
            resolver(this.uiClient.uiData.filter);
        });
    }
}