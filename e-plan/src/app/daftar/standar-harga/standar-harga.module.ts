import { NgModule } from "@angular/core";

import { SharedModule } from "../../shared/shared.module";
import { StandarHargaUiModule } from "./standar-harga-ui.module";
import { StandarHargaRoutingModule } from "./standar-harga-routing.module";
import { JenisStandarModule } from "../../jenis/jenis-standar/jenis-standar.module";

import { StandarHargaComponent } from "./standar-harga.component";
import { StandarHargaDetailComponent } from "./detail/standar-harga-detail.component";
import { StandarHargaService } from "./standar-harga.service";
import { UIObservableModule } from "../../infrastructur/ui-observable.module";
import { CreateStandarHargaComponent } from "./create/create-standar-harga.component";
import { StandarHargaFilterComponent } from "./filter/standar-harga-filter.component";

@NgModule({
  imports: [
    SharedModule,
    StandarHargaUiModule,
    StandarHargaRoutingModule,
    UIObservableModule,
    JenisStandarModule
  ],
  declarations: [
    StandarHargaComponent,
    StandarHargaDetailComponent,
    CreateStandarHargaComponent,
    StandarHargaFilterComponent
  ],
  providers: [StandarHargaService]
})
export class StandarHargaModule { }
