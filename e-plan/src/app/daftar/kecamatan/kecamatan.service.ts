import { PrioritasPembangunan } from "../prioritas-pembangunan/prioritas-pembangunan.service";
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Kecamatan } from "../../shared/data/transactions";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const api = 'api/daftar/kecamatan';

@Injectable()
export class KecamatanService {
  constructor(private http: HttpClient) { }

  getCollection() {
    return this.http.get<Kecamatan[]>(api);
  }

  create(item: Kecamatan) {
    return this.http.post<Kecamatan>(
      api,
      item,
      httpOptions);
  }

  delete(item: Kecamatan) {
    return this.http.post<boolean>(
      `${api}/delete`,
      item,
      httpOptions);
  }

  deletes(items: Kecamatan[]) {
    return this.http.post<boolean>(
      `${api}/deletes`,
      items,
      httpOptions);
  }
}
