import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { KecamatanComponent } from "./kecamatan.component";

const routes: Routes = [
  {
    path: '',
    component: KecamatanComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KecamatanRoutingModule { }
