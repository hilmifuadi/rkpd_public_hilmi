import { Component, OnInit, ViewChild, ContentChild } from "@angular/core";
import { UIObservableService } from "../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../infrastructur/subscriber";
import { MatTableDataSource, MatSort } from "@angular/material";
import { UIFilter } from "../../infrastructur/data";
import { KecamatanService } from "./kecamatan.service";
import { UnitOrganisasiService } from "../unit-organisasi/unit-organisasi.service";
import { Tree } from "../../shared/tree/tree";
import { TreeHostComponent } from "../../shared/tree/tree-host.component";
import { TreeService } from "../../shared/tree/tree.service";
import { TreeClient } from "../../shared/tree/tree.client";
import { Kecamatan } from "../../shared/data/transactions";

@Component({
  templateUrl: './kecamatan.component.html',
  providers: [UIObservableService],
  styleUrls: ['./kecamatan.component.css']
})
export class KecamatanComponent implements OnInit {
  constructor(
    private service: KecamatanService,
    private uiService: UIObservableService) { }

  title: string = "Kecamatan";
  items: Kecamatan[] = [];
  item: Kecamatan = new Kecamatan;

  columns = ['kode', 'nama', 'alamat'];
  dataSource = new MatTableDataSource;
  @ViewChild(MatSort) sort: MatSort;

  public uiClient = new UIObservableClient;

  ngOnInit() {
    this.uiService.addObserver(this.uiClient);
    this.service.getCollection().subscribe(items => {
      this.items = items;
      this.dataSource.data = items;
    });
    this.dataSource.sort = this.sort;
  }

  onSearchChanged(search: string) {
    this.dataSource.filter = search;
  }

  onDelete() {
    let found = this.items.filter(e => e.isSelected);
    if (found) {
      this.service.deletes(found).subscribe(
        value => {
          if (value) {
            this.items = this.items.filter(e => !e.isSelected);
            this.dataSource.data = this.items;
          }
        },
        (error: any) => {

        }
      );
    }
  }

  onItemCreated(item: Kecamatan) {
    this.items.push(item);
    this.dataSource.data = this.items;
  }

  onItemDeleted(item: Kecamatan) {
    let i = this.items.length;
    while (i--) {
      if (this.items[i].id.trim() === item.id.trim()) {
        this.items.splice(i, 1);
        break;
      }
    }
    this.dataSource.data = this.items;
  }

  rowSelectedHandler(item: Kecamatan): void {
    this.item = item;
    this.uiService.notifyOnDetailStateChanged(true);
  }

  selectionHandler(item: Kecamatan) {
    item.isSelected = !item.isSelected;
  }
}
