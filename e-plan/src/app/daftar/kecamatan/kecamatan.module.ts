import { NgModule } from "@angular/core";
import { KecamatanComponent } from "./kecamatan.component";
import { KecamatanService } from "./kecamatan.service";
import { SharedModule } from "../../shared/shared.module";
import { UnitOrganisasiModule } from "../unit-organisasi/unit-organisasi.module";
import { KecamatanUiModule } from "./kecamatan-ui.module";
import { KecamatanRoutingModule } from "./kecamatan-routing.module";
import { UIObservableModule } from "../../infrastructur/ui-observable.module";
import { TreeModule } from "../../shared/tree/tree.module";
import { KecamatanDetailComponent } from "./detail/kecamatan-detail.component";
import { CreateKecamatanComponent } from "./create/create-kecamatan.component";

@NgModule({
  imports: [
    SharedModule,
    KecamatanUiModule,
    KecamatanRoutingModule,
    UIObservableModule,
    TreeModule,
    UnitOrganisasiModule,
  ],
  declarations: [
    KecamatanComponent, KecamatanDetailComponent, CreateKecamatanComponent
  ],
  providers: [KecamatanService]
})
export class KecamatanModule {}
