import { TreeService } from "../../../shared/tree/tree.service";
import { TreeClient } from "../../../shared/tree/tree.client";
import { OnInit, Component, OnDestroy, Output, EventEmitter } from "@angular/core";
import { UnitOrganisasiService } from "../../unit-organisasi/unit-organisasi.service";
import { Tree } from "../../../shared/tree/tree";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { KecamatanService } from "../kecamatan.service";
import { UnitOrganisasi, Kecamatan } from "../../../shared/data/transactions";

@Component({
    selector: 'create-kecamatan',
    templateUrl: 'create-kecamatan.component.html'
})
export class CreateKecamatanComponent implements OnInit, OnDestroy {
    constructor(
        private uiService: UIObservableService,
        private kecamatanService: KecamatanService,
        private unitOrganisasiService: UnitOrganisasiService) { }

    private uiClient = new UIObservableClient;
    public items: UnitOrganisasi[] = [];

    @Output() onItemCreated = new EventEmitter<Kecamatan>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.unitOrganisasiService.get(3, 'KECAMATAN').subscribe(
            organisasi => this.items = organisasi,
            (error: any) => { },
            () => { }
        );
        // this.treeService.addObserver(this.treeClient);
        // this.unitOrganisasiService.getCollection().subscribe(values => {
        //     values.forEach(value => {
        //         if(value.nama.trim().toLowerCase().match('kewilayahan')) {
        //             let tree = new Tree;
        //             tree.id = value.id;
        //             tree.text = `${value.kode} ${value.nama}`;
        //             tree.isExpanded = false;
        //             this.buildTreeData(tree, value.unit);
        //             this.tree.push(tree);
        //         }
        //     });

        //     if(this.treeClient.tree.length <= 0) {
        //         this.treeService.notifyOnTreeChanged(this.tree);
        //     }
        // });
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    itemSelectedHandler(item: UnitOrganisasi) {
        let kecamatan = new Kecamatan;
        kecamatan.unitOrganisasiKey = item.id;
        this.kecamatanService.create(kecamatan).subscribe(
            value => {
                this.uiService.notifyOnCreateStateChanged();
                this.onItemCreated.emit(value);
            },
            (error: any) => { },
            () => { }
        );
    }

    // buildTreeData(node: Tree, item: UnitOrganisasi[]) {
    //     item.forEach(value => {
    //         let tree = new Tree;
    //         tree.id = value.id;
    //         tree.text = `${value.kode} ${value.nama}`;
    //         tree.isExpanded = false;
    //         tree.level = node.level + 1;
    //         node.node.push(tree);
    //         if (value.unit.length > 0) {
    //             this.buildTreeData(tree, value.unit);
    //         }
    //     });
    // }
}