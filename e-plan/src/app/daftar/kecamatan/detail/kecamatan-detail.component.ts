import { Component, Input, Output, EventEmitter } from "@angular/core";
import { KecamatanService } from "../kecamatan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { Kecamatan } from "../../../shared/data/transactions";

@Component({
    selector: 'kecamatan-detail',
    templateUrl: './kecamatan-detail.component.html'
})
export class KecamatanDetailComponent {
    constructor(private service: KecamatanService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: Kecamatan;
    @Output() onItemDeleted = new EventEmitter<Kecamatan>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}