import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { UnitOrganisasiComponent } from './unit-organisasi.component'

const routes: Routes = [
    {
        path: '',
        component: UnitOrganisasiComponent
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class UnitOrganisasiRoutingModule {}