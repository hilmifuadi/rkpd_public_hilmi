import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { UnitOrganisasi } from "../../shared/data/transactions";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const api = 'api/daftar/unit-organisasi';

@Injectable()
export class UnitOrganisasiService {
    constructor(private httpClient: HttpClient) { }

    get(level: number, unit: string) {
        return this.httpClient.get<UnitOrganisasi[]>(`${api}/node/${level}/${unit}`);
    }

    getCollection() {
        return this.httpClient.get<UnitOrganisasi[]>(api);
    }
}