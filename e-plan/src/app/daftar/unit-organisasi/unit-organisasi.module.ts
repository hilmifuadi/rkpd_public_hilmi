import { NgModule } from '@angular/core'

import { UnitOrganisasiRoutingModule } from './unit-organisasi-routing.module'

import { UnitOrganisasiComponent } from './unit-organisasi.component'
import { UnitOrganisasiService } from './unit-organisasi.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        UnitOrganisasiRoutingModule
    ],
    declarations: [UnitOrganisasiComponent],
    providers: [UnitOrganisasiService]
})
export class UnitOrganisasiModule { }