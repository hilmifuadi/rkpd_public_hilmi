import { NgModule } from "@angular/core";
import { DesaComponent } from "./desa.component";
import { DesaService } from "./desa.service";
import { SharedModule } from "../../shared/shared.module";
import { UIObservableModule } from "../../infrastructur/ui-observable.module";
import { DesaRoutingModule } from "./desa-routing.module";
import { KecamatanModule } from "../kecamatan/kecamatan.module";
import { UnitOrganisasiModule } from "../unit-organisasi/unit-organisasi.module";
import { DesaUiModule } from "./desa-ui.module";
import { DesaDetailComponent } from "./detail/desa-detail.component";

@NgModule({
    imports: [
        SharedModule,
        UIObservableModule,
        DesaUiModule,
        DesaRoutingModule,
        KecamatanModule,
        UnitOrganisasiModule
    ],
    declarations: [
        DesaComponent,
        DesaDetailComponent
    ],
    providers: [
        DesaService
    ]
})
export class DesaModule {}