import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Desa } from "../../shared/data/transactions";
import { ActionOfDesa } from "../../shared/data/actions";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const api = 'api/daftar/desa';

@Injectable()
export class DesaService {

    constructor(private http: HttpClient) { }

    gets() {
        return this.http.get<Desa[]>(api);
    }

    create(item: Desa) {
        return this.http.post<Desa>(
            api,
            item,
            httpOptions);
    }

    delete(item: Desa) {
        return this.http.post<boolean>(
            `${api}/delete`,
            item,
            httpOptions);
    }

    deletes(items: Desa[]) {
        return this.http.post<boolean>(
            `${api}/deletes`,
            items,
            httpOptions);
    }
}