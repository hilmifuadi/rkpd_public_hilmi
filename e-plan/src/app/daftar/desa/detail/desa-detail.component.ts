import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DesaService } from "../desa.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { Desa } from "../../../shared/data/transactions";

@Component({
    selector: 'desa-detail',
    templateUrl: './desa-detail.component.html'
})
export class DesaDetailComponent {
    constructor(private service: DesaService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: Desa;
    @Output() onItemDeleted = new EventEmitter<Desa>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}