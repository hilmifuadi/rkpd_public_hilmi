import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DesaComponent } from "./desa.component";

const routes: Routes = [
    {
        path: '',
        component: DesaComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DesaRoutingModule { }
