import { Component, ViewChild } from "@angular/core";
import { UIObservableService } from "../../infrastructur/ui-observable.service";
import { MatTableDataSource, MatSort } from "@angular/material";
import { UIObservableClient } from "../../infrastructur/subscriber";
import { Desa } from "../../shared/data/transactions";
import { DesaService } from "./desa.service";

@Component({
    templateUrl: './desa.component.html'
})
export class DesaComponent {
    constructor(private service: DesaService, private uiService: UIObservableService) { }

    items: Desa[] = [];
    item: Desa = new Desa;

    columns = ['kode', 'nama'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    title: string = 'Desa';
    uiClient: UIObservableClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.dataSource.sort = this.sort;
    }

    fillDataSource() {
        if (this.uiClient.uiData.filter.length > 0) {
            this.service.gets().subscribe(value => {
                this.items = value;
                let i = this.uiClient.uiData.filter.length;
                while (i--) {
                    if (this.uiClient.uiData.filter[i].category === "Kecamatan") {
                        this.items = this.items.filter(f => f.kecamatanId.trim() === this.uiClient.uiData.filter[i].id.trim());
                    }
                }
                this.dataSource.data = this.items;
            });
        }
    }

    onSearchChanged(search: string) {
        this.dataSource.filter = search;
    }

    onFilterResultChanged() {
        this.fillDataSource();
    }

    onItemCreated(standarHarga: Desa) {
        this.items.push(standarHarga);
        this.dataSource.data = this.items;
    }

    onItemDeleted(standarHarga: Desa) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === standarHarga.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    onItemUpdated(standarHarga: Desa) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === standarHarga.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(standarHarga);
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: Desa): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: Desa) {
        data.isSelected = !data.isSelected;
    }

    toogleSnackbarHandler() {
        this.uiService.notifyOnBinStateChanged();
    }

    deleteItemsHandler() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }
}