import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { DaftarComponent } from './daftar.component'

const routes: Routes = [
    {
        path: '',
        component: DaftarComponent,
        outlet: 'outlet-daftar'
    },
    {
        path: 'dasar-hukum',
        loadChildren: 'app/daftar/dasar-hukum/dasar-hukum.module#DasarHukumModule'
    },
    {
        path: 'prioritas-provinsi',
        loadChildren: 'app/daftar/prioritas-provinsi/prioritas-provinsi.module#PrioritasProvinsiModule'
    },
    {
        path: 'unit-organisasi',
        loadChildren: 'app/daftar/unit-organisasi/unit-organisasi.module#UnitOrganisasiModule'
    },
    {
        path: 'prioritas-pembangunan',
        loadChildren: 'app/daftar/prioritas-pembangunan/prioritas-pembangunan.module#PrioritasPembangunanModule'
    },
    {
        path: 'standar-harga',
        loadChildren: 'app/daftar/standar-harga/standar-harga.module#StandarHargaModule'
    },
    {
        path: 'sasaran-pembangunan',
        loadChildren: 'app/daftar/sasaran-pembangunan/sasaran-pembangunan.module#SasaranPembangunanModule'
    },
    {
        path: 'kecamatan',
        loadChildren: 'app/daftar/kecamatan/kecamatan.module#KecamatanModule'
    },
    {
        path: 'desa',
        loadChildren: 'app/daftar/desa/desa.module#DesaModule'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DaftarRoutingModule { }