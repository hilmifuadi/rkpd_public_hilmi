import { Component, Output, EventEmitter } from "@angular/core";
import { PrioritasPembangunan, PrioritasPembangunanService } from "../prioritas-pembangunan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-prioritas-pembangunan',
    templateUrl: './create-prioritas-pembangunan.component.html'
})
export class CreatePrioritasPembangunanComponent {
    constructor(private service: PrioritasPembangunanService, public uiService: UIObservableService) { }
    
    @Output() onItemCreated = new EventEmitter<PrioritasPembangunan>();

    public item = new PrioritasPembangunan;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new PrioritasPembangunan;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}