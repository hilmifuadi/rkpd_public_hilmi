import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { PrioritasPembangunan, PrioritasPembangunanService } from "./prioritas-pembangunan.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './prioritas-pembangunan.component.html'
})
export class PrioritasPembangunanComponent {
    constructor(private service: PrioritasPembangunanService, private uiService: UIObservableService) { }

    public items: PrioritasPembangunan[];
    public item = new PrioritasPembangunan;
    public title: string = 'Prioritas Pembangunan';
    public uiClient = new UIObservableClient;

    public columns = ['nomor', 'nama'];
    public dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe(
            items => {
                this.items = items;
                this.dataSource.data = items;
                this.dataSource.sort = this.sort;
            },
            (error: any) => {

            },
            () => {

            }
        );
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
                    console.log(error);
                }
            );
        }
    }

    onItemCreated(item: PrioritasPembangunan) {
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemUpdated(item: PrioritasPembangunan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemDeleted(item: PrioritasPembangunan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items.sort();
    }

    rowSelectedHandler(data: PrioritasPembangunan): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: PrioritasPembangunan) {
        data.isSelected = !data.isSelected;
    }
}