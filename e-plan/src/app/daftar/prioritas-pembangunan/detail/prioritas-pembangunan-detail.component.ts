import { Component, Input, Output, EventEmitter } from "@angular/core";
import { PrioritasPembangunan, PrioritasPembangunanService } from "../prioritas-pembangunan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'prioritas-pembangunan-detail',
    templateUrl: './prioritas-pembangunan-detail.component.html'
})
export class PrioritasPembangunanDetailComponent {
    constructor(private service: PrioritasPembangunanService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: PrioritasPembangunan;
    @Output() onItemUpdated = new EventEmitter<PrioritasPembangunan>();
    @Output() onItemDeleted = new EventEmitter<PrioritasPembangunan>();

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}