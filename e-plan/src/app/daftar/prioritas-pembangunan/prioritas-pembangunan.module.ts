import { NgModule } from '@angular/core'

import { PrioritasPembangunanService } from "./prioritas-pembangunan.service";

import { SharedModule } from "../../shared/shared.module";
import { PrioritasPembangunanUiModule } from "./prioritas-pembangunan-ui.module";
import { PrioritasPembangunanRoutingModule } from './prioritas-pembangunan-routing.module';

import { PrioritasPembangunanComponent} from './prioritas-pembangunan.component';
import { CreatePrioritasPembangunanComponent } from "./create/create-prioritas-pembangunan.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { PrioritasPembangunanDetailComponent } from './detail/prioritas-pembangunan-detail.component';

@NgModule({
    imports: [
        SharedModule,
        PrioritasPembangunanUiModule,
        PrioritasPembangunanRoutingModule,
        UIObservableModule
    ],
    declarations: [ 
        PrioritasPembangunanComponent,
        CreatePrioritasPembangunanComponent, PrioritasPembangunanDetailComponent
    ],
    providers: [
        PrioritasPembangunanService
    ]
})
export class PrioritasPembangunanModule { }