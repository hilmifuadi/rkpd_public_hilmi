import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PrioritasPembangunanComponent } from './prioritas-pembangunan.component'

const routes: Routes = [
    { 
        path: '', 
        component: PrioritasPembangunanComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class PrioritasPembangunanRoutingModule { }