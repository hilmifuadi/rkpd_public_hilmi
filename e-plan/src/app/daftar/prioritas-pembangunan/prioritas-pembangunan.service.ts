import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class PrioritasPembangunan {
    constructor(
        public id: string = "",
        public nomor: string = "",
        public nama: string = "",
        public sasaran: string = "",
        public keterangan: string = "",
        public isSelected: boolean = false
    ) { }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PrioritasPembangunanService {

    constructor(private http: HttpClient) { }

    private api = 'api/daftar/prioritas-pembangunan'

    getCollection() {
        return this.http.get<PrioritasPembangunan[]>(this.api);
    }

    create(entity: PrioritasPembangunan) {
        return this.http.post<PrioritasPembangunan>(this.api, entity, httpOptions);
    }

    read(id: string): Observable<PrioritasPembangunan> {
        return this.http.get<PrioritasPembangunan>(`${this.api}/${id}`);
    }

    update(entity: PrioritasPembangunan): Observable<boolean> {
        return this.http.put<boolean>(this.api, entity, httpOptions);
    }

    delete(item: PrioritasPembangunan): Observable<boolean> {
        return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
    }

    deletes(entities: PrioritasPembangunan[]): Observable<boolean> {
        return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
    }
}
