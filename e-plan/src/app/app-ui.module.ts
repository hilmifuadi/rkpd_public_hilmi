import { NgModule } from '@angular/core'
import { MatSidenavModule, MatIconModule, MatExpansionModule, MatButtonModule, MatToolbarModule, MatListModule } from '@angular/material'

@NgModule({
  imports: [MatIconModule, MatSidenavModule, MatExpansionModule, MatButtonModule, MatToolbarModule, MatListModule],
  exports: [MatIconModule, MatSidenavModule, MatExpansionModule, MatButtonModule, MatToolbarModule, MatListModule]
})
export class AppUiModule { }
