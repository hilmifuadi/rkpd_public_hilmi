import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module'
import { AppUiModule } from './app-ui.module'
import { CoreModule } from './core/core.module'

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found.component'

import { MAT_LABEL_GLOBAL_OPTIONS } from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppUiModule,
    CoreModule,
    AppRoutingModule
  ],
  providers: [
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
