import { NgModule } from '@angular/core'
import { MatIconModule, MatListModule, MatButtonModule } from '@angular/material'

@NgModule({
  imports: [MatIconModule, MatListModule, MatButtonModule],
  exports: [MatIconModule, MatListModule, MatButtonModule]
})
export class RpjmdUiModule { }
