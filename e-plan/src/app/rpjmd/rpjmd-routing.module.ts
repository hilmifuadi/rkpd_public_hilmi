import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { RpjmdComponent } from './rpjmd.component'

const routes: Routes = [
    { 
        path: '', 
        component: RpjmdComponent,
        outlet: 'outlet-rpjmd'
    },
    {
        path: 'visi',
        loadChildren: 'app/rpjmd/visi/visi.module#VisiModule'
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class RpjmdRoutingModule { }
