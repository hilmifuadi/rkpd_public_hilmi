import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { VisiService } from "../visi.service";
import { log } from "util";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { Visi } from "../../../shared/data/transactions";

@Component({
    selector: 'visi-detail',
    templateUrl: './visi-detail.component.html'
})
export class VisiDetailComponent implements OnInit, OnDestroy {
    constructor(private visiService: VisiService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: Visi;
    @Output() onItemUpdated = new EventEmitter<Visi>();
    @Output() onItemDeleted = new EventEmitter<Visi>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.visiService.update(this.item).subscribe(
            value => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(value.isError);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.visiService.delete(this.item.id).subscribe(
            value => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(value.isError);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}