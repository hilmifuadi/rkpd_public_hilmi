import { Component, ViewChild, OnDestroy, OnInit } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { VisiService } from "./visi.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';
import { Visi } from '../../shared/data/transactions';

@Component({
    templateUrl: './visi.component.html'
})
export class VisiComponent implements OnInit, OnDestroy {
    constructor(private service: VisiService, private uiService: UIObservableService) { }

    public items: Visi[];
    public item = new Visi;
    public title: string = 'Visi';
    public uiClient = new UIObservableClient;

    public columns = ['nomor', 'uraian'];
    public dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe(
            items => {
                this.items = items;
                this.dataSource.data = items;
                this.dataSource.sort = this.sort;
            },
            (error: any) => {

            },
            () => {

            }
        );
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        let keys = [];
        found.forEach((value,index) => 
            keys[index] = value.id)
        if(found) {
            this.service.deletes(keys).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
                    console.log(error);
                }
            );
        }
    }

    onItemCreated(item: Visi) {
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemUpdated(item: Visi) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemDeleted(item: Visi) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].id.trim() === item.id.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items.sort();
    }

    rowSelectedHandler(data: Visi): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: Visi) {
        data.isSelected = !data.isSelected; 
    }
}