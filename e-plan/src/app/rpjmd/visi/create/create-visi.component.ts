import { Component, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { VisiService } from "../visi.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { Visi } from "../../../shared/data/transactions";

@Component({
    selector: 'create-visi',
    templateUrl: './create-visi.component.html'
})
export class CreateVisiComponent implements OnInit, OnDestroy {
    constructor(private visiService: VisiService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<Visi>();

    public item = new Visi;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.visiService.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new Visi;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}