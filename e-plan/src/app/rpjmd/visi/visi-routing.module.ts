import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { VisiComponent } from './visi.component'

const routes: Routes = [
    { 
        path: '', 
        component: VisiComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class VisiRoutingModule { }