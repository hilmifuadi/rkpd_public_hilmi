import { NgModule } from '@angular/core'

import { VisiService } from "./visi.service";
import { VisiUiModule } from "./visi-ui.module";
import { VisiRoutingModule } from './visi-routing.module';
import { SharedModule } from "../../shared/shared.module";

import { VisiComponent } from './visi.component';
import { CreateVisiComponent } from "./create/create-visi.component";
import { VisiDetailComponent } from "./detail/visi-detail.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

@NgModule({
    imports: [
        SharedModule,
        VisiUiModule,
        UIObservableModule,
        VisiRoutingModule
    ],
    declarations: [ 
        VisiComponent,
        CreateVisiComponent, VisiDetailComponent,
    ],
    providers: [
        VisiService
    ]
})
export class VisiModule { }