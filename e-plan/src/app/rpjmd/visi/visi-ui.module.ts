import { NgModule } from '@angular/core'
import { 
  MatToolbarModule, 
  MatIconModule,
  MatButtonModule, 
  MatListModule, 
  MatInputModule, 
  MatTableModule, 
  MatPaginatorModule, 
  MatSortModule } from '@angular/material'

@NgModule({
  imports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatSortModule,
    MatPaginatorModule,
  ],
  exports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatSortModule,
    MatPaginatorModule,
  ]
})
export class VisiUiModule { }
