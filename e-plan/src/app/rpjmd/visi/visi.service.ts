import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { Visi } from '../../shared/data/transactions';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class VisiService {

  constructor(private http: HttpClient) { }

  private api = 'api/rpjmd/visi'

  getCollection() {
    return this.http.get<Visi[]>(this.api);
  }

  create(entity: Visi) {  
    return this.http.post<Visi>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<Visi> {
    return this.http.get<Visi>(`${this.api}/${id}`);
  }

  update(entity: Visi): Observable<Visi> {
    return this.http.put<Visi>(`${this.api}/${entity.id}`, entity, httpOptions);
  }

  delete(id: string) {
    return this.http.post<Visi>(`${this.api}/delete/${id}`, httpOptions);
  }

  deletes(keys: string[]): Observable<Visi[]> {
    return this.http.post<Visi[]>(`${this.api}/deletes`, keys, httpOptions);
  }
}
