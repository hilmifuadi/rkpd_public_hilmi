import { NgModule } from '@angular/core'

import { RpjmdUiModule } from './rpjmd-ui.module'
import { RpjmdRoutingModule } from './rpjmd-routing.module';
import { RpjmdComponent } from './rpjmd.component';


@NgModule({
  imports: [
        RpjmdUiModule,
        RpjmdRoutingModule
    ],
    declarations: [ 
       RpjmdComponent
    ]
})
export class RpjmdModule { }
