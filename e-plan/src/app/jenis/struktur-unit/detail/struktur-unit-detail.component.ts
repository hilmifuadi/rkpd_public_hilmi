import { Component, Input, Output, EventEmitter } from "@angular/core";
import { StrukturUnitService } from "../struktur-unit.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { StrukturUnit } from "../../../shared/data/transactions";

@Component({
    selector: 'struktur-unit-detail',
    templateUrl: './struktur-unit-detail.component.html'
})
export class StrukturUnitDetailComponent {
    constructor(private service: StrukturUnitService, private uiService: UIObservableService) {}

    public uiClient = new UIObservableClient;
    @Input() item: StrukturUnit;
    @Output() onItemUpdated = new EventEmitter<StrukturUnit>();
    @Output() onItemDeleted = new EventEmitter<StrukturUnit>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}