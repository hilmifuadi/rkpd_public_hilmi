import { NgModule } from '@angular/core'

import { StrukturUnitRoutingModule } from './struktur-unit-routing.module'
import { StrukturUnitUiModule } from './struktur-unit-ui.module'
import { SharedModule } from '../../shared/shared.module'

import { StrukturUnitComponent } from './struktur-unit.component'
import { CreateStrukturUnitComponent } from "./create/create-struktur-unit.component";

import { StrukturUnitService } from './struktur-unit.service'
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { StrukturUnitDetailComponent } from './detail/struktur-unit-detail.component';

@NgModule({
  imports: [
    SharedModule,
    StrukturUnitUiModule,
    UIObservableModule,
    StrukturUnitRoutingModule
  ],
  declarations: [ 
      StrukturUnitComponent,
      CreateStrukturUnitComponent, StrukturUnitDetailComponent
  ],
  providers: [
    StrukturUnitService
  ]
})
export class StrukturUnitModule { }
