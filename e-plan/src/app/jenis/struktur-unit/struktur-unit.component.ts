import { Component, OnInit, ViewChild } from '@angular/core'
import { StrukturUnitService } from './struktur-unit.service'
import { MatSidenav, MatSort, MatTableDataSource } from "@angular/material";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';
import { StrukturUnit } from '../../shared/data/transactions';

@Component({
    templateUrl: './struktur-unit.component.html'
})
export class StrukturUnitComponent implements OnInit {
    constructor(private service: StrukturUnitService, private uiService: UIObservableService) { }

    public items: StrukturUnit[];
    public item = new StrukturUnit;

    public title: string = 'Struktur Unit';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'uraian'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: StrukturUnit) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: StrukturUnit) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: StrukturUnit) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: StrukturUnit): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: StrukturUnit) {
        data.isSelected = !data.isSelected;
    }
}
