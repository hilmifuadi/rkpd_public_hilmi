import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { StrukturUnitComponent } from './struktur-unit.component'

const routes: Routes = [
  {
    path: '', component: StrukturUnitComponent
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StrukturUnitRoutingModule { }
