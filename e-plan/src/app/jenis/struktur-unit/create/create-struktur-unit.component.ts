import { Component, Output, EventEmitter, OnInit } from "@angular/core";
import { StrukturUnitService } from "../struktur-unit.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { StrukturUnit } from "../../../shared/data/transactions";

@Component({
    selector: 'create-struktur-unit',
    templateUrl: './create-struktur-unit.component.html'
})
export class CreateStrukturUnitComponent implements OnInit {
    constructor(private service: StrukturUnitService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<StrukturUnit>();

    public item = new StrukturUnit;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new StrukturUnit;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}