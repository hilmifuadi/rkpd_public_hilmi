import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { StrukturUnit } from '../../shared/data/transactions';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class StrukturUnitService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/struktur-unit'

  getCollection() {
    return this.http.get<StrukturUnit[]>(this.api);
  }

  create(entity: StrukturUnit) {  
    return this.http.post<StrukturUnit>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<StrukturUnit> {
    return this.http.get<StrukturUnit>(`${this.api}/${id}`);
  }

  update(entity: StrukturUnit): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: StrukturUnit) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: StrukturUnit[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
