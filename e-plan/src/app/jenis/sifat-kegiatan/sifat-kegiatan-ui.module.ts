import { NgModule } from '@angular/core'
import { MatToolbarModule, MatIconModule, MatButtonModule, MatTableModule, MatListModule, MatInputModule, MatSortModule, MatPaginatorModule } from '@angular/material';

@NgModule({
  imports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatSortModule,
    MatPaginatorModule,
  ],
  exports: [ 
    MatToolbarModule, 
    MatIconModule, 
    MatButtonModule, 
    MatTableModule, 
    MatListModule, 
    MatInputModule, 
    MatSortModule,
    MatPaginatorModule,
  ]
})
export class SifatKegiatanUiModule { }
