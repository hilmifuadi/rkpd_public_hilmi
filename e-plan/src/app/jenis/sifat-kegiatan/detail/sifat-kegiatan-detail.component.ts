import { Component, Input, Output, EventEmitter } from "@angular/core";
import { SifatKegiatan, SifatKegiatanService } from "../sifat-kegiatan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
 
@Component({
    selector: 'sifat-kegiatan-detail.',
    templateUrl: './sifat-kegiatan-detail.component.html'
})
export class SifatKegiatanDetailComponent {
    constructor(private service: SifatKegiatanService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: SifatKegiatan;
    @Output() onItemUpdated = new EventEmitter<SifatKegiatan>();
    @Output() onItemDeleted = new EventEmitter<SifatKegiatan>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}