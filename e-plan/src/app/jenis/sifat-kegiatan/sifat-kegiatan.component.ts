import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatSort, MatTableDataSource } from "@angular/material";

import { SifatKegiatan, SifatKegiatanService } from "./sifat-kegiatan.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './sifat-kegiatan.component.html'
})
export class SifatKegiatanComponent {
    constructor(private service: SifatKegiatanService, private uiService: UIObservableService) { }

    public items: SifatKegiatan[];
    public item = new SifatKegiatan;

    public title: string = 'Sifat Kegiatan';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'uraian'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: SifatKegiatan) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: SifatKegiatan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: SifatKegiatan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: SifatKegiatan): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: SifatKegiatan) {
        data.isSelected = !data.isSelected;
    }
}