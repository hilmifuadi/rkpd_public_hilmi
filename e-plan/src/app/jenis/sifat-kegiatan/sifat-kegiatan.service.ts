import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class SifatKegiatan {
  constructor(
    public kode: string = "", 
    public uraian: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SifatKegiatanService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/sifat-kegiatan'

  getCollection() {
    return this.http.get<SifatKegiatan[]>(this.api);
  }

  create(entity: SifatKegiatan) {  
    return this.http.post<SifatKegiatan>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<SifatKegiatan> {
    return this.http.get<SifatKegiatan>(`${this.api}/${id}`);
  }

  update(entity: SifatKegiatan): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: SifatKegiatan) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: SifatKegiatan[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
