import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { SifatKegiatanUiModule } from "./sifat-kegiatan-ui.module";
import { SifatKegiatanRoutingModule } from './sifat-kegiatan-routing.module'
import { SifatKegiatanService } from "./sifat-kegiatan.service";
import { SifatKegiatanComponent } from './sifat-kegiatan.component';
import { CreateSifatKegiatanComponent } from "./create/create-sifat-kegiatan.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { SifatKegiatanDetailComponent } from './detail/sifat-kegiatan-detail.component';

@NgModule({
    imports: [
        SharedModule, 
        SifatKegiatanUiModule,
        UIObservableModule,
        SifatKegiatanRoutingModule
    ],
    declarations: [ 
        SifatKegiatanComponent, CreateSifatKegiatanComponent,
        SifatKegiatanDetailComponent
    ],
    providers: [
        SifatKegiatanService
    ]
})
export class SifatKegiatanModule { }