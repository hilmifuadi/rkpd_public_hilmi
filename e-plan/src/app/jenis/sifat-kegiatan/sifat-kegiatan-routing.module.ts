import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SifatKegiatanComponent } from './sifat-kegiatan.component'

const routes: Routes = [
    { 
        path: '', 
        component: SifatKegiatanComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class SifatKegiatanRoutingModule { }