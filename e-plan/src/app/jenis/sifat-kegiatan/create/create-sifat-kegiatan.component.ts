import { Component, Output, EventEmitter } from "@angular/core";
import { SifatKegiatan, SifatKegiatanService } from "../sifat-kegiatan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-sifat-kegiatan',
    templateUrl: './create-sifat-kegiatan.component.html'
})
export class CreateSifatKegiatanComponent {
    constructor(private service: SifatKegiatanService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<SifatKegiatan>();

    public item = new SifatKegiatan;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new SifatKegiatan;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}