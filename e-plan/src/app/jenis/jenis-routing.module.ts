import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { JenisComponent } from './jenis.component'

const routes: Routes = [
    { 
        path: '', 
        component: JenisComponent,
        outlet: 'outlet-jenis'
    },
    {
        path: 'struktur-unit',
        loadChildren: 'app/jenis/struktur-unit/struktur-unit.module#StrukturUnitModule'
    },
    {
        path: 'struktur-rekening',
        loadChildren: 'app/jenis/struktur-rekening/struktur-rekening.module#StrukturRekeningModule'
    },
    {
        path: 'golongan',
        loadChildren: 'app/jenis/golongan/golongan.module#GolonganModule'
    },
    {
        path: 'jenis-perspektif',
        loadChildren: 'app/jenis/jenis-perspektif/jenis-perspektif.module#JenisPerspektifModule'
    },
    {
        path: 'jenis-dana',
        loadChildren: 'app/jenis/jenis-dana/jenis-dana.module#JenisDanaModule'
    },
    {
        path: 'jenis-kinerja-kegiatan',
        loadChildren: 'app/jenis/jenis-kinerja-kegiatan/jenis-kinerja-kegiatan.module#JenisKinerjaKegiatanModule'
    },
    {
        path: 'sifat-kegiatan',
        loadChildren: 'app/jenis/sifat-kegiatan/sifat-kegiatan.module#SifatKegiatanModule'
    },
    {
        path: 'jenis-standar',
        loadChildren: 'app/jenis/jenis-standar/jenis-standar.module#JenisStandarModule'
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class JenisRoutingModule { }
