import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { JenisDana, JenisDanaService } from "./jenis-dana.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './jenis-dana.component.html'
})
export class JenisDanaComponent implements OnInit, OnDestroy {
    constructor(private service: JenisDanaService, private uiService: UIObservableService) { }

    public items: JenisDana[];
    public item = new JenisDana;

    public title: string = 'Jenis Dana';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'nama'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
            this.dataSource.sort = this.sort;
        });
        this.dataSource.sort = this.sort;
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if(found) {
            this.service.deletes(found).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
    
                }
            );
        }
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onItemCreated(item: JenisDana) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: JenisDana) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: JenisDana) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: JenisDana): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: JenisDana) {
        data.isSelected = !data.isSelected; 
    }
}