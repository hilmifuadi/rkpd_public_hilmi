import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/observable";

export class JenisDana {
    constructor(
        public kode: string = "",
        public nama: string = "",
        public keterangan: string = "",
        public isSelected: boolean = false) { }
}

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class JenisDanaService {

    constructor(private http: HttpClient) { }

    private api = 'api/jenis/jenis-dana'

    getCollection() {
        return this.http.get<JenisDana[]>(this.api);
    }

    create(entity: JenisDana) {
        return this.http.post<JenisDana>(this.api, entity, httpOptions);
    }

    read(id: string): Observable<JenisDana> {
        return this.http.get<JenisDana>(`${this.api}/${id}`);
    }

    update(entity: JenisDana): Observable<boolean> {
        return this.http.put<boolean>(this.api, entity, httpOptions);
    }

    delete(item: JenisDana) {
        return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
    }

    deletes(entities: JenisDana[]): Observable<boolean> {
        return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
    }
}
