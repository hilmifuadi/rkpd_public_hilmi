import { Component, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { JenisDana, JenisDanaService } from "../jenis-dana.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-jenis-dana',
    templateUrl: './create-jenis-dana.component.html'
})
export class CreateJenisDanaComponent implements OnInit, OnDestroy {
    constructor(private jenisDanaService: JenisDanaService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<JenisDana>();

    public item = new JenisDana;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.jenisDanaService.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new JenisDana;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}