import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { JenisDanaUiModule } from "./jenis-dana-ui.module";
import { JenisDanaRoutingModule } from './jenis-dana-routing.module'

import { JenisDanaService } from "./jenis-dana.service";

import { JenisDanaComponent } from './jenis-dana.component';
import { CreateJenisDanaComponent } from "./create/create-jenis-dana.component";
import { JenisDanaDetailComponent } from "./detail/jenis-dana-detail.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

@NgModule({
    imports: [
        SharedModule,
        JenisDanaUiModule,
        UIObservableModule,
        JenisDanaRoutingModule
    ],
    declarations: [ 
        JenisDanaComponent,
        CreateJenisDanaComponent, JenisDanaDetailComponent
    ],
    providers: [
        JenisDanaService
    ]
})
export class JenisDanaModule { }