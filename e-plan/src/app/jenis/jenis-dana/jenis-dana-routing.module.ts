import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { JenisDanaComponent } from './jenis-dana.component'

const routes: Routes = [
    { 
        path: '', 
        component: JenisDanaComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class JenisDanaRoutingModule { }