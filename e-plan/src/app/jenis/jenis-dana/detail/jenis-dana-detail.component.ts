import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { JenisDana, JenisDanaService} from "../jenis-dana.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'jenis-dana-detail',
    templateUrl: './jenis-dana-detail.component.html'
})
export class JenisDanaDetailComponent implements OnInit, OnDestroy {
    constructor(private jenisDanaService: JenisDanaService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: JenisDana;
    @Output() onItemUpdated = new EventEmitter<JenisDana>();
    @Output() onItemDeleted = new EventEmitter<JenisDana>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.jenisDanaService.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.jenisDanaService.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}