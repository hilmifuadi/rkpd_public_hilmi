import { NgModule } from '@angular/core'

import { GolonganService } from "./golongan.service";
import { GolonganUiModule } from "./golongan-ui.module";
import { GolonganRoutingModule } from './golongan-routing.module';
import { SharedModule } from "../../shared/shared.module";

import { GolonganComponent } from './golongan.component';
import { CreateGolonganComponent } from "./create/create-golongan.component";
import { GolonganDetailComponent } from "./detail/golongan-detail.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

@NgModule({
    imports: [
        SharedModule,
        GolonganUiModule,
        UIObservableModule,
        GolonganRoutingModule
    ],
    declarations: [ 
        GolonganComponent,
        CreateGolonganComponent, GolonganDetailComponent,
    ],
    providers: [
        GolonganService
    ]
})
export class GolonganModule { }