import { Component, ViewChild, OnDestroy, OnInit } from '@angular/core'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";

import { Golongan, GolonganService } from "./golongan.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './golongan.component.html'
})
export class GolonganComponent implements OnInit, OnDestroy {
    constructor(private service: GolonganService, private uiService: UIObservableService) { }

    public items: Golongan[];
    public item = new Golongan;
    public title: string = 'Golongan';
    public uiClient = new UIObservableClient;

    public columns = ['kode', 'nama', 'pangkat'];
    public dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe(
            items => {
                this.items = items;
                this.dataSource.data = items;
                this.dataSource.sort = this.sort;
            },
            (error: any) => {

            },
            () => {

            }
        );
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if(found) {
            this.service.deletes(found).subscribe(
                value => {
                    if(value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {
                    console.log(error);
                }
            );
        }
    }

    onItemCreated(item: Golongan) {
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemUpdated(item: Golongan) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items.sort();
    }

    onItemDeleted(item: Golongan) {
        let i = this.items.length;
        while(i--) {
            if(this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items.sort();
    }

    rowSelectedHandler(data: Golongan): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: Golongan) {
        data.isSelected = !data.isSelected; 
    }
}