import { Component, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { Golongan, GolonganService } from "../golongan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-golongan',
    templateUrl: './create-golongan.component.html'
})
export class CreateGolonganComponent implements OnInit, OnDestroy {
    constructor(private golonganService: GolonganService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<Golongan>();

    public item = new Golongan;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.golonganService.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new Golongan;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}