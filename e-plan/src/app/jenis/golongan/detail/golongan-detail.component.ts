import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { Golongan, GolonganService } from "../golongan.service";
import { log } from "util";
import { UIObservableClient } from "../../../infrastructur/subscriber";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";

@Component({
    selector: 'golongan-detail',
    templateUrl: './golongan-detail.component.html'
})
export class GolonganDetailComponent implements OnInit, OnDestroy {
    constructor(private golonganService: GolonganService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: Golongan;
    @Output() onItemUpdated = new EventEmitter<Golongan>();
    @Output() onItemDeleted = new EventEmitter<Golongan>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.golonganService.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.golonganService.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}