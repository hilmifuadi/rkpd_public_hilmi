import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/observable";

export class Golongan {
  constructor(
    public kode: string = "", 
    public nama: string = "", 
    public pangkat: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GolonganService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/golongan'

  getCollection() {
    return this.http.get<Golongan[]>(this.api);
  }

  create(entity: Golongan) {  
    return this.http.post<Golongan>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<Golongan> {
    return this.http.get<Golongan>(`${this.api}/${id}`);
  }

  update(entity: Golongan): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: Golongan) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: Golongan[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
