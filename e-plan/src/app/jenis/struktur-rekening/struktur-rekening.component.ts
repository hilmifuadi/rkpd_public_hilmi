import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatSort, MatTableDataSource } from "@angular/material";

import { StrukturRekening, StrukturRekeningService } from "./struktur-rekening.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './struktur-rekening.component.html'
})
export class StrukturRekeningComponent {
    constructor(private service: StrukturRekeningService, private uiService: UIObservableService) { }

    public items: StrukturRekening[];
    public item = new StrukturRekening;

    public title: string = 'Struktur Rekening';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'uraian'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: StrukturRekening) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: StrukturRekening) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: StrukturRekening) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: StrukturRekening): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: StrukturRekening) {
        data.isSelected = !data.isSelected;
    }
}