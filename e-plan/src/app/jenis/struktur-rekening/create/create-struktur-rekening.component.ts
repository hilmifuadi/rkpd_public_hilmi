import { Component, Output, EventEmitter } from "@angular/core";
import { StrukturRekening, StrukturRekeningService } from "../struktur-rekening.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-struktur-rekening',
    templateUrl: './create-struktur-rekening.component.html'
})
export class CreateStrukturRekeningComponent {
    constructor(private service: StrukturRekeningService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<StrukturRekening>();

    public item = new StrukturRekening;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new StrukturRekening;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}