import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { StrukturRekeningUiModule } from "./struktur-rekening-ui.module";
import { StrukturRekeningRoutingModule } from './struktur-rekening-routing.module'

import { StrukturRekeningService } from "./struktur-rekening.service";

import { StrukturRekeningComponent } from './struktur-rekening.component'
import { CreateStrukturRekeningComponent } from "./create/create-struktur-rekening.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { StrukturRekeningDetailComponent } from './detail/struktur-rekening-detail.component';

@NgModule({
    imports: [
        SharedModule, 
        StrukturRekeningUiModule, 
        UIObservableModule,
        StrukturRekeningRoutingModule
    ],
    declarations: [
        StrukturRekeningComponent, CreateStrukturRekeningComponent,
        StrukturRekeningDetailComponent
    ],
    providers: [
        StrukturRekeningService
    ]
})
export class StrukturRekeningModule {}