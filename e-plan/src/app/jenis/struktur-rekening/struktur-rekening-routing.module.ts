import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { StrukturRekeningComponent } from './struktur-rekening.component'

const routes: Routes = [
    { path: '', component: StrukturRekeningComponent }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class StrukturRekeningRoutingModule {}