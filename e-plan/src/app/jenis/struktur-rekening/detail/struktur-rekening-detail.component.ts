import { Component, Input, Output, EventEmitter } from "@angular/core";
import { StrukturRekening, StrukturRekeningService } from "../struktur-rekening.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";
 
@Component({
    selector: 'struktur-rekening-detail',
    templateUrl: './struktur-rekening-detail.component.html'
})
export class StrukturRekeningDetailComponent {
    constructor(private service: StrukturRekeningService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: StrukturRekening;
    @Output() onItemUpdated = new EventEmitter<StrukturRekening>();
    @Output() onItemDeleted = new EventEmitter<StrukturRekening>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}