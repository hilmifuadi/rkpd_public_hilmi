import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class StrukturRekening {
  constructor(
    public kode: string = "",
    public uraian: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class StrukturRekeningService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/struktur-rekening'

  getCollection() {
    return this.http.get<StrukturRekening[]>(this.api);
  }

  create(entity: StrukturRekening) {  
    return this.http.post<StrukturRekening>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<StrukturRekening> {
    return this.http.get<StrukturRekening>(`${this.api}/${id}`);
  }

  update(entity: StrukturRekening): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: StrukturRekening) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: StrukturRekening[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
