import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { JenisPerspektifUiModule } from "./jenis-perspektif-ui.module";
import { JenisPerspektifRoutingModule } from './jenis-perspektif-routing.module'

import { JenisPerspektifService } from "./jenis-perspektif.service";

import { JenisPerspektifComponent } from './jenis-perspektif.component'
import { CreateJenisPerspektifComponent } from "./create/create-jenis-perspektif.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { JenisPerspektifDetailComponent } from './detail/jenis-perspektif-detail.component';

@NgModule({
    imports: [
        SharedModule,
        JenisPerspektifUiModule,
        UIObservableModule,
        JenisPerspektifRoutingModule
    ],
    declarations: [ 
        JenisPerspektifComponent, CreateJenisPerspektifComponent,
        JenisPerspektifDetailComponent
    ],
    providers: [
        JenisPerspektifService
    ]
})
export class JenisPerspektifModule { }