import { Component, ViewChild } from '@angular/core'
import { MatSidenav, MatSort, MatTableDataSource } from "@angular/material";

import { JenisPerspektif, JenisPerspektifService } from "./jenis-perspektif.service";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './jenis-perspektif.component.html'
})
export class JenisPerspektifComponent {
    constructor(private service: JenisPerspektifService, private uiService: UIObservableService) { }

    public items: JenisPerspektif[];
    public item = new JenisPerspektif;

    public title: string = 'Jenis Perspektif';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'nama'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
            this.dataSource.sort = this.sort;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: JenisPerspektif) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: JenisPerspektif) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: JenisPerspektif) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: JenisPerspektif): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: JenisPerspektif) {
        data.isSelected = !data.isSelected;
    }
}