import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { JenisPerspektifComponent} from './jenis-perspektif.component'

const routes: Routes = [
    { 
        path: '', 
        component: JenisPerspektifComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class JenisPerspektifRoutingModule { }