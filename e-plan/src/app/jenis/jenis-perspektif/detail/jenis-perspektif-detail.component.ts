import { Component, Input, Output, EventEmitter } from "@angular/core";
import { JenisPerspektif, JenisPerspektifService } from "../jenis-perspektif.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'jenis-perspektif-detail',
    templateUrl: './jenis-perspektif-detail.component.html'
})
export class JenisPerspektifDetailComponent {
    constructor(private service: JenisPerspektifService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: JenisPerspektif;
    @Output() onItemUpdated = new EventEmitter<JenisPerspektif>();
    @Output() onItemDeleted = new EventEmitter<JenisPerspektif>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}