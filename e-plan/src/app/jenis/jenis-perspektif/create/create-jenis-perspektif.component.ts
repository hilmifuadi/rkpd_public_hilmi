import { Component, Output, EventEmitter } from "@angular/core";
import { JenisPerspektif, JenisPerspektifService } from "../jenis-perspektif.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-jenis-perspektif',
    templateUrl: './create-jenis-perspektif.component.html'
})
export class CreateJenisPerspektifComponent {
    constructor(private service: JenisPerspektifService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<JenisPerspektif>();

    public item = new JenisPerspektif;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new JenisPerspektif;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}