import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class JenisPerspektif {
  constructor(
    public kode: string = "", 
    public nama: string = "", 
    public keterangan: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class JenisPerspektifService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/jenis-perspektif';

  getCollection() {
    return this.http.get<JenisPerspektif[]>(this.api);
  }

  create(entity: JenisPerspektif) {  
    return this.http.post<JenisPerspektif>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<JenisPerspektif> {
    return this.http.get<JenisPerspektif>(`${this.api}/${id}`);
  }

  update(entity: JenisPerspektif): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: JenisPerspektif) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: JenisPerspektif[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
