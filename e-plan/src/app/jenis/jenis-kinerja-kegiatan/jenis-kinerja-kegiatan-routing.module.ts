import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { JenisKinerjaKegiatanComponent } from './jenis-kinerja-kegiatan.component'

const routes: Routes = [
    { 
        path: '', 
        component: JenisKinerjaKegiatanComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class JenisKinerjaKegiatanRoutingModule { }