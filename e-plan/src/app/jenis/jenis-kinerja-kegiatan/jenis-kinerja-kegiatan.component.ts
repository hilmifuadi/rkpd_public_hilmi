import { Component, OnInit, ViewChild } from '@angular/core'
import { JenisKinerjaKegiatan, JenisKinerjaKegiatanService } from './jenis-kinerja-kegiatan.service'
import { MatSidenav, MatTableDataSource, MatSort } from "@angular/material";
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
    templateUrl: './jenis-kinerja-kegiatan.component.html'
})
export class JenisKinerjaKegiatanComponent implements OnInit {
    constructor(private service: JenisKinerjaKegiatanService, private uiService: UIObservableService) { }

    public items: JenisKinerjaKegiatan[];
    public item = new JenisKinerjaKegiatan;

    public title: string = 'Jenis Kinerja Kegiatan';

    public uiClient = new UIObservableClient;

    columns = ['kode', 'uraian'];
    dataSource = new MatTableDataSource;
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
        this.service.getCollection().subscribe((value) => {
            this.items = value;
            this.dataSource.data = value;
            this.dataSource.sort = this.sort;
        });
        this.dataSource.sort = this.sort;
    }

    onSearchChanged(text: string) {
        this.dataSource.filter = text;
    }

    onDelete() {
        let found = this.items.filter(e => e.isSelected);
        if (found) {
            this.service.deletes(found).subscribe(
                value => {
                    if (value) {
                        this.items = this.items.filter(e => !e.isSelected);
                        this.dataSource.data = this.items;
                    }
                },
                (error: any) => {

                }
            );
        }
    }

    onItemCreated(item: JenisKinerjaKegiatan) {
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemUpdated(item: JenisKinerjaKegiatan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.items.push(item);
        this.dataSource.data = this.items;
    }

    onItemDeleted(item: JenisKinerjaKegiatan) {
        let i = this.items.length;
        while (i--) {
            if (this.items[i].kode.trim() === item.kode.trim()) {
                this.items.splice(i, 1);
                break;
            }
        }
        this.dataSource.data = this.items;
    }

    rowSelectedHandler(data: JenisKinerjaKegiatan): void {
        this.item = data;
        this.uiService.notifyOnDetailStateChanged(true);
    }

    selectionHandler(data: JenisKinerjaKegiatan) {
        data.isSelected = !data.isSelected;
    }
}
