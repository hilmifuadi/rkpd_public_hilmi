import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { JenisKinerjaKegiatanUiModule } from "./jenis-kinerja-kegiatan-ui.module";
import { JenisKinerjaKegiatanRoutingModule} from './jenis-kinerja-kegiatan-routing.module'
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

import { JenisKinerjaKegiatanService } from "./jenis-kinerja-kegiatan.service";

import { JenisKinerjaKegiatanComponent } from './jenis-kinerja-kegiatan.component'
import { CreateJenisKinerjaKegiatanComponent } from "./create/create-jenis-kinerja-kegiatan.component";
import { JenisKinerjaKegiatanDetailComponent } from './detail/jenis-kinerja-kegiatan-detail.component';

@NgModule({
    imports: [
        SharedModule,
        JenisKinerjaKegiatanUiModule,
        UIObservableModule,
        JenisKinerjaKegiatanRoutingModule
    ],
    declarations: [ 
        JenisKinerjaKegiatanComponent,
        CreateJenisKinerjaKegiatanComponent, JenisKinerjaKegiatanDetailComponent
    ],
    providers: [
        JenisKinerjaKegiatanService
    ]
})
export class JenisKinerjaKegiatanModule { }