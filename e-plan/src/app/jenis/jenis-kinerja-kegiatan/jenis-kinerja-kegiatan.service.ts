import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class JenisKinerjaKegiatan {
  constructor(
    public kode: string = "", 
    public uraian: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class JenisKinerjaKegiatanService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/jenis-kinerja-kegiatan'

  getCollection() {
    return this.http.get<JenisKinerjaKegiatan[]>(this.api);
  }

  create(entity: JenisKinerjaKegiatan) {  
    return this.http.post<JenisKinerjaKegiatan>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<JenisKinerjaKegiatan> {
    return this.http.get<JenisKinerjaKegiatan>(`${this.api}/${id}`);
  }

  update(entity: JenisKinerjaKegiatan): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: JenisKinerjaKegiatan) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: JenisKinerjaKegiatan[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
