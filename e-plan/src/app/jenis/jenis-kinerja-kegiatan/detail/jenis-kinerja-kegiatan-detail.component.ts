import { Component, Input, Output, EventEmitter } from "@angular/core";
import { JenisKinerjaKegiatan, JenisKinerjaKegiatanService } from "../jenis-kinerja-kegiatan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'jenis-kinerja-kegiatan-detail',
    templateUrl: './jenis-kinerja-kegiatan-detail.component.html',
    styleUrls: ['./jenis-kinerja-kegiatan-detail.component.css']
})
export class JenisKinerjaKegiatanDetailComponent {
    constructor(private service: JenisKinerjaKegiatanService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: JenisKinerjaKegiatan;
    @Output() onItemUpdated = new EventEmitter<JenisKinerjaKegiatan>();
    @Output() onItemDeleted = new EventEmitter<JenisKinerjaKegiatan>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}