import { Component, Output, EventEmitter, OnInit } from "@angular/core";
import { JenisKinerjaKegiatan, JenisKinerjaKegiatanService } from "../jenis-kinerja-kegiatan.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-jenis-kinerja-kegiatan',
    templateUrl: './create-jenis-kinerja-kegiatan.component.html',
    styleUrls: ['./create-jenis-kinerja-kegiatan.component.css']
})
export class CreateJenisKinerjaKegiatanComponent implements OnInit {
    constructor(private service: JenisKinerjaKegiatanService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<JenisKinerjaKegiatan>();

    public item = new JenisKinerjaKegiatan;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new JenisKinerjaKegiatan;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}