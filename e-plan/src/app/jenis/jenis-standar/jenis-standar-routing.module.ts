import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { JenisStandarComponent } from './jenis-standar.component'

const routes: Routes = [
    { 
        path: '', 
        component: JenisStandarComponent,
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes) ], 
    exports: [ RouterModule ]
})
export class JenisStandarRoutingModule { }