import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/observable";
import { of } from 'rxjs/observable/of'
import { catchError, map, tap } from "rxjs/operators";
import { HttpParams } from '@angular/common/http/src/params';

export class JenisStandar {
  constructor(
    public kode: string = "", 
    public nama: string = "", 
    public keterangan: string = "",
    public isSelected: boolean = false) { }
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class JenisStandarService {

  constructor(private http: HttpClient) { }

  private api = 'api/jenis/jenis-standar'

  getCollection() {
    return this.http.get<JenisStandar[]>(this.api);
  }

  create(entity: JenisStandar) {  
    return this.http.post<JenisStandar>(this.api, entity, httpOptions);
  }

  read(id: string): Observable<JenisStandar> {
    return this.http.get<JenisStandar>(`${this.api}/${id}`);
  }

  update(entity: JenisStandar): Observable<boolean> {
    return this.http.put<boolean>(this.api, entity, httpOptions);
  }

  delete(item: JenisStandar) {
    return this.http.post<boolean>(`${this.api}/delete`, item, httpOptions);
  }

  deletes(entities: JenisStandar[]): Observable<boolean> {
    return this.http.post<boolean>(`${this.api}/deletes`, entities, httpOptions);
  }
}
