import { Component, Input, Output, EventEmitter } from "@angular/core";
import { JenisStandar, JenisStandarService } from "../jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'jenis-standar-detail',
    templateUrl: './jenis-standar-detail.component.html'
})
export class JenisStandarDetailComponent {
    constructor(private service: JenisStandarService, private uiService: UIObservableService) { }

    public uiClient = new UIObservableClient;
    @Input() item: JenisStandar;
    @Output() onItemUpdated = new EventEmitter<JenisStandar>();
    @Output() onItemDeleted = new EventEmitter<JenisStandar>();
    
    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler()  {
        this.uiService.notifyOnDetailStateChanged(false);
    }

    updateHandler() {
        this.service.update(this.item).subscribe(
            (value: boolean) => {
                this.onItemUpdated.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error : any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }

    deleteHandler() {
        this.service.delete(this.item).subscribe(
            (value: boolean) => {
                this.onItemDeleted.emit(this.item);
                this.uiService.notifyOnDetailStateChanged(!value);
            },
            (error: any) => {
                this.uiService.notifyOnDetailStateChanged(true);
            }
        );
    }
}