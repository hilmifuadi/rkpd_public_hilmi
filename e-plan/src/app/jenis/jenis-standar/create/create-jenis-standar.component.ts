import { Component, Output, EventEmitter } from "@angular/core";
import { JenisStandar, JenisStandarService } from "../jenis-standar.service";
import { UIObservableService } from "../../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../../infrastructur/subscriber";

@Component({
    selector: 'create-jenis-standar',
    templateUrl: './create-jenis-standar.component.html'
})
export class CreateJenisStandarComponent {
    constructor(private service: JenisStandarService, public uiService: UIObservableService) { }

    @Output() onItemCreated = new EventEmitter<JenisStandar>();

    public item = new JenisStandar;
    public uiClient = new UIObservableClient;

    ngOnInit() {
        this.uiService.addObserver(this.uiClient);
    }

    ngOnDestroy() {
        this.uiService.removeObserver(this.uiClient);
    }

    closePanelHandler() {
        this.uiService.notifyOnCreateStateChanged();
    }

    createHandler() {
        this.service.create(this.item).subscribe(
            value => {
                this.onItemCreated.emit(value);
                this.item = new JenisStandar;
                this.uiService.notifyOnCreateStateChanged();
            },
            (error: any) => {

            }
        );
    }
}