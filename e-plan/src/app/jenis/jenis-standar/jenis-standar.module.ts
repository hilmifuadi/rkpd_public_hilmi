import { NgModule } from '@angular/core'

import { SharedModule } from "../../shared/shared.module";
import { JenisStandarUiModule } from "./jenis-standar-ui.module";
import { JenisStandarRoutingModule} from './jenis-standar-routing.module'
import { JenisStandarService } from "./jenis-standar.service";
import { JenisStandarComponent } from './jenis-standar.component';
import { CreateJenisStandarComponent } from "./create/create-jenis-standar.component";
import { UIObservableModule } from '../../infrastructur/ui-observable.module';
import { JenisStandarDetailComponent } from './detail/jenis-standar-detail.component';

@NgModule({
    imports: [
        SharedModule,
        JenisStandarUiModule,
        UIObservableModule,
        JenisStandarRoutingModule
    ],
    declarations: [ 
        JenisStandarComponent,
        CreateJenisStandarComponent,
        JenisStandarDetailComponent
    ],
    providers: [
        JenisStandarService
    ]
})
export class JenisStandarModule { }