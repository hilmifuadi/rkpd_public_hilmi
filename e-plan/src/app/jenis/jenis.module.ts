import { NgModule } from '@angular/core'

import { JenisRoutingModule } from './jenis-routing.module'
import { JenisUiModule } from './jenis-ui.module'

import { JenisComponent } from './jenis.component'

@NgModule({
  imports: [
        JenisUiModule,
        JenisRoutingModule
    ],
    declarations: [ 
        JenisComponent
    ]
})
export class JenisModule { }
