import { Component, Input, Output, EventEmitter } from '@angular/core'
import { MatDrawer } from '@angular/material'

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() title: string
  @Output() onToggleSidenav = new EventEmitter();
  toggleSidenavHandler() {
    this.onToggleSidenav.emit();
  }
}
