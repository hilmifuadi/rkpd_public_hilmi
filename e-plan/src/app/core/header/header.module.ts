import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { HeaderComponent } from './header.component'
import { HeaderUiModule } from './header-ui.module'

@NgModule({
  imports: [ CommonModule, HeaderUiModule],
  declarations: [HeaderComponent],
  exports: [HeaderComponent]
})
export class HeaderModule { }
