import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'

import { SubHeaderModule } from './sub-header/sub-header.module'
import { SubHeaderWithFilterModule } from "./sub-header-with-filter/sub-header-with-filter.module";
import { RouterModule } from '@angular/router';
import { TreeModule } from './tree/tree.module';

@NgModule({
  imports: [
    HttpClientModule, 
    FormsModule, 
    CommonModule,
    RouterModule, 
    SubHeaderModule, 
    SubHeaderWithFilterModule,
    TreeModule
  ],
  exports: [
    HttpClientModule, 
    FormsModule, 
    CommonModule,
    RouterModule, 
    SubHeaderModule, 
    SubHeaderWithFilterModule,
    TreeModule
  ]
})
export class SharedModule { }
