import { NgModule } from '@angular/core'
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatChipsModule } from "@angular/material/chips";
import { MatTableModule, MatListModule } from '@angular/material';

@NgModule({
  imports: [
    MatIconModule, 
    MatButtonModule,
    MatToolbarModule,
    MatChipsModule,
    MatTableModule,
    MatListModule
  ],
  exports: [
    MatIconModule, 
    MatButtonModule,
    MatToolbarModule,
    MatChipsModule,
    MatTableModule,
    MatListModule
  ]
})
export class SubHeaderWithFilterUiModule { }
