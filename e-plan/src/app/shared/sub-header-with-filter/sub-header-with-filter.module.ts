import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SubHeaderWithFilterComponent } from './sub-header-with-filter.component'
import { SubHeaderWithFilterUiModule } from './sub-header-with-filter-ui.module'
import { UIObservableModule } from '../../infrastructur/ui-observable.module';

@NgModule({
  imports: [CommonModule, SubHeaderWithFilterUiModule, UIObservableModule],
  declarations: [SubHeaderWithFilterComponent],
  exports: [SubHeaderWithFilterComponent]
})
export class SubHeaderWithFilterModule { }
