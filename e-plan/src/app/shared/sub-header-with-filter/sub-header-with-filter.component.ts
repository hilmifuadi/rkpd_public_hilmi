import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core'
import { UIObservableService } from '../../infrastructur/ui-observable.service';
import { UIObservableClient } from '../../infrastructur/subscriber';

@Component({
  selector: 'sub-header-with-filter',
  templateUrl: './sub-header-with-filter.component.html',
  styleUrls: ['./sub-header-with-filter.component.css']
})
export class SubHeaderWithFilterComponent implements OnInit, OnDestroy {
  constructor(private uiObservableService: UIObservableService) { }

  public uiClient: UIObservableClient = new UIObservableClient;
  
  @Input() title: string;
  @Output() onSearchChanged = new EventEmitter<string>();

  ngOnInit() {
    this.uiObservableService.addObserver(this.uiClient);
  }

  ngOnDestroy() {
    this.uiObservableService.removeObserver(this.uiClient);
  }

  filterStateChangesHandler() {
    this.uiObservableService.notifyOnFilterStateChanged();
  }

  createStateChangesHandler(): void {
    this.uiObservableService.notifyOnCreateStateChanged();
  }

  deleteStateChangesHandler(): void {
    this.uiObservableService.notifyOnDeleteStateChanged();
  }

  searchHandler(search: string): void {
    this.onSearchChanged.emit(search);
  }
}
