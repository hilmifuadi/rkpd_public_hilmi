import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SubHeaderComponent } from './sub-header.component'
import { SubHeaderUiModule } from './sub-header-ui.module'

@NgModule({
  imports: [CommonModule, SubHeaderUiModule],
  declarations: [SubHeaderComponent],
  exports: [SubHeaderComponent]
})
export class SubHeaderModule { }
