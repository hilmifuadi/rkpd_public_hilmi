import { NgModule } from '@angular/core'
import { MatIconModule, MatButtonModule, MatToolbarModule } from '@angular/material'

@NgModule({
  imports: [MatIconModule, MatButtonModule, MatToolbarModule],
  exports: [MatIconModule, MatButtonModule, MatToolbarModule]
})
export class SubHeaderUiModule { }
