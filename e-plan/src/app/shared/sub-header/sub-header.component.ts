import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core'
import { UIObservableClient } from '../../infrastructur/subscriber';
import { UIObservableService } from '../../infrastructur/ui-observable.service';

@Component({
  selector: 'sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit, OnDestroy {
  constructor(private uiService: UIObservableService) {}

  public uiClient = new UIObservableClient;
  @Output() onSearchChanged = new EventEmitter<string>();
  @Input() title: string;
  @Output() onDelete = new EventEmitter;

  ngOnInit() {
    this.uiService.addObserver(this.uiClient);
  }

  ngOnDestroy() {
    this.uiService.removeObserver(this.uiClient);
  }

  createStateChangesHandler() {
    this.uiService.notifyOnCreateStateChanged();
  }

  deleteStateChangesHandler() {
    this.uiService.notifyOnDeleteStateChanged();
  }

  deleteHandler() {
    this.onDelete.emit();
  }

  searchHandler(text: string) {
    this.onSearchChanged.emit(text);
  }
}
