import { Desa } from "./transactions";

interface IAction<T> {
    items: T[];
    type: string;
    error: boolean;
    errorMessages: ErrorMessage[];
} 

class ErrorMessage {
    public title: string;
    public message: string;
}

export class ActionOfDesa implements IAction<Desa> {
    public items: Desa[] = [];
    public type: string = "";
    public error: boolean = false;
    public errorMessages: ErrorMessage[] = [];
}