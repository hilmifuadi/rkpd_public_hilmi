import { isError } from "util";

export class StatusUnit {
    public kode: string = "";
    public uraian: string = "";
}

export class StrukturUnit {
    constructor(
        public kode: string = "",
        public uraian: string = "",
        public isSelected: boolean = false) { }
}

export class UnitOrganisasi {
    public id: string = "";
    public kode: string = "";
    public nama: string = "";
    public akrounit: string = "";
    public alamat: string = "";
    public telepon: string = "";
    public tipe: string = "";
    public isSelected: boolean = false;
    public strukturUnitKey: string = "";
    public strukturUnit: StrukturUnit = new StrukturUnit;
    public statusUnitKey: string = "";
    public statusUnit: StatusUnit = new StatusUnit;
    public unit: UnitOrganisasi[] = [];
}

export class Kecamatan {
    public id: string = "";
    public isSelected: boolean = false;
    public unitOrganisasiKey: string = "";
    public unitOrganisasi: UnitOrganisasi = new UnitOrganisasi;
}

export class Desa {
    public id: string = "";
    public pagu: number = 0;
    public kecamatanId: string = "";
    public kecamatan = new Kecamatan;
    public unitOrganisasiId = "";
    public unitOrganisasi = new UnitOrganisasi;
    public tahunId: string = "";
    public tahun = new Tahun;
    public isSelected: boolean = false;
}

export class Tahun {
    public id: string = "";
    public nama: string = ""
}

class ErrorMessage {
    public title: string;
    public message: string;
}

class IState {
    public isError: boolean;
    public errMsg: ErrorMessage;
    constructor(){
        this.isError = false;
        this.errMsg = new ErrorMessage;
    }
}

export class Visi extends IState {
    constructor(
      public id: string = "", 
      public nomor: string = "", 
      public uraian: string = "",
      public isSelected: boolean = false,
    ) { super(); }
  }