import { Injectable } from "@angular/core";
import { TreeServer } from "./tree.server";
import { ITreeObserver } from "./tree.infrastructur";
import { Tree } from "./tree";

@Injectable()
export class TreeService {
    private treeServer = new TreeServer;

    public addObserver(observer: ITreeObserver) {
        this.treeServer.addObserver(observer);
    }

    public removeObserver(observer: ITreeObserver) {
        this.treeServer.removeObserver(observer);
    }

    public notifyOnTreeChanged(data: Tree[]) {
        this.treeServer.notifyOnTreeChanged(data);
    }
}