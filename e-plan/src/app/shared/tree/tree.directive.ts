import { Directive, ViewContainerRef } from "@angular/core";


@Directive({selector: '[tree-host]'})
export class TreeDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}