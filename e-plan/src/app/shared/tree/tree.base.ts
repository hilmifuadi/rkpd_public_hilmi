import { ITreeObservable, ITreeObserver } from "./tree.infrastructur";
import { Tree } from "./tree";

export abstract class TreeObservable implements ITreeObservable {
    
    protected tree: Tree[];
    protected observers: ITreeObserver[];

    constructor() {
        this.observers= [];
        this.tree = [];
    }

    abstract addObserver(observer: ITreeObserver): void;

    removeObserver(observer: ITreeObserver): void {
        let i = this.observers.length;
        while(i--) {
            if(this.observers[i] === observer) {
                this.observers.splice(i, 1);
            }
        }
    }

    notifyObservers() {
        let i = this.observers.length;
        while(i--) {
            this.observers[i].setData(this.tree);
        }
    }
}