import { TreeObservable } from "./tree.base";
import { Tree } from "./tree";
import { ITreeObserver } from "./tree.infrastructur";

export class TreeServer extends TreeObservable {
    constructor() {
        super();
    }
    
    addObserver(observer: ITreeObserver): void {
        this.observers.push(observer);
        this.notifyObservers();
    }

    notifyOnTreeChanged(data: Tree[]) {
        this.tree = data;
        this.notifyObservers();
    }
}