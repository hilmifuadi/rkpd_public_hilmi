import { ITreeObserver } from "./tree.infrastructur";
import { Tree } from "./tree";
import { TreeServer } from "./tree.server";

export class TreeClient implements ITreeObserver  {
    constructor() {
        this.tree = [];
    }

    public tree: Tree[];
    
    setData(data: Tree[]) {
        this.tree = data;
    }
}