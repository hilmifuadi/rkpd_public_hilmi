import { Tree } from "./tree";

export interface ITreeObservable {
    addObserver(observer: ITreeObserver);
    removeObserver(observer: ITreeObserver);
    notifyObservers();
}

export interface ITreeObserver {
    setData(data: Tree[]): void;
}