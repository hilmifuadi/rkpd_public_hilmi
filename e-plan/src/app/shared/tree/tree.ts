export class Tree {
    public id: string = "";
    public text: string = "";
    public isExpanded: boolean = false;
    public node: Tree[] = [];
    public level: number = 0;
}