import { Component, ComponentFactoryResolver, ViewChild, Input  } from "@angular/core";
import { TreeDirective } from "./tree.directive";
import { TreeComponent } from "./tree.component";
import { Tree } from "./tree";
import { TreeService } from "./tree.service";
import { TreeClient } from "./tree.client";

@Component({
    selector: 'tree-host',
    templateUrl: './tree-host.component.html',
    styleUrls: ['./tree-host.component.css']
})
export class TreeHostComponent {

    constructor(private componentFactoryResolver: ComponentFactoryResolver) { 
    }

    @ViewChild(TreeDirective) treeDirective: TreeDirective;

    @Input() set tree(value: Tree[]) {
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(TreeComponent);
        let viewContainerRef = this.treeDirective.viewContainerRef;
        value.forEach(item => {
            let componentRef = viewContainerRef.createComponent(componentFactory);
            let newComp = <TreeComponent>componentRef.instance;
            newComp.tree = item;             
        });
    }
    get tree() { return this.tree; }
}