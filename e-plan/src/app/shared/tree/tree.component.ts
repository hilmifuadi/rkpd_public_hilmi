import { Component, ViewChild, ComponentFactoryResolver, OnDestroy } from "@angular/core";
import { Tree } from "./tree";
import { TreeDirective } from "./tree.directive";
import { UIObservableService } from "../../infrastructur/ui-observable.service";
import { UIObservableClient } from "../../infrastructur/subscriber";
import { UIFilter } from "../../infrastructur/data";
import { TreeService } from "./tree.service";
import { TreeClient } from "./tree.client";

@Component({
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnDestroy  {
    constructor(public componentFactoryResolver: ComponentFactoryResolver, public treeService: TreeService) {
        this.treeService.addObserver(this.treeClient);
    }
    @ViewChild(TreeDirective) treeDirective: TreeDirective;
    
    private _tree: Tree = new Tree;
    public set tree(value: Tree) { this._tree = value; };
    public get tree() { return this._tree; }

    public treeClient = new TreeClient;

    ngOnDestroy() {
        this.treeService.removeObserver(this.treeClient);
    }

    treeExpandedHandler(component: TreeComponent) {
        if(component.treeDirective.viewContainerRef.length <= 0) {
            this.buildNode(component.tree, component);
        }
        else {
            component.treeDirective.viewContainerRef.clear();
        }
    }

    treeNodeSelectedHandler(tree: Tree) {
        console.log(tree);
    }

    buildNode(node: Tree, component: TreeComponent) {
        let componentFactory = component.componentFactoryResolver.resolveComponentFactory(TreeComponent);
        let viewContainerRef = component.treeDirective.viewContainerRef;
        node.node.forEach(value => {
            let componentRef = viewContainerRef.createComponent(componentFactory);
            let newComp = <TreeComponent>componentRef.instance;
            newComp.tree = value;
        });
    }
}