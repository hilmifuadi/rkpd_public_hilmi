import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TreeComponent } from "./tree.component";
import { TreeHostComponent } from "./tree-host.component";
import { TreeDirective } from "./tree.directive";
import { TreeService } from "./tree.service";
import { TreeUiModule } from "./tree-ui.module";

@NgModule({
    imports: [CommonModule, TreeUiModule],
    declarations: [TreeComponent, TreeDirective, TreeHostComponent],
    exports: [TreeHostComponent],
    entryComponents: [TreeComponent],
    providers: [TreeService]
})
export class TreeModule { }