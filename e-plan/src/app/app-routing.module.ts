import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PageNotFoundComponent } from './page-not-found.component'

const routes: Routes = [
    { path: 'jenis',  loadChildren: 'app/jenis/jenis.module#JenisModule' },
    { path: 'daftar',  loadChildren: 'app/daftar/daftar.module#DaftarModule' },
    { path: 'rpjmd', loadChildren: 'app/rpjmd/rpjmd.module#RpjmdModule'},
    { path: '',  redirectTo: 'jenis', pathMatch: 'full' },
    { path: '**',  component: PageNotFoundComponent }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
